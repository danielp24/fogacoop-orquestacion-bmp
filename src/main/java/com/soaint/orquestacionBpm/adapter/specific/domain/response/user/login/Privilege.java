
package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Privilege implements Serializable {

    private String id;
    private String name;
    private String code;
    private Boolean enabled;

}
