package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetalleNovedadesDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
public class ResponseGetDetalleNovedadesDTO extends BaseResponseAdapter<BodyGetDetalleNovedadesDTO[]> implements Serializable {
}