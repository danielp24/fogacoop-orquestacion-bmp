package com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd;

import lombok.*;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
public class lstAdjuntosDTO {

    private String nombre;
    private String contentType;
    private String tipoAdjunto;
    private String fileBase64;
}
