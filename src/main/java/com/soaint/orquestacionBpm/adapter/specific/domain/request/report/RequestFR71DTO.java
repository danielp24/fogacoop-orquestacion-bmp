package com.soaint.orquestacionBpm.adapter.specific.domain.request.report;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@ApiModel(description = "Clase representativa de la captura de datos del reporte")
public class RequestFR71DTO {

    @ApiModelProperty(notes = "NIT identificador de la Cooperativa")
    private String nitCooperativa;
    @ApiModelProperty(notes = "Nombre de la Cooperativa")
    private String nombreEntidad;
    @ApiModelProperty(notes = "Concepto o descripción de Pagos")
    private String conceptoSaldos;
    @ApiModelProperty(notes = "Recursos del reporte")
    private String recursos;
    @ApiModelProperty(notes = "Valor que se devuelve")
    private String valorDevolucion;
    @ApiModelProperty(notes = "Logo del Reporte")
    private String logo;
    @ApiModelProperty(notes = "Imagen Radio Buttom No Seleccionado")
    private String rbNoSelect;
    @ApiModelProperty(notes = "Imagen Radio Buttom Seleccionado")
    private String rbSelect;
    @ApiModelProperty(notes = "Rutas del nombre jasper y Nombre del PDF")
    private RutasDTO rutas;
}
