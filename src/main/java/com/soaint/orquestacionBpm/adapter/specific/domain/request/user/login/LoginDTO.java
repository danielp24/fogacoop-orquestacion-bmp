package com.soaint.orquestacionBpm.adapter.specific.domain.request.user.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString

@ApiModel(description = "Clase representativa de la solicitud de proceso")
public class LoginDTO implements Serializable {

    @ApiModelProperty(notes = "Encabezado con parámetros de solicitud de proceso Login")
    private HeaderDTO header;

    @ApiModelProperty(notes = "Cuerpo con parámetros de solicitud de proceso Login")
    private BodyDTO body;
}
