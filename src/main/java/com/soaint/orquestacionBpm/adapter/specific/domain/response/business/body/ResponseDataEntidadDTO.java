package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los datos de entidad")
public class ResponseDataEntidadDTO extends BaseResponseAdapter<ResponseGetEntityByCodeDTO[]> {

}
