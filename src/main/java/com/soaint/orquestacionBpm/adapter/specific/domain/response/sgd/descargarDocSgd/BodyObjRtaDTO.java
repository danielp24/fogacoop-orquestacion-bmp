package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.descargarDocSgd;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class BodyObjRtaDTO implements Serializable {

    @ApiModelProperty(notes = "Respuesta al proceso ")
    private ObjRtaDTO objRta;
}
