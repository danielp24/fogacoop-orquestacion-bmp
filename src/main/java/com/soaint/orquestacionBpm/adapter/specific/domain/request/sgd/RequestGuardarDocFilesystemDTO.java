package com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@ApiModel(description = "Clase representativa de envio de datos para resultado de reporte")
public class RequestGuardarDocFilesystemDTO implements Serializable {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nombre del archivo")
    private String nombreArchivo;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Extension del archivo")
    private String extensionArchivo;

    @ApiModelProperty(notes = "Lista de Contextos o seguimiento de rutas de destino del Filesystem.")
    private List<String> contextoFilesystem;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Binario del archivo")
    private String binBase64;

}
