package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.report.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.report.ReportApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.report.RequestFR71DTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.report.RutasDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.report.ResponseReportDTO;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.File;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class ReportApiImp implements ReportApiCliente {

    public ReportApiImp() {
        super();
    }

    @Autowired
    private ObjectMapper mapper;
    private RestTemplate template = new RestTemplate();

    @Value("${endpoint.report.fr71}")
    public String ENDPOINT_DATA_REPORT;
    @Value("${reporte.logo}")
    String Logo;
    @Value("${reporte.img.radiobutton.of}")
    String RadioButtonOFF;
    @Value("${reporte.img.radiobutton.on}")
    String RadioButtonON;
    @Value("${reporte.nombre.reporte}")
    String NombreReporte;
    @Value("${reporte.jasper}")
    String JasperReport;
    @Value("${resource.jasper}")
    String ubicationJasper;
    @Value("${resource.imagen}")
    String ubicationImg;
    @Value("${unit.disk}")
    String unitDisk;


    @Override
    public ResponseReportDTO dataReportConsume(RequestFR71DTO fr71DTO) throws WebClientException {

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_DATA_REPORT;

            //Verificando OS
            String so = System.getProperty("os.name");
            LogUtils.info(" >>>>>>>>>>>>>> os.name = " + so);
            String initialOS = (so.toLowerCase().contains("windows")) ? unitDisk + ":/" : "/";
            LogUtils.info(" >>>>>>>>>>>>>> initialOS = " + initialOS);
            String pathJasper = initialOS + ubicationJasper;
            String pathImg = initialOS + ubicationImg;

            LogUtils.info(" >>>>>>>>>>>>>> ubicationJasper = " + pathJasper);
            LogUtils.info(" >>>>>>>>>>>>>> ubicationImg = " + pathImg);
            String logo = pathImg + Logo;
            String rbNoSelect = pathImg + RadioButtonOFF;
            String rbSelect = pathImg + RadioButtonON;
            String nameJasper = pathJasper + JasperReport;
            String nameReport = NombreReporte;
            fr71DTO.setLogo(logo);
            fr71DTO.setRbNoSelect(rbNoSelect);
            fr71DTO.setRbSelect(rbSelect);
            fr71DTO.setRutas(RutasDTO.builder().nombreReporte(nameReport).nombreJasper(nameJasper).build());
            HttpEntity<RequestFR71DTO> entity = new HttpEntity<>(fr71DTO, headers);
            String responseService = template.postForObject(url, entity, String.class);

            return new Gson().fromJson(responseService, ResponseReportDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(" Error Invocando servicio Data Report: " + e.getMessage());
        }
    }
}
