package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Role implements Serializable {

    private String codeAuthApplication;
    private String nameAuthApplication;
    private String id;
    private String description;
    private String name;
    private Boolean enabled;
    private List<Privilege> privileges;

}
