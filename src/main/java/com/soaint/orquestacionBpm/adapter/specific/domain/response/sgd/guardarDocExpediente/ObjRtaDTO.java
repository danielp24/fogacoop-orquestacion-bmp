package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.guardarDocExpediente;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ObjRtaDTO implements Serializable {

    private String estado;
    private String codigo;
    private String mensaje;
    private List<RespuestaDTO> rta;
}
