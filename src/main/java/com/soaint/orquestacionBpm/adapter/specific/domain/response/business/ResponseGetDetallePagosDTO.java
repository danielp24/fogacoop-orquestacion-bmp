package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;


import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetallePagosDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;


@Builder
@NoArgsConstructor
@ToString
@ApiModel(description = "Clase representativa del Detalle de los Pagos")

public class ResponseGetDetallePagosDTO extends BaseResponseAdapter<List<BodyGetDetallePagosDTO>> implements Serializable {

}
