package com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa del proceso de solicitar Contabilizacion en la respuesta")
public class BodyConsultarEstadoComprobanteDTO {

    @ApiModelProperty(notes = "Respuesta del estado de la trasaccion del comprobante")
    private String solicitarEstadoTransaccionContableResult;
}
