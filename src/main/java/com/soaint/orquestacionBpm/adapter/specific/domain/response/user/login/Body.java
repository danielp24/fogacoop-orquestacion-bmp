
package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Body implements Serializable {

    private List<Role> roles;
    private String token;
    private String id;
    private String userName;
    private String name;
    private String email;
    private String phoneNumber;
    private String organization;
    private boolean enabled;
    private boolean lockoutEnabled;
    private String lockoutEndDateUtc;
    private String ldapGuid;

}
