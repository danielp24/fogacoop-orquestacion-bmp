package com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.body.BodyConsultarEstadoComprobanteDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa del proceso de solicitar Contabilizacion en la respuesta")
public class ResponseConsultarEstadoComprobanteDTO  extends BaseResponseAdapter<BodyConsultarEstadoComprobanteDTO> implements Serializable {
}