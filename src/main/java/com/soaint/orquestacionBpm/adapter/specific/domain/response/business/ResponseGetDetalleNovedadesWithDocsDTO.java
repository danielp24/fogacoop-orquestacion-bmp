package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;


import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetalleNovedadesDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetListaDocumentosNovedadDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la información de liqidación")
public class ResponseGetDetalleNovedadesWithDocsDTO {

    private BodyGetDetalleNovedadesDTO novedad;
    private List<BodyGetListaDocumentosNovedadDTO> listaDocumentos;

}
