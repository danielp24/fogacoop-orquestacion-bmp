package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los valores de los parámetros")
public class BodyAuthenticateDTO implements Serializable {

    @ApiModelProperty(notes = "tiempo limite en que expira el token")
    private String expire;
    @ApiModelProperty(notes = "Tipo del Token que se esta generando")
    private String typeToken;
    @ApiModelProperty(notes = "Token retornado")
    private String token;


}
