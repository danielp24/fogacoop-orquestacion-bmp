package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString

public class ResponseListUserOrg implements Serializable {
    @ApiModelProperty(notes = "Estado del proceso obtener lista de usuarios por organización")
    private Integer status;

    @ApiModelProperty(notes = "Cuerpo representativo de la respuesta del proceso obtener lista de usuarios por organización")
    private BodyListUserOrgDTO[] body;

    @ApiModelProperty(notes = "Mensaje de la respuesta del proceso obtener lista de usuarios por organización")
    private String message;
}
