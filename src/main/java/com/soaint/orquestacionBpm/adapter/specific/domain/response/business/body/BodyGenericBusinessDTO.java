package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;


import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la respuesta de GenericBusiness")
public class BodyGenericBusinessDTO {

    private String result;

}