package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa del cuerpo de la busqueda")
public class ResponseGetFechaCorteDTO extends BaseResponseAdapter<BodyResponseGetFechaCorteDTO> implements Serializable {
}
