package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyFindIdDominioDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@ApiModel(description = "Clase representativa de la tabla de parametros")
public class ResponseFindIdDominioDTO implements Serializable {

    private List<BodyFindIdDominioDTO> body;
}
