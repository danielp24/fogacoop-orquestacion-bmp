package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.opeCorreoGuarda;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la respuesta al proceso")
public class ResponseOpeCorreoDTO {

    @ApiModelProperty(notes = "Respuesta al proceso ")
    private BodyObjRtaDTO body;
}
