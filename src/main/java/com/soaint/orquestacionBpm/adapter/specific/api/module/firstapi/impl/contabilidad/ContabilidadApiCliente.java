package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.contabilidad;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad.RequestRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad.RequestConsultarEstadoComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.ResponseRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.ResponseConsultarEstadoComprobanteDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;

public interface ContabilidadApiCliente {

    ResponseRegistrarComprobanteDTO registrarComprobante(RequestRegistrarComprobanteDTO request) throws WebClientException;
    ResponseConsultarEstadoComprobanteDTO consultarEstadoComprobante(RequestConsultarEstadoComprobanteDTO request) throws WebClientException;

}
