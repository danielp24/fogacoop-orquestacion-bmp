package com.soaint.orquestacionBpm.adapter.specific.domain.request.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los valores de los parámetros")
public class RequestResolverComprobanteContableDTO implements Serializable {

    @ApiModelProperty(notes = "Id del pago a suministrar en el negocio si son comprobantes atados a pagos")
    private Integer idPago = 0;

    @ApiModelProperty(notes = "Lista de String de id de registros contables")
    private String listRegContable;

    @ApiModelProperty(notes = "Datos básicos")
    private RequestNitFechaCorteDTO basic;

    @ApiModelProperty(notes = "Código de la contabilidad")
    private String codigoContabilidad;

    @ApiModelProperty(notes = "Tipo de comprobante ")
    private String tipoComprobante;

    @ApiModelProperty(notes = "Fecha del comprobante")
    private String fecha;

    @ApiModelProperty(notes = "Plan de cuentas")
    private String planCuentas;

    @ApiModelProperty(notes = "Cuenta contable ")
    private String cuenta;

    @ApiModelProperty(notes = "Código documento soporte")
    private String codigoDocSoporte;

    @ApiModelProperty(notes = "Número documento soporte")
    private String numeroDocSoporte;

    @ApiModelProperty(notes = "Número documento soporte")
    private Integer tipoResolComprobante;

}
