package com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos;

import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los parámetros de busquedad")

public class RequestNitFechaCorteWithSecurityDTO implements Serializable {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Codigo identifacor de empresas")
    private String nit;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha del corte del pago")
    private String fechaCorte;

    @RequiredParameter
    @ApiModelProperty("Token asociados al negocio")
    private TokenDTO tokenAutenticatedBusiness;
}
