package com.soaint.orquestacionBpm.adapter.specific.domain.request.user.login;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
public class BodyDTO implements Serializable {

    private String username;
    private String password;

}
