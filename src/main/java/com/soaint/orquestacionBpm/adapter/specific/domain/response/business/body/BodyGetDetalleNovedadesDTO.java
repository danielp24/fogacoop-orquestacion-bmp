package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la información de liqidación")
public class BodyGetDetalleNovedadesDTO {

    @ApiModelProperty(notes = "Id Detalle de la novedad")
    private BigDecimal idDetalleNovedad;
    @ApiModelProperty(notes = "Fecha de registro de la novedad")
    private String fechaRegistro;
    @ApiModelProperty(notes = "Tipo de la novedad ejecutada")
    private String tipoNovedad;
    @ApiModelProperty(notes = "Fecha de inicio de la novedad")
    private String fechaInicio;
    @ApiModelProperty(notes = "Fecha final de la novedad")
    private String fechaFinal;
    @ApiModelProperty(notes = "Fecha efectiva de la novedad")
    private String fechaEfectiva;
    @ApiModelProperty(notes = "Nombre de la persona de la entidad")
    private String nombrePersDesciEntidad;
    @ApiModelProperty(notes = "Nombre de la persona del fondo")
    private String nombrePersDesciFondo;
    @ApiModelProperty(notes = "Observaciones de la novedad")
    private String observaciones;

}
