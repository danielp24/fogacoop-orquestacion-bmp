package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BodyGeneralGetListUserDTO {
    private ResponseListUser body;
    private String status;
}
