package com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)

public class RequestUpdateInstance {

    private String idInstance;
    private Integer stateInstance;

}
