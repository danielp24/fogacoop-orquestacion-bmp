package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.io.Serializable;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@ApiOperation("Clase que representa la respuesta para obtener plantilla")
public class BodyGetPlantillasDTO implements Serializable {

    @ApiModelProperty(notes = "Motivo de la Plantilla")
    private String motivo;
    @ApiModelProperty(notes = "Cuerpo HTML de la plantilla")
    private String cuerpo;
    @ApiModelProperty(notes = "Asunto del correo")
    private String asunto;
    @ApiModelProperty(notes = "Descripción del correo")
    private String descripcion;
}
