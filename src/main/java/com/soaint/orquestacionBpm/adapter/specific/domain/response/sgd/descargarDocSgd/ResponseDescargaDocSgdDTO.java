package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.descargarDocSgd;

import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.io.Serializable;

@AllArgsConstructor
@Builder
public class ResponseDescargaDocSgdDTO extends BaseResponseAdapter<BodyObjRtaDTO> implements Serializable {

}
