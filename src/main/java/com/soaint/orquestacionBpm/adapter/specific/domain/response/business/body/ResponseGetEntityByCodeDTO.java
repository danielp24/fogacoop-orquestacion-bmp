package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de los datos de la Entidad")

public class ResponseGetEntityByCodeDTO {

    private BigDecimal tipoEntidad;
    private BigDecimal nit;
    private BigDecimal digitoCheq;
    private BigDecimal clase;
    private String nombreEntidad;
    private String siglaEntidad;
    private String palabraClave;
    private String email;
    private String estadoEntidad;
    private String grupos;
    private String subGrupos;
    private BigDecimal departamento;
    private BigDecimal municipio;
    private String direccion;
    private String telefono;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaConstitucion;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaAutorizacion;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaInscripcion;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaFinInscripcion;
    private BigDecimal tipoEntidadSes;
    private String estadoEntidadSes;
    private String perfilAsociados;
}
