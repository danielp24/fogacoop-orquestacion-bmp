package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.opeCorreoGuarda;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

public class RespuestaDTO {
    private Integer idRadica;
    private String consecRadica;
}
