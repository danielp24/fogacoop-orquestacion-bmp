package com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.UserDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Clase representativa para la peticion de la operacion IniciarLotesProcesoRecaudo.")
public class RequestIniciarProcesoVerificarPagos implements Serializable {

    @RequiredParameter
    @ApiModelProperty("Datos del usuario propietario")
    private UserDto ownerUser;

    @RequiredParameter
    @ApiModelProperty("Datos Basico de solicitud")
    private RequestNitFechaCorteDTO basic;

    @RequiredPrimitiveParameter
    @ApiModelProperty("Origen de pago de la solicitud")
    private String origenPago;

    @RequiredParameter
    @ApiModelProperty("Token asociados al negocio")
    private TokenDTO tokenAutenticatedBusiness;

}
