package com.soaint.orquestacionBpm.adapter.specific.domain.request;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

public class RequestBpmDTO implements Serializable {

    private String containerId;
    private String taskInstanceId;
    private OwnerUser user;
}
