package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de la parámetros de busquedad")
public class BodyConsultarLiquidacionTotal {

    @ApiModelProperty(notes = "Valor Total a pagar de la Liquidacion")
    private BigDecimal vrPagarPsd;
}
