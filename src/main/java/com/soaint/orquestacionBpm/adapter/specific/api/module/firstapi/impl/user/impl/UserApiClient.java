package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.user.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.user.IUserApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.user.RequestListUserOrgDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.user.login.BodyDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.user.login.HeaderDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.user.login.LoginDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser.BodyGeneralGetListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser.ResponseListUser;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg.BodyGeneralListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login.ResponseUser;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class UserApiClient implements IUserApiClient {

    private RestTemplate template = new RestTemplate();

    @Value("${endpoint.orquestacion.ListUserOrg}")
    String LIST_USER_ORG;
    @Value("${api.user.token}")
    public String APLICATION_TOKEN;
    @Value("${endpoint.orquestacion.login}")
    String ENDPOINT_LOGIN;

    @Override
    public BodyGeneralListUserDTO listUserOrg(String apitoken, String organization) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = LIST_USER_ORG.concat("?apiToken=" + apitoken).concat("&organization=" + organization);

            HttpEntity<RequestListUserOrgDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), BodyGeneralListUserDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException("Error invocando listUserOrg: " + e.getMessage());
        }
    }



    @Value("${endpoint.orquestacion.getListUser}")
    String GET_LIST_USER;

    @Override
    public BodyGeneralGetListUserDTO getListUser(String apitoken) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = GET_LIST_USER.concat("?apiToken=" + apitoken);

            HttpEntity<ResponseListUser> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), BodyGeneralGetListUserDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeBadOperationGeneric, "Error invocando listUserOrg: " + e.getMessage());
        }
    }

    @Override
    public ResponseUser login(String username, String password) throws WebClientException {
        try {

            LoginDTO login = new LoginDTO();

            HeaderDTO header = new HeaderDTO();
            header.setApiToken(APLICATION_TOKEN);
            login.setHeader(header);

            BodyDTO body = new BodyDTO();
            body.setUsername(username);
            body.setPassword(password);
            login.setBody(body);

            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_LOGIN;

            HttpEntity<LoginDTO> entity = new HttpEntity<>(login, headers);
            ResponseEntity<String> responseService = null;
            responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseUser.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeBadOperationGeneric, e.getMessage() + " : " + username);
        }
    }
}
