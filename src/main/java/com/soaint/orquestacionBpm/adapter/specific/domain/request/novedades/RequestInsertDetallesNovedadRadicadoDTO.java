package com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa los paramteros a ingresar en detalles novedades")
public class RequestInsertDetallesNovedadRadicadoDTO {

    @ApiModelProperty(notes = "Id del detalle de la novedad.")
    private Integer idDetalleNovedad;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Numero radicado del documento del SGDEA.")
    private BigInteger numeroRadicado;
}
