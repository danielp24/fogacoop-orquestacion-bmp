package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BodyListUserOrgDTO implements Serializable {
    private RolUserOrg[] roles;
    private String token;
    private String id;
    private String userName;
    private String name;
    private String email;
    private String phoneNumber;
    private String organization;
    private Boolean enabled;
    private Boolean lockoutEnabled;
    private Date lockoutEndDateUtc;
    private String ldapGuid;
}

