package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;


import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyEntidadDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;


@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa del cuerpo de la busqueda")
public class ResponseCooperativaDTO extends BaseResponseAdapter<BodyEntidadDTO[]> {}
