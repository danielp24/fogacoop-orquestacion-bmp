package com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString

@ApiModel(description = "Clase representativa de la solicitud de proceso")
public class RequestGuardaDocExpedienteDTO implements Serializable {
    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Id del Expediente")
    private Integer idExpediente;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Usuario que Clasifica")
    private String usuario;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nombre del documento")
    private String nomDocumento;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Tipo de documento, depende de la subserie donde se está guardando el documento")
    private Integer idTipDoc;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Información del archivo Codificado en base 64 ")
    private String fileBase64;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha documento “yyyy-MM-dd”")
    private String fchDoc;

    @ApiModelProperty(notes = "Metadata documento")
    private List<ArrMetaDataDTO> arrMetaData;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nombre visible “Amigable” del documento.")
    private String nomDocVisible;

    @ApiModelProperty(notes = "Nombre de usuario auntenticacion")
    private String username;

    @ApiModelProperty(notes = "Contraseña de autenticacion")
    private Integer password;

}
