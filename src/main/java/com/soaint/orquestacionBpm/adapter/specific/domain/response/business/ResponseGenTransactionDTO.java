package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
public class ResponseGenTransactionDTO extends BaseResponseAdapter<ResponseGenericTransactionDTO> implements Serializable {

}
