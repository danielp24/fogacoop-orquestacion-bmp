package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;


import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import com.soaint.orquestacionBpm.commons.domains.response.business.body.BodyGenericTransactionDTO;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;


@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa del cuerpo de la busqueda")
public class ResponseResolverComprobanteContableDTO extends BaseResponseAdapter<BodyGenericTransactionDTO> {
}
