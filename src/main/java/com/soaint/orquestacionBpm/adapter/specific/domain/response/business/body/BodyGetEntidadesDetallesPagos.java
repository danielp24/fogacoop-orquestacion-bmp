package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyEntidadDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Clase representativa de la respuesta de IniciarLotesProcesoRecaudo")
public class BodyGetEntidadesDetallesPagos {

    @ApiModelProperty(notes = "nit de la cooperativa")
    private BigDecimal nit;
    @ApiModelProperty(notes = "Nombre de la cooperativa")
    private String nombre;
    @ApiModelProperty(notes = "Consecutivo del proceso para la fecha de corte actual")
    private BigDecimal consecutivoProceso;
    @ApiModelProperty(notes = "Estado de la cooperativa")
    private BigDecimal estado;
    @ApiModelProperty(notes = "Saldo de la liquidacion")
    private BigDecimal saldoLiquidacion;
    @ApiModelProperty(notes = "Saldo a Favor de la cooperativa")
    private BigDecimal saldoFavor;
    @ApiModelProperty(notes = "Saldo pendiente de la cooperativa")
    private BigDecimal saldoPendiente;
    @ApiModelProperty(notes = "Saldo de Intereses de mora de la cooperativa")
    private BigDecimal saldoInteresMora;
    @ApiModelProperty(notes = "Saldo total a pagar de la cooperativa")
    private BigDecimal totalAPagar;


}
