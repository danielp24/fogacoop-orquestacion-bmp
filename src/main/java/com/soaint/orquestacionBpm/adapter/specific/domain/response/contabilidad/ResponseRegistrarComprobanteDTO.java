package com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.body.BodyRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa del proceso de solicitar Contabilizacion en la respuesta")
public class ResponseRegistrarComprobanteDTO extends BaseResponseAdapter<BodyRegistrarComprobanteDTO> implements Serializable {
}