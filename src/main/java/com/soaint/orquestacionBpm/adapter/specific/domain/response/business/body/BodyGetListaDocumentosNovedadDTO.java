package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la información de liqidación")
public class BodyGetListaDocumentosNovedadDTO {

    @ApiModelProperty(notes = "Id Documento")
    private String idDocumento;
    @ApiModelProperty(notes = "Codigo SGDA documento")
    private String codigoDocumento;
    @ApiModelProperty(notes = "Id SGDA documento")
    private BigDecimal idDocSgdea;
    @ApiModelProperty(notes = "Fecha Inicio de documento")
    private String fechaInicio;
    @ApiModelProperty(notes = "Nombre de de documento")
    private String nombreDocumento;
    @ApiModelProperty(notes = "Ruta de de documento")
    private String rutaDocumento;
    @ApiModelProperty(notes = "Usuario clasificador de documento")
    private String usuarioClasificador;

}
