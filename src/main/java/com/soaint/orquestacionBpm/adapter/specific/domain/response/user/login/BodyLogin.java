package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class BodyLogin {

    private String message;
    private Integer status;
    private Body body;

}
