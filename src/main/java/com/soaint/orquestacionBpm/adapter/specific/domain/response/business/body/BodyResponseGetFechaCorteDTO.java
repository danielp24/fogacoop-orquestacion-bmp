package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de la respuesta de la fecha de corte actual")
public class BodyResponseGetFechaCorteDTO {

    @ApiModelProperty(notes = "Fecha de corte")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaCorte = new Date();

    @ApiModelProperty(notes = "Fecha limite de pago del corte actual")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaLimitePago = new Date();

    @ApiModelProperty(notes = "Fecha inicial y fecha final de la fecha de corte actual")
    private String periodo;
}
