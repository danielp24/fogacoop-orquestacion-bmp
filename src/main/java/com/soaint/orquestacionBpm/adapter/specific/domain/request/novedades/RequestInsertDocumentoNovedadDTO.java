package com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa los paramteros a ingresar en detalles novedades")
public class RequestInsertDocumentoNovedadDTO {

    @ApiModelProperty(notes = "Codigo del docuemnto retornado por el SGDEA.")
    private String codigoDocumento = "";

    @ApiModelProperty(notes = "Id del Docuemnto retornado por el sgdea.")
    private Integer idDocSgdea = 0;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Id del detalle de la novedad.")
    private Integer idDetalleNovedad;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha de inicio del documento.")
    private String fechaInicio = "";

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nobre de negocio atado al documento")
    private String nombreDocumento;

    @ApiModelProperty(notes = "Ruta del documento del filesystem.")
    private String rutaDocumento = "";

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Usuario clasificador del documento.")
    private String usuarioClasificador;

}
