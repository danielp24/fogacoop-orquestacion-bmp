package com.soaint.orquestacionBpm.adapter.specific.domain.response;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.RequestBpmDTO;
import lombok.Data;

@Data
public class ResponseStartProcessDTO {

    private RequestBpmDTO body;
    private String status;
    private String timeResponse;
    private String message;
    private String path;
    private String transactionState;
}
