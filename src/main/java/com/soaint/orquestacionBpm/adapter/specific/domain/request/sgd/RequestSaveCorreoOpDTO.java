package com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
@ApiModel(description = "Clase representativa de la solicitud de correo")

public class RequestSaveCorreoOpDTO {
    @ApiModelProperty(notes = "Dirección Remitente")
    private String de;

    @ApiModelProperty(notes = "Lista de direcciones de correo de destinatarios")
    private List<String> lstPara;

    @ApiModelProperty(notes = "Lista de direcciones de correo de destinatarios copiados")
    private List<String> lstParaCopia;

    @ApiModelProperty(notes = "Asunto del correo electrónico.")
    private String asunto;

    @ApiModelProperty(notes = "Cuerpo del correo electrónico")
    private String cuerpo;

    @ApiModelProperty(notes = "Lista de archivos Adjuntos en Base 64")
    private List<lstAdjuntosDTO> lstAdjuntos;

    @ApiModelProperty(notes = "Consecutivo del radicado al que se relaciona el correo electrónico")
    private String consRadicado;

    @ApiModelProperty(notes = "Código del expediente donde relaciona el correo (Si se relaciona a un expediente)")
    private String codExpediente;

    @ApiModelProperty(notes = "Nombre del separador dentro del expediente (Si se relaciona a un Expediente, y se maneja separador)")
    private String nomSeparador;

    @ApiModelProperty(notes = "Tipo de notificación")
    private String idTipNotifica;

    @ApiModelProperty(notes = "Usuario que remite correo")
    private String usuario;
    //estos dos siguientes son para consumir autenticación
    @ApiModelProperty(notes = "Nombre de usuario auntenticacion")
    private String username;

    @ApiModelProperty(notes = "contraseña del usuario")
    private String password;
}
