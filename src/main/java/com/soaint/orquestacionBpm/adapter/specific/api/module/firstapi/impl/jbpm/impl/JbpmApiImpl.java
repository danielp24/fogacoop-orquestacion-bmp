package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.jbpm.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.manager.impl.EndpointManagerAbstract;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.jbpm.JbpmApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.infrastructure.EndpointConfig;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessRequestDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessResponseDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessesInstances;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)

public class JbpmApiImpl extends EndpointManagerAbstract implements JbpmApiCliente {

    @Autowired
    private ObjectMapper mapper;
    private RestTemplate template = new RestTemplate();

    public JbpmApiImpl(EndpointConfig endpointConfig) {
        super(endpointConfig);
    }

    @Value("${endpoint.api.jbpm.process.startProcessInstance}")
    public String ENDPOINT_INICIAR_INSTANCIA;

    @Value("${endpoint.api.jbpm.process.deleteProcessIntance}")
    public String ENDPOINT_ELIMINAR_INSTANCIA;

    @Value("${endpoint.api.jbpm.process.terminateProcessIntance}")
    public String ENDPOINT_TERMINAR_INSTANCIA;

    @Value("${endpoint.api.jbpm.process.startSignalIntance}")
    public String ENDPOINT_INICIAR_SIGNAL;

    @Value("${endpoint.api.jbpm.process.consultarSubprocesosByIdProcess}")
    public String ENDPOINT_CONSULTAR_SUB_PROCESS_BY_ID_PROCESS;

    @Value("${endpoint.api.jbpm.process.instancesByVariable}")
    public String ENDPOINT_INSTANCE_BY_VARIABLE;

    @Value("${endpoint.api.jbpm.process.getInstanceByProcessId}")
    public String ENDPOINT_GET_PROCESS_INSTANCE_BY_PROCESS_ID;



    @Override
    public ProcessResponseDto iniciarInstanciaProceso(ProcessRequestDto procesoRecaudoRq) throws WebClientException {

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_INICIAR_INSTANCIA;

            Gson gson = new Gson();
            HttpEntity<ProcessRequestDto> entity = new HttpEntity<>(procesoRecaudoRq, headers);
            String responseService = template.postForObject(url, entity, String.class);

            return gson.fromJson(responseService, ProcessResponseDto.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException,e.getMessage());
        }
    }

    @Override
    public ProcessResponseDto eliminarInstancia(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_ELIMINAR_INSTANCIA;

            Gson gson = new Gson();
            HttpEntity<ProcessRequestDto> entity = new HttpEntity<>(procesoRecaudoRq, headers);
            ResponseEntity<String> responseService = template.exchange(url,  HttpMethod.DELETE, entity ,String.class);

            return gson.fromJson(responseService.getBody(), ProcessResponseDto.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException,e.getMessage());
        }
    }

    @Override
    public ProcessResponseDto iniciarSignal(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_INICIAR_SIGNAL;

            Gson gson = new Gson();
            HttpEntity<ProcessRequestDto> entity = new HttpEntity<>(procesoRecaudoRq, headers);
            String responseService = template.postForObject(url, entity, String.class);

            return gson.fromJson(responseService, ProcessResponseDto.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public ProcessResponseDto terminarInstancia(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_TERMINAR_INSTANCIA;

            Gson gson = new Gson();
            HttpEntity<ProcessRequestDto> entity = new HttpEntity<>(procesoRecaudoRq, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.DELETE, entity, String.class);

            return gson.fromJson(responseService.getBody(), ProcessResponseDto.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public ProcessesInstances consultarSubprocesosByIdProcess(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_CONSULTAR_SUB_PROCESS_BY_ID_PROCESS;

            Gson gson = new Gson();
            HttpEntity<ProcessRequestDto> entity = new HttpEntity<>(procesoRecaudoRq, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return gson.fromJson(responseService.getBody(), ProcessesInstances.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }


    @Override
    public ProcessResponseDto instancesByVariable(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_INSTANCE_BY_VARIABLE;

            Gson gson = new Gson();
            HttpEntity<ProcessRequestDto> entity = new HttpEntity<>(procesoRecaudoRq, headers);
            String responseService = template.postForObject(url, entity, String.class);

            return gson.fromJson(responseService, ProcessResponseDto.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public Map<String, Object> getInstanceByProcessId(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_GET_PROCESS_INSTANCE_BY_PROCESS_ID;
            System.out.println("url a consumir--->" + url);
            Gson gson = new Gson();
            //System.out.println("rq que llega para ir a consumir--->" + gson.toJson(procesoRecaudoRq));
            ProcessRequestDto dto = new ProcessRequestDto();
            dto.setContainerId(procesoRecaudoRq.getContainerId());
            dto.setOwnerUser(procesoRecaudoRq.getOwnerUser());
            dto.setProcessesId(procesoRecaudoRq.getProcessInstance());
            System.out.println("dto a enviar---->"+ gson.toJson(dto));


            HttpEntity<ProcessRequestDto> entity = new HttpEntity<>(dto, headers);
            String responseService = template.postForObject(url, entity, String.class);
           // System.out.println("response servicio getInstanceByProcessID-->" +responseService);
            return gson.fromJson(responseService, Map.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }
}
