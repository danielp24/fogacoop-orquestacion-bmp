package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.contabilidad.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.manager.impl.EndpointManagerAbstract;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.contabilidad.ContabilidadApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad.RequestRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad.RequestConsultarEstadoComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.ResponseRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.ResponseConsultarEstadoComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.infrastructure.EndpointConfig;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)

public class ContabilidadApiImpl extends EndpointManagerAbstract implements ContabilidadApiCliente {

    @Autowired
    private ObjectMapper mapper;
    private RestTemplate template = new RestTemplate();

    public ContabilidadApiImpl(EndpointConfig endpointConfig) {
        super(endpointConfig);
    }

    @Value("${endpoint.api.contabilidad.registrarComprobante}")
    public String ENDPOINT_SOLICITAR_CONTABILIZACION;

    @Value("${endpoint.api.contabilidad.consultarEstadoComprobante}")
    public String ENDPOINT_SOLICITAR_ESTADO_CONTABILIZACION;




    @Override
    public ResponseRegistrarComprobanteDTO registrarComprobante(RequestRegistrarComprobanteDTO request) throws WebClientException {
        try {
            System.out.println("Entra al consumo de registrarComprobante");
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_SOLICITAR_CONTABILIZACION;

            Gson gson = new Gson();
            HttpEntity<RequestRegistrarComprobanteDTO> entity = new HttpEntity<>(request, headers);
            String responseService = template.postForObject(url, entity, String.class);

            System.out.println("Sale de consumo registrarComprobante");
            return gson.fromJson(responseService, ResponseRegistrarComprobanteDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException,e.getMessage());
        }
    }

    @Override
    public ResponseConsultarEstadoComprobanteDTO consultarEstadoComprobante(RequestConsultarEstadoComprobanteDTO request) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_SOLICITAR_ESTADO_CONTABILIZACION;

            Gson gson = new Gson();
            HttpEntity<RequestConsultarEstadoComprobanteDTO> entity = new HttpEntity<>(request, headers);
            String responseService = template.postForObject(url, entity, String.class);

            return gson.fromJson(responseService, ResponseConsultarEstadoComprobanteDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException,e.getMessage());
        }
    }
}
