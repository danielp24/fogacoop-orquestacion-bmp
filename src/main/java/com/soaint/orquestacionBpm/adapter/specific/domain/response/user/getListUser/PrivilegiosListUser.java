package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class PrivilegiosListUser {

    private String id;
    private String name;
    private String code;
    private boolean enabled;
}
