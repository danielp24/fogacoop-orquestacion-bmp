package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.descargarDocSgd;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

public class RespuestaDTO {

    private Integer idSoporte;

    private String codigo;

    private String nombre;

    private String fileBase64;

    private String nomDocVisible;

    private Integer idTipoDoc;

    private String nomTipDoc;
}
