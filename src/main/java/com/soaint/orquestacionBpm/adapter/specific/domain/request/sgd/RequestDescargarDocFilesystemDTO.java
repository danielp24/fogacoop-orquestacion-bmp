package com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@ApiModel(description = "Clase representativa de descarga documento")
public class RequestDescargarDocFilesystemDTO implements Serializable {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Ruta del archivo")
    private String rutaArchivo;


}
