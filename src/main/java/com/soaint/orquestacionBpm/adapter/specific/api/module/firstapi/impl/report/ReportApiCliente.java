package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.report;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.report.RequestFR71DTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.report.ResponseReportDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;

public interface ReportApiCliente {

    ResponseReportDTO dataReportConsume(RequestFR71DTO requestFR71DTO) throws WebClientException;
}
