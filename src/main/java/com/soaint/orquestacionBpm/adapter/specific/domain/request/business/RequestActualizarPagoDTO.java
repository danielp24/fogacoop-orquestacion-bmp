package com.soaint.orquestacionBpm.adapter.specific.domain.request.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los parámetros de GenericBusiness")
public class RequestActualizarPagoDTO extends TokenDTO implements Serializable {

    private String nroTransaccion;
    private String tipoPago;
    private String estadoPago;
}
