package com.soaint.orquestacionBpm.adapter.specific.domain.response.jbpm.recaudo;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetEntidadesDetallesPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetResolverNovedadEntidadLote;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Clase representativa de la respuesta de IniciarLotesProcesoRecaudo")
public class ResponseIniciarLotesProcesoRecaudo {

    @ApiModelProperty(notes = "Lista de entidades cuyas condiciones se encuentran ejecutadas para este lanzamiento del servicio.")
    private Set<BodyGetEntidadesDetallesPagos> entidadesProcesosEjecutados;
    @ApiModelProperty(notes = "Lista de entidades que se encuentran con el proceso activo para la fecha de corte actual.")
    private Set<BodyGetEntidadesDetallesPagos> entidadesPreviamenteEjecutados;
    @ApiModelProperty(notes = "Lista de entidades que aun no han sido ejecutadas para la fecha de corte actual")
    private Set<BodyGetEntidadesDetallesPagos> entidadesSinEjecutar;
    @ApiModelProperty(notes = "Lista de entidades se le han aplicado novedad de abono")
    private Set<BodyGetResolverNovedadEntidadLote> entidadesAplicacionAbono;

}
