package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BodyListUser {

    private RolListUser[] roles;
    private boolean acceptedHabeasData;
    private boolean acceptedTermsAndConditions;
    private String token;
    private String id;
    private String userName;
    private String name;
    private String email;
    private String phoneNumber;
    private String organization;
    private Boolean enabled;
    private Boolean lockoutEnabled;
    private String lockoutEndDateUtc;
    private String ldapGuid;
}
