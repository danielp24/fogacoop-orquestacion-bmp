package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetTrazabilidadNotificacionDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Builder
@ApiOperation("Clase que representa la respuesta para obtener trazabilidad notificacion")
public class ResponseTrazabilidadNotificacionDTO extends BaseResponseAdapter<List<BodyGetTrazabilidadNotificacionDTO>> implements Serializable {
}
