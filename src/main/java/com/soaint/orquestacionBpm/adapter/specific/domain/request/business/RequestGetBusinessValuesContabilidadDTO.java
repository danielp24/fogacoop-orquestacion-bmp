package com.soaint.orquestacionBpm.adapter.specific.domain.request.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los parámetros de Contactos")
public class RequestGetBusinessValuesContabilidadDTO {

    @RequiredParameter
    @ApiModelProperty(notes = "Datos básicos")
    private RequestNitFechaCorteDTO basic;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Id padre de los parametros de negocios a consultar")
    private Integer idParentBusinessParam;

    @ApiModelProperty(notes = "Id del pago a suministrar en el negocio si son comprobantes atados a pagos")
    private Integer idPago = 0;

}
