package com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)

public class RequestInsertInstance {

    @RequiredPrimitiveParameter
    private String idInstance;
    @RequiredPrimitiveParameter
    private Integer sequenceInstance;
    @RequiredPrimitiveParameter
    private String fechaCorte;
    @RequiredPrimitiveParameter
    private String nit;
    @RequiredPrimitiveParameter
    private Integer stateInstance;

}
