package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de la parámetros de busquedad")
public class BodyResponseGetInfoBasica implements Serializable {

    @ApiModelProperty(notes = "Fecha de Registro del contacto")
    private String fechaRegistro;
    @ApiModelProperty(notes = "Tipo de la Entidad")
    private String tipoEntidad;
    @ApiModelProperty(notes = "Codigo identifacor de la Entidad")
    private BigDecimal codigoEntidad;
    @ApiModelProperty(notes = "Nombre de la Entidad")
    private String nombreEntidad;
    @ApiModelProperty(notes = "ID Tipo de identificacion del contacto")
    private String idTipoIdentContacto;
    @ApiModelProperty(notes = "Tipo de identificacion del contacto")
    private String tipoIdentContacto;
    @ApiModelProperty(notes = "Numero de identificacion del contacto")
    private String numeroIdentContacto;
    @ApiModelProperty(notes = "Nombre de Contacto de la persona encargada de la Entidad")
    private String nombresContacto;
    @ApiModelProperty(notes = "Area del contacto")
    private String area;
    @ApiModelProperty(notes = "Cargo del contacto")
    private String cargo;
    @ApiModelProperty(notes = "Correo Electrónico de contacto")
    private String correoElectronico;
    @ApiModelProperty(notes = "Direccion del contacto")
    private String direccion;
    @ApiModelProperty(notes = "Departamento del contacto")
    private String departamento;
    @ApiModelProperty(notes = "Ciudad del contacto")
    private String municipio;
    @ApiModelProperty(notes = "Telefonos de la Entidad")
    private String telefonos;
}
