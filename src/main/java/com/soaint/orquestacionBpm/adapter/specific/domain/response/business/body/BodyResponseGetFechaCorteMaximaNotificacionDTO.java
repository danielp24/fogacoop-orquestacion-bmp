package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa del retorno del periodo actual")
public class BodyResponseGetFechaCorteMaximaNotificacionDTO {

    private String fechaCorte;
}
