package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.guardarDocExpediente;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

public class RespuestaDTO {
    private Integer idDocumento;
    private String codigo;
}
