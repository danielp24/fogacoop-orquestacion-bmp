package com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos;

import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de consultar los pagos")

public class RequestConsultarPagosDTO extends RequestNitFechaCorteDTO implements Serializable {

    @ApiModelProperty(notes = "Estado de los pagos")
    private String estado = "NA";

    @RequiredParameter
    @ApiModelProperty(notes = "Token de autenticación")
    private TokenDTO tokenAutenticatedBusiness;


}
