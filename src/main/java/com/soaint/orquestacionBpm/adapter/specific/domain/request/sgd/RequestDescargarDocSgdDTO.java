package com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@ApiModel(description = "Clase representativa de descarga documento")
public class RequestDescargarDocSgdDTO implements Serializable {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nombre usuario autenticación")
    private String username;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "contraseña autenticación")
    private Integer password;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Id del documento soporte SGD")
    private String idSoporte;


}
