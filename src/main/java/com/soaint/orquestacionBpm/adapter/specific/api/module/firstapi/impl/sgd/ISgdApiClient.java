package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.*;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.descargarDocSgd.ResponseDescargaDocSgdDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.ResponseDescargarDocFilesystemDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.ResponseGuardarDocFilesystemDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.guardarDocExpediente.ResponseGuardarDocExpedienteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.opeCorreoGuarda.ResponseOpeCorreoDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;

public interface ISgdApiClient {

    ResponseOpeCorreoDTO opeCorreoGuarda(RequestSaveCorreoOpDTO correoOPDTO, String authorizationJwt) throws WebClientException;

    ResponseGuardarDocFilesystemDTO guardarDocumento(RequestGuardarDocFilesystemDTO rqGuardarDoc, String authorizationJwt) throws WebClientException;

    ResponseGuardarDocExpedienteDTO guardaDocExpediente(RequestGuardaDocExpedienteDTO rqGuardarDoc, String authorizationJwt) throws WebClientException;

    ResponseDescargarDocFilesystemDTO descargarDocFilesystem(RequestDescargarDocFilesystemDTO idDetalleNovedad, String authorizationJwt) throws WebClientException;

    ResponseDescargaDocSgdDTO descargarDocSgd(RequestDescargarDocSgdDTO idDetalleNovedad, String authorizationJwt) throws WebClientException;

}
