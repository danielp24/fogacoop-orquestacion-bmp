package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class RolListUser {

    private String codeAuthApplication;
    private String nameAuthApplication;
    private String id;
    private String description;
    private String name;
    private boolean enabled;
    private PrivilegiosListUser[] privileges;
}
