package com.soaint.orquestacionBpm.adapter.specific.domain.request.comunicaciones;


import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los parámetros de notificacion")
public class RequestGetDestinatariosDTO {
    private BigDecimal idPlantilla;
    private BigDecimal nit;

}
