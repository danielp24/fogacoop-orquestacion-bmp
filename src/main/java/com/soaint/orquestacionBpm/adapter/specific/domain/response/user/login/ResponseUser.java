package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login;


import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
public class ResponseUser extends BaseResponseAdapter<BodyLogin> {

}
