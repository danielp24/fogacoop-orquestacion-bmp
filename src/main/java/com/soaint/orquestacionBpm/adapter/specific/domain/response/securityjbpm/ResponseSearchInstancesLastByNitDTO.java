package com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
//@JsonNaming(PropertyNamingStrategy..class)
@MatchAllAtributes
@ToString(includeFieldNames = true)
public class ResponseSearchInstancesLastByNitDTO implements Serializable {

    private String idInstance;
    private BigDecimal consecutivoProceso;
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date fechaCorte;
    private String nit;
    private BigDecimal state;

    @Override
    public String toString() {
        return "ResponseSearchInstancesLastByNitDTO{" +
                "idInstance='" + idInstance + '\'' +
                ", fechaCorte=" + fechaCorte +
                ", nit='" + nit + '\'' +
                ", state=" + state +
                '}';
    }
}
