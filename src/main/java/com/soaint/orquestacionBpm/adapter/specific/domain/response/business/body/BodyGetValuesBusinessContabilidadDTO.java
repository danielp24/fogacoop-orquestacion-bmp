package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los valores de los parámetros")
public class BodyGetValuesBusinessContabilidadDTO implements Serializable {

    @ApiModelProperty(notes = "Lista dinamica de llaves ordenadas")
    private String keyListStringResult;

    @ApiModelProperty(notes = "Lista dinamica de valores ordenados")
    private String valueListStringResult;


}
