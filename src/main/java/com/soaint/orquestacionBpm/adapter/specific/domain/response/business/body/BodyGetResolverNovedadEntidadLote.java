package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa del los abonos aplicados")

public class BodyGetResolverNovedadEntidadLote {

    @ApiModelProperty(notes = "Tipo de la novedad ejecutada")
    private String numeroIdentificacion;
    @ApiModelProperty(notes = "Id Detalle de la novedad")
    private BigDecimal valorAbonoAplicado;

}
