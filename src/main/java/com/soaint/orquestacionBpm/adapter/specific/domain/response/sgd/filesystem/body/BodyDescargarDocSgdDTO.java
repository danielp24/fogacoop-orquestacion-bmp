package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@ApiOperation("Clase que representa la respuesta el detalle de los pagos")
public class BodyDescargarDocSgdDTO {

    @ApiModelProperty(notes = "Ruta de archivo almacenado")
    private String rutaArchivo;

}
