package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa del retorno del periodo actual")
public class BodyGetPeriodoActualDTO implements Serializable {

    @ApiModelProperty(notes = "Periodo actual que dependiendo de el tipo de periodo mensual, bimestral, semestral, etc.")
    private BigDecimal periodoActual;

}
