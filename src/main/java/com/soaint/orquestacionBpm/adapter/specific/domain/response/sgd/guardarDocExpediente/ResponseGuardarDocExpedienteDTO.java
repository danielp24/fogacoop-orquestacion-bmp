package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.guardarDocExpediente;

import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.io.Serializable;


@AllArgsConstructor
@Builder
@ApiOperation("Clase que representa la respuesta para el guardado de los documentos al SGD al expediente")
public class ResponseGuardarDocExpedienteDTO extends BaseResponseAdapter<BodyGuardarDocExpedienteDTO> implements Serializable {

}
