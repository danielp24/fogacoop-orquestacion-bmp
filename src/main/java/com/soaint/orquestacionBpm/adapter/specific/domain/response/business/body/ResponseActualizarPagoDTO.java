package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;


import com.soaint.orquestacionBpm.adapter.specific.domain.request.business.RequestActualizarPagoDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa del la respuesta de la actualziacion de datos")
public class ResponseActualizarPagoDTO extends BaseResponseAdapter<RequestActualizarPagoDTO> {

}
