package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@ApiOperation("Clase que representa la respuesta el detalle de los pagos")
public class BodyGetDetallePagosDTO {

    @ApiModelProperty(notes = "Id de Pago")
    private BigDecimal idPago;
    @ApiModelProperty(notes = "Codigo de Forma de Pago")
    private BigDecimal codigoFormaPago;
    @ApiModelProperty(notes = "Nombre de Forma de Pago")
    private String nombreFormaPago;
    @ApiModelProperty(notes = "Codigo de Banco")
    private BigDecimal codigoBanco;
    @ApiModelProperty(notes = "Nombre del banco")
    private String nombreBanco;
    @ApiModelProperty(notes = "Codigo de Banco Destino")
    private BigDecimal codigoBancoDestino;
    @ApiModelProperty(notes = "Nombre del banco")
    private String nombreBancoDestino;
    @ApiModelProperty(notes = "Numero de Cheque/Transaccion")
    private String nroChequeTransac;
    @ApiModelProperty(notes = "Codigo del Departamento")
    private BigDecimal codigoDepto;
    @ApiModelProperty(notes = "Nombre del Departamento")
    private String nombreDepto;
    @ApiModelProperty(notes = "Codigo de Ciudad")
    private BigDecimal codigoCiudad;
    @ApiModelProperty(notes = "Nombre de Ciudad")
    private String nombreCiudad;
    @ApiModelProperty(notes = "Fecha del pago")
    private String fechaPago;
    @ApiModelProperty(notes = "Valor pagado")
    private BigDecimal valorPagado;
    @ApiModelProperty(notes = "Observaciones")
    private String observaciones;
    @ApiModelProperty(notes = "Pago aplicado en el sistema")
    private String aplicado;
    @ApiModelProperty(notes = "Estado del pago frente a la entidad bancaria")
    private BigDecimal codigoEstadoPago;
    @ApiModelProperty(notes = "Pago aplicado en el sistema")
    private String nombreEstadoPago;
    @ApiModelProperty(notes = "ID del ticket de la tarsaccion (Aplica solo para PSE)")
    private String idTicket;
}
