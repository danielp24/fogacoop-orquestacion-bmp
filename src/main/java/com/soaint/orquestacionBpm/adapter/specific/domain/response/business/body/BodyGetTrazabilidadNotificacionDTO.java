package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de los valores que traera la Trazabilidad")
public class BodyGetTrazabilidadNotificacionDTO {

    private String numeroIdentificacion;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaCorte;
    private BigDecimal idPlantilla;

}
