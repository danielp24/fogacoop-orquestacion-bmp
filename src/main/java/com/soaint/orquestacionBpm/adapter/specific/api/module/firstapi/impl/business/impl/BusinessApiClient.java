package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.manager.impl.EndpointManagerAbstract;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.business.*;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.comunicaciones.RequestGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDetallesNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDetallesNovedadRadicadoDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDocumentoNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestConsultarPagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteWithSecurityDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.*;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseActualizarPagoDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseDataEntidadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseGetFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones.ResponseGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.adapter.specific.infrastructure.EndpointConfig;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.RequestGenericJsonDTO;
import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGeNombresContactoDTO;
import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGetPosLaboralDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import com.soaint.orquestacionBpm.commons.domains.response.business.ResponseGenericTransactionWResDTO;
import com.soaint.orquestacionBpm.commons.domains.response.cooperativa.*;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class BusinessApiClient extends EndpointManagerAbstract implements IBusinessApiClient {

    private RestTemplate template = new RestTemplate();

    public BusinessApiClient(EndpointConfig endpointConfig) {
        super(endpointConfig);
    }

    @Value("${endpoint.orquestacion.consultarLista}")
    public String ENDPOINT_CONSULTAR_LISTA;

    @Value("${endpoint.orquestacion.getEntityByCode}")
    public String ENDPOINT_GET_ENTITY_BY_CODE;

    @Value("${endpoint.orquestacion.getDominioContable}")
    public String ENDPOINT_GET_DOMINIO_CONTABLE;

    @Value("${endpoint.orquestacion.getBusinessValuesContabilidad}")
    public String ENDPOINT_FILL_GEN_BUSINESS_CONTABILIDAD;

    @Value("${endpoint.orquestacion.getResolverComprobanteContable}")
    public String ENDPOINT_RESOLV_COMPROBANTE_CONTABLE;

    @Value("${endpoint.orquestacion.obtenerPlantilla}")
    public String ENDPOINT_OBTENER_PLANTILLA;

    @Value("${endpoint.orquestacion.informacionBasica}")
    public String ENDPOINT_GET_INFO_BASICA;

    @Value("${endpoint.orquestacion.consultarPagos}")
    public String ENDPOINT_GET_PAGOS;

    @Value("${endpoint.orquestacion.businessParameterTemplate}")
    String BUSINESS_PARAMETER_TEMPLATE;

    @Value("${endpoint.orquestacion.getLiquidacionTotal}")
    public String ENDPOINT_GET_LIQUIDACION_TOTAL;

    @Value("${endpoint.orquestacion.getEntidadesLiq}")
    public String ENDPOINT_GET_ENTIDADES;

    @Value("${endpoint.orquestacion.consultarDetallePagos}")
    public String ENDPOINT_GET_DETALLE_PAGOS;

    @Value("${endpoint.orquestacion.actualizarPago}")
    public String ENDPOINT_UPDATE_PAGO;

    @Value("${endpoint.orquestacion.authenticate}")
    public String ENDPOINT_AUTHENTICATE;

    @Value("${endpoint.orquestacion.getPeriodoActual}")
    public String ENDPOINT_GET_PERIODO_ACTUAL;

    @Value("${endpoint.orquestacion.getDestinatarios}")
    public String ENDPOINT_GET_DESTINATARIOS;

    @Value("${endpoint.orquestacion.getEntidadesDetallesPagos}")
    public String ENDPOINT_GET_ENTIDADES_DETALLES_PAGOS;

    @Value("${endpoint.orquestacion.getResolverNovedadEntidadLote}")
    public String ENDPOINT_GET_RESOLVER_NOVEDAD_ENTIDAD_LOTE;

    @Value("${endpoint.orquestacion.getDetalleLiquidacion}")
    public String ENDPOINT_GET_DETALLE_LIQUIDACION;

    @Value("${endpoint.orquestacion.getTrazabilidadNotificacion}")
    public String ENDPOINT_GET_TRAZABILIDAD_NOTIFICACION;

    @Value("${endpoint.orquestacion.getEntidadesInscritas}")
    public String ENDPOINT_GET_ENTIDADES_INSCRITAS;

    @Value("${endpoint.orquestacion.getFechaCorte}")
    public String ENDPOINT_GET_FECHA_CORTE;

    @Value("${endpoint.orquestacion.getFechaCorteMaxima}")
    public String ENDPOINT_GET_FECHA_CORTE_MAXIMA_NOTIFICACION;

    //Novedades

    @Value("${endpoint.orquestacion.insertDetalleNovedades}")
    public String INSERT_DETALLE_NOVEDADES;

    @Value("${endpoint.orquestacion.insertDocumentoNovedad}")
    public String INSERT_DOCUMENTO_NOVEDAD;

    @Value("${endpoint.orquestacion.insertDetalleNovedadesRadicado}")
    public String INSERT_DETALLE_NOVEDADES_RADICADO;

    @Value("${endpoint.orquestacion.getDetalleNovedades}")
    public String GET_DETALLES_NOVEDADES;

    @Value("${endpoint.orquestacion.getListaDocumentosNovedad}")
    public String GET_DOCUMENTOS_NOVEDADES;

    @Value("${endpoint.orquestacion.getTelefonosContacto}")
    public String GET_TELEFONOS_CONTACTO;

    @Value("${endpoint.orquestacion.getDireccionesContacto}")
    public String GET_DIRECCIONES_CONTACTO;

    @Value("${endpoint.orquestacion.getCorreosContacto}")
    public String GET_CORREOS_CONTACTO;

    @Value("${endpoint.orquestacion.getPosicionLaboralContacto}")
    public String GET_POS_LAB_CONTACTO;

    @Value("${endpoint.orquestacion.getNombresContacto}")
    public String GET_NOMBRES_CONTACTO;

    @Value("${endpoint.orquestacion.getResolverNotificacionInscritas}")
    public String RESOLVER_NOTIFICACION_INSCRITAS;


    @Value("${bearer.token}")
    public String BEARER;


    @Override
    public ResponseFindIdDominioDTO findIdDominio(String idDominio, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_CONSULTAR_LISTA + idDominio;

            HttpEntity<ResponseFindIdDominioDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseFindIdDominioDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, " Error Invocando servicio de Obtener datos por Dominio: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetPlantillasDTO getPlantilla(String idPlantilla, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_OBTENER_PLANTILLA + idPlantilla;

            HttpEntity<ResponseGetPlantillasDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseGetPlantillasDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando la plantilla: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetInfoBasicaDTO getInfoBasica(String nit, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_INFO_BASICA + nit;

            HttpEntity<ResponseGetInfoBasicaDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseGetInfoBasicaDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando informacion basica: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetPagosDTO getPagos(RequestConsultarPagosDTO dto, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_PAGOS;
            List<ResponseGetPagosDTO> list = new ArrayList<ResponseGetPagosDTO>();

            HttpEntity<RequestConsultarPagosDTO> entity = new HttpEntity<>(dto, headers);

            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetPagosDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando Pagos: " + e.getMessage());

        }
    }

    @Override
    public ResponseGenericBusinessDTO businessParameterTemplate(RequestBusinessDTO dto, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = BUSINESS_PARAMETER_TEMPLATE;

            HttpEntity<RequestBusinessDTO> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGenericBusinessDTO.class);

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando Business Parameter: " + e.getMessage());
        }
    }

    @Override
    public ResponseConsultarLiquidacionTotalDTO getLiquidacionTotal(RequestNitFechaCorteDTO dto, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_LIQUIDACION_TOTAL;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseConsultarLiquidacionTotalDTO.class);

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando liquidacionTotal: " + e.getMessage());
        }
    }


    @Override
    public ResponseCooperativaDTO getEntidadesLiq(RequestNitFechaCorteDTO dto, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_ENTIDADES;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseCooperativaDTO.class);

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando getDetallePagos: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetDetallePagosDTO getDetallePagos(RequestConsultarPagosDTO dto, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_DETALLE_PAGOS;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetDetallePagosDTO.class);

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando getEntidadesLiq: " + e.getMessage());
        }
    }

    @Override
    public ResponseActualizarPagoDTO updatePago(RequestActualizarPagoDTO dto, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_UPDATE_PAGO;

            HttpEntity<RequestActualizarPagoDTO> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.PUT, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseActualizarPagoDTO.class);

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando actualizacion pagos: " + e.getMessage());
        }
    }

    @Override
    public ResponseAuthenticateDTO authenticate(RequestAuthtenticateDTO dto) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            String url = ENDPOINT_AUTHENTICATE;

            HttpEntity<RequestAuthtenticateDTO> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseAuthenticateDTO.class);

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }


    @Override
    public ResponseGetPeriodoActualDTO getPeriodoActual(TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_PERIODO_ACTUAL;

            HttpEntity<RequestAuthtenticateDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetPeriodoActualDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetDestinatariosDTO getDestinatarios(RequestGetDestinatariosDTO request, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_DESTINATARIOS;

            HttpEntity<RequestGetDestinatariosDTO> entity = new HttpEntity<>(request, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetDestinatariosDTO.class);

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    public ResponseGetEntidadesDetallesPagos getEntidadesDetallesPago(String fechaCorte, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_ENTIDADES_DETALLES_PAGOS + fechaCorte;

            HttpEntity<RequestAuthtenticateDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetEntidadesDetallesPagos.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }

    }

    @Override
    public ResponseGetResolverNovedadEntidadLoteDTO getResolverNovedadEntidadLote(String fechaCorte, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_RESOLVER_NOVEDAD_ENTIDAD_LOTE + fechaCorte;

            HttpEntity<RequestAuthtenticateDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetResolverNovedadEntidadLoteDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetDetalleLiquidacion getDetalleLiquidacion(RequestNitFechaCorteDTO dto, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_DETALLE_LIQUIDACION;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(dto, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetDetalleLiquidacion.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGenericTransactionWResDTO insertDetalleNovedades(RequestInsertDetallesNovedadDTO insert, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = INSERT_DETALLE_NOVEDADES;

            HttpEntity<RequestInsertDetallesNovedadDTO> entity = new HttpEntity<>(insert, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGenericTransactionWResDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGenericTransactionDTO insertDetalleNovedadesRadicado(RequestInsertDetallesNovedadRadicadoDTO insert, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = INSERT_DETALLE_NOVEDADES_RADICADO;

            HttpEntity<RequestInsertDetallesNovedadRadicadoDTO> entity = new HttpEntity<>(insert, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGenericTransactionDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGenericTransactionDTO insertDocumentoNovedad(RequestInsertDocumentoNovedadDTO insert, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = INSERT_DOCUMENTO_NOVEDAD;

            HttpEntity<RequestInsertDocumentoNovedadDTO> entity = new HttpEntity<>(insert, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGenericTransactionDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetDetalleNovedadesDTO getDetalleNovedades(RequestNitFechaCorteWithSecurityDTO rq, TokenDTO token) throws WebClientException {
        try {
            RequestNitFechaCorteDTO rqs = new RequestNitFechaCorteDTO();
            rqs.setNit(rq.getNit());
            rqs.setFechaCorte(rq.getFechaCorte());
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = GET_DETALLES_NOVEDADES;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(rqs, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetDetalleNovedadesDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetListaDocumentosNovedadDTO getListaDocumentosNovedad(Integer idDetalleNovedad, TokenDTO token) throws WebClientException {
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = GET_DOCUMENTOS_NOVEDADES + idDetalleNovedad;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetListaDocumentosNovedadDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetNombresDTO getNombresContacto(RequestGeNombresContactoDTO rq, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = GET_NOMBRES_CONTACTO;

            HttpEntity<RequestGeNombresContactoDTO> entity = new HttpEntity<>(rq, headers);

            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetNombresDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando getNombresContacto: " + e.getMessage());

        }
    }

    @Override
    public ResponseGetPosicionLaboralContactoDTO getPosicionLaboralContacto(RequestGetPosLaboralDTO rq, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = GET_POS_LAB_CONTACTO;

            HttpEntity<RequestGetPosLaboralDTO> entity = new HttpEntity<>(rq, headers);

            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetPosicionLaboralContactoDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando getNombresContacto: " + e.getMessage());

        }
    }

    @Override
    public ResponseGetCorreosDTO getCorreosContacto(String numeroIdent, TokenDTO token) throws WebClientException {
        ResponseGetCorreosDTO res = new ResponseGetCorreosDTO();
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = GET_CORREOS_CONTACTO + numeroIdent;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetCorreosDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            BaseResponseAdapter rs = new Gson().fromJson(e.getResponseBodyAsString(), BaseResponseAdapter.class);
            if (rs.getBusinessStatus().equals(BusinessException.codeSizeIsEmpty)) {
                res.setBusinessStatus(BusinessException.codeSizeIsEmpty);
                return res;
            }
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetDireccionDTO getDireccionesContacto(String numeroIdent, TokenDTO token) throws WebClientException {
        ResponseGetDireccionDTO res = new ResponseGetDireccionDTO();
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = GET_DIRECCIONES_CONTACTO + numeroIdent;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetDireccionDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            BaseResponseAdapter rs = new Gson().fromJson(e.getResponseBodyAsString(), BaseResponseAdapter.class);
            if (rs.getBusinessStatus().equals(BusinessException.codeSizeIsEmpty)) {
                res.setBusinessStatus(BusinessException.codeSizeIsEmpty);
                return res;
            }
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetTelefonosDTO getTelefonosContacto(String numeroIdent, TokenDTO token) throws WebClientException {
        ResponseGetTelefonosDTO res = new ResponseGetTelefonosDTO();
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = GET_TELEFONOS_CONTACTO + numeroIdent;

            HttpEntity<RequestNitFechaCorteDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGetTelefonosDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            BaseResponseAdapter rs = new Gson().fromJson(e.getResponseBodyAsString(), BaseResponseAdapter.class);
            if (rs.getBusinessStatus().equals(BusinessException.codeSizeIsEmpty)) {
                res.setBusinessStatus(BusinessException.codeSizeIsEmpty);
                return res;
            }
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseFindIdDominioDTO getDominioContable(String nombreSetComprobante, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_DOMINIO_CONTABLE + nombreSetComprobante;

            HttpEntity<ResponseFindIdDominioDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseFindIdDominioDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, " Error Invocando servicio de Obtener datos por Dominio Contable: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetValueBusinessContabilidadDTO getBusinessValuesContabilidad(RequestGetBusinessValuesContabilidadDTO rq, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_FILL_GEN_BUSINESS_CONTABILIDAD;

            HttpEntity<RequestGetBusinessValuesContabilidadDTO> entity = new HttpEntity<>(rq, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseGetValueBusinessContabilidadDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, " Error Invocando servicio de Obtener datos de negocio elemento/registro contable: " + e.getMessage());
        }
    }

    @Override
    public ResponseDataEntidadDTO getEntidadByCode(Integer pCodEntidad, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_ENTITY_BY_CODE + pCodEntidad;

            HttpEntity<ResponseFindIdDominioDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseDataEntidadDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, " Error Invocando servicio de Obtener Datos Entidad Por código: " + e.getMessage());
        }
    }


    @Override
    public ResponseResolverComprobanteContableDTO getResolverComprobanteContable(RequestResolverComprobanteContableDTO rq, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_RESOLV_COMPROBANTE_CONTABLE;

            HttpEntity<RequestResolverComprobanteContableDTO> entity = new HttpEntity<>(rq, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseResolverComprobanteContableDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, " Error Invocando servicio de Obtener datos de negocio elemento/registro contable: " + e.getMessage());
        }
    }

    @Override
    public ResponseTrazabilidadNotificacionDTO getTrazabilidadNotificacion(TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_TRAZABILIDAD_NOTIFICACION;

            HttpEntity<ResponseTrazabilidadNotificacionDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseTrazabilidadNotificacionDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando la Trazabilidad: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetEntidadesInscritasDTO getEntidadesInscritas(TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_ENTIDADES_INSCRITAS;

            HttpEntity<ResponseGetEntidadesInscritasDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseGetEntidadesInscritasDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando las Entidades Inscritas: " + e.getMessage());
        }
    }

    @Override
    public ResponseGenTransactionDTO resolverNotificacionInscritas(RequestGenericJsonDTO jsonDTO, TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = RESOLVER_NOTIFICACION_INSCRITAS;

            HttpEntity<RequestGenericJsonDTO> entity = new HttpEntity<>(jsonDTO, headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.POST, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGenTransactionDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetFechaCorteDTO getFechaCorte(TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_FECHA_CORTE;

            HttpEntity<ResponseGetFechaCorteDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseGetFechaCorteDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando la fecha de corte: " + e.getMessage());
        }
    }

    @Override
    public ResponseGetFechaCorteMaximaNotificacionDTO getFechaMaximaCorte(TokenDTO token) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, BEARER + token.getTokenBusiness());
            String url = ENDPOINT_GET_FECHA_CORTE_MAXIMA_NOTIFICACION;

            HttpEntity<ResponseGetFechaCorteMaximaNotificacionDTO> entity = new HttpEntity<>(headers);
            ResponseEntity<String> responseService = template.exchange(url, HttpMethod.GET, entity, String.class);

            responseService.getHeaders().getLocation();
            responseService.getStatusCode();
            String responseBody = responseService.getBody();
            Gson gson = new Gson();
            return gson.fromJson(responseBody, ResponseGetFechaCorteMaximaNotificacionDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, "Error invocando la fecha maxima de corte: " + e.getMessage());
        }
    }
}
