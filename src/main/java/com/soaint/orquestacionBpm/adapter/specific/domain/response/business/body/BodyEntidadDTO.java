package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los valores de los parámetros")
public class BodyEntidadDTO implements Serializable {

    @ApiModelProperty(notes = "Nit de la Entidad")
    private BigDecimal nit;
    @ApiModelProperty(notes = "Nombre de la Entidad")
    private String nombreEntidad;
    @ApiModelProperty(notes = "Estado de la Entidad")
    private String estadoEntidad;
    @ApiModelProperty(notes = "Valor total de la liquidacion de la Entidad para la fecha de corte actual")
    private BigDecimal valorTotalLiquidacion;

}
