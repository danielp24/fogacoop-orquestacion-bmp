package com.soaint.orquestacionBpm.adapter.specific.domain.request.user.login;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class HeaderDTO implements Serializable {

    private String apiToken;
}
