package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la información de liqidación")
public class BodyGetDetalleLiquidacionDTO {

    @ApiModelProperty("Monto de la prima normal de la fecha de corte actual")
    private BigDecimal prima;
    @ApiModelProperty("Monto de la prima por posicion neta de la fecha de corte actual")
    private BigDecimal primaPosNeta;
    @ApiModelProperty("Monto de la sobreprima por riesgo de la fecha de corte actual")
    private BigDecimal sobrePrima;
    @ApiModelProperty("Monto del saldo pendiente acumulado hasta la fecha de corte actual")
    private BigDecimal saldoPendiente;
    @ApiModelProperty("Monto del interes de mora acumulado hasta la fecha de corte actual")
    private BigDecimal interesMora;
    @ApiModelProperty("Monto del saldo a favor acumulado hasta la fecha de corte actual")
    private BigDecimal saldoFavor;
    @ApiModelProperty("Monto del saldo a favor que se aplico en la fecha de corte anterior")
    private BigDecimal saldoFavorAplicadoPerAnt;
    @ApiModelProperty("Monto total de la retransmision que se presento en la fecha de corte actual")
    private BigDecimal totalRetransmision;
    @ApiModelProperty("Monto del valor total sugerido por parte del fondo hacia la entidad")
    private BigDecimal valorTotalSugerido;

}
