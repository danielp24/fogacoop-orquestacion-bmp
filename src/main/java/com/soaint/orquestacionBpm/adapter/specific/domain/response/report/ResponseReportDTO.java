package com.soaint.orquestacionBpm.adapter.specific.domain.response.report;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.report.body.BodyReportFR71DTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@Builder
public class ResponseReportDTO extends BaseResponseAdapter<BodyReportFR71DTO> implements Serializable {
}
