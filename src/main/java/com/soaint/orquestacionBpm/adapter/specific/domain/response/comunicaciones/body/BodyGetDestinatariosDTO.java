package com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de la parámetros de busquedad")
public class BodyGetDestinatariosDTO {

    @ApiModelProperty(notes = "Tipo destinatario: copia o principal")
    private String tipoDestinatario;
    @ApiModelProperty(notes = "Correo electronico del contacto de envio notificacion")
    private String correoElectronico;
}
