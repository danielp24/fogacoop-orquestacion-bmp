package com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Clase representativa del proceso de solicitar Contabilizacion")
public class RequestRegistrarComprobanteDTO {

    @ApiModelProperty(notes = " Corresponderá a la acción que se desea ejecutar")
    private int unaAccion;
    @ApiModelProperty(notes = " Corresponderá al comprobante con los registros contables en formato XML")
    private String unComprobante;
}
