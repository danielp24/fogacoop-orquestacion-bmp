package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.ISgdApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.*;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.descargarDocSgd.ResponseDescargaDocSgdDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.ResponseDescargarDocFilesystemDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.ResponseGuardarDocFilesystemDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.guardarDocExpediente.ResponseGuardarDocExpedienteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.opeCorreoGuarda.ResponseOpeCorreoDTO;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class SgdApiClient implements ISgdApiClient {
    private RestTemplate template = new RestTemplate();

    @Value("${endpoint.orquestacion.opeCorreo}")
    String OPE_CORREO_GUARDA;

    @Value("${endpoint.orquestacion.guardarDocumento}")
    String GUARDAR_DOC_FILESYSTEM;

    @Value("${endpoint.orquestacion.guardaDocExpediente}")
    String GUARDAR_DOC_EXPEDIENTE;

    @Value("${endpoint.orquestacion.descargarDocFilesystem}")
    String DESCARGAR_DOC_FILESYSTEM;

    @Value("${endpoint.orquestacion.descargarDocSgd}")
    String DESCARGAR_DOC_SGD;

    @Override
    public ResponseOpeCorreoDTO opeCorreoGuarda(RequestSaveCorreoOpDTO correoOPDTO, String authorizationJwt) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, authorizationJwt);
            String url = OPE_CORREO_GUARDA;

            HttpEntity<RequestSaveCorreoOpDTO> entity = new HttpEntity<>(correoOPDTO, headers);
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseOpeCorreoDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException("Error invocando OpeCorreo: " + e.getMessage());
        }
    }

    @Override
    public ResponseGuardarDocFilesystemDTO guardarDocumento(RequestGuardarDocFilesystemDTO rqGuardarDoc, String authorizationJwt) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, authorizationJwt);
            String url = GUARDAR_DOC_FILESYSTEM;

            HttpEntity<RequestGuardarDocFilesystemDTO> entity = new HttpEntity<>(rqGuardarDoc, headers);
            Gson gson = new Gson();
            System.out.println("REQUEST A ENVIAR A SGD FYLESYSTEM--->" +gson.toJson(rqGuardarDoc));
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGuardarDocFilesystemDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException("Error invocando guardarDocumento: " + e.getMessage());
        }
    }

    @Override
    public ResponseGuardarDocExpedienteDTO guardaDocExpediente(RequestGuardaDocExpedienteDTO rqGuardarDoc, String authorizationJwt) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, authorizationJwt);
            String url = GUARDAR_DOC_EXPEDIENTE;

            HttpEntity<RequestGuardaDocExpedienteDTO> entity = new HttpEntity<>(rqGuardarDoc, headers);
            Gson gson = new Gson();
            System.out.println("REQUEST A ENVIAR A SGD INFOPOINT--->" +gson.toJson(rqGuardarDoc));
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseGuardarDocExpedienteDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException("Error invocando guardarDocumento: " + e.getMessage());
        }
    }

    @Override
    public ResponseDescargarDocFilesystemDTO descargarDocFilesystem(RequestDescargarDocFilesystemDTO rq, String authorizationJwt) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, authorizationJwt);
            String url = DESCARGAR_DOC_FILESYSTEM;

            HttpEntity<RequestDescargarDocFilesystemDTO> entity = new HttpEntity<>(rq, headers);
            Gson gson = new Gson();
            System.out.println("REQUEST A ENVIAR A SGD DESCARGAR DOC FILESYSTEM--->" +gson.toJson(rq));
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseDescargarDocFilesystemDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException("Error invocando descargarDocFilesystem: " + e.getMessage());
        }
    }

    @Override
    public ResponseDescargaDocSgdDTO descargarDocSgd(RequestDescargarDocSgdDTO rq, String authorizationJwt) throws WebClientException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.AUTHORIZATION, authorizationJwt);
            String url = DESCARGAR_DOC_SGD;

            HttpEntity<RequestDescargarDocSgdDTO> entity = new HttpEntity<>(rq, headers);
            Gson gson = new Gson();
            System.out.println("REQUEST A ENVIAR A SGD DESCARGAR DOC INFOPOINT--->" +gson.toJson(rq));
            ResponseEntity<String> responseService = template.postForEntity(url, entity, String.class);

            return new Gson().fromJson(responseService.getBody(), ResponseDescargaDocSgdDTO.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw ValidationUtils.catchServerClientException(e);
        } catch (Exception e) {
            ValidationUtils.printLogErrorCatchException(e);
            throw new WebClientException("Error invocando descargarDocSgd: " + e.getMessage());
        }
    }
}
