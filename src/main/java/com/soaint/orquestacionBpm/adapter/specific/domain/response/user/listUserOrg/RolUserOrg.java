package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg;

import lombok.*;

import java.io.Serializable;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class RolUserOrg implements Serializable {
    private String codeAuthApplication;
    private String nameAuthApplication;
    private String id;
    private String description;
    private String name;
    private boolean enabled;
    private PrivilegiosUserOrg[] privileges;
}
