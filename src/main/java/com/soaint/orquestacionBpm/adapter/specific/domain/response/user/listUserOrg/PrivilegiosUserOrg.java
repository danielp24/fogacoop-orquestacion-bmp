package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg;

import lombok.*;

import java.io.Serializable;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class PrivilegiosUserOrg implements Serializable {
    private String id;
    private String name;
    private String code;
    private boolean enabled;

}
