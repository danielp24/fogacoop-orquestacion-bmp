package com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString


public class ArrMetaDataDTO implements Serializable {


    private String variable;

    private String valor;
}
