package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.business.*;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.comunicaciones.RequestGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDetallesNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDetallesNovedadRadicadoDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDocumentoNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestConsultarPagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteWithSecurityDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.*;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseActualizarPagoDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseDataEntidadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseGetFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones.ResponseGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.RequestGenericJsonDTO;
import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGeNombresContactoDTO;
import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGetPosLaboralDTO;
import com.soaint.orquestacionBpm.commons.domains.response.business.ResponseGenericTransactionWResDTO;
import com.soaint.orquestacionBpm.commons.domains.response.cooperativa.*;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;

public interface IBusinessApiClient {

    ResponseFindIdDominioDTO findIdDominio(String idDominio, TokenDTO token) throws WebClientException;

    ResponseGetPlantillasDTO getPlantilla(String idPlantilla, TokenDTO token) throws WebClientException;

    ResponseGetInfoBasicaDTO getInfoBasica(String nit, TokenDTO token) throws WebClientException;

    ResponseGetPagosDTO getPagos(RequestConsultarPagosDTO dto, TokenDTO token) throws WebClientException;

    ResponseGenericBusinessDTO businessParameterTemplate(RequestBusinessDTO dto, TokenDTO token) throws WebClientException;

    ResponseConsultarLiquidacionTotalDTO getLiquidacionTotal(RequestNitFechaCorteDTO dto, TokenDTO token) throws WebClientException;

    ResponseCooperativaDTO getEntidadesLiq(RequestNitFechaCorteDTO dto, TokenDTO token) throws WebClientException;

    ResponseGetDetallePagosDTO getDetallePagos(RequestConsultarPagosDTO dto, TokenDTO token) throws WebClientException;

    ResponseActualizarPagoDTO updatePago(RequestActualizarPagoDTO dto, TokenDTO token) throws WebClientException;

    ResponseAuthenticateDTO authenticate(RequestAuthtenticateDTO dto) throws WebClientException;

    ResponseGetPeriodoActualDTO getPeriodoActual(TokenDTO token) throws WebClientException;

    ResponseGetEntidadesDetallesPagos getEntidadesDetallesPago(String fechaCorte, TokenDTO token) throws WebClientException;

    ResponseGetDestinatariosDTO getDestinatarios(RequestGetDestinatariosDTO request, TokenDTO token) throws WebClientException;

    ResponseGetResolverNovedadEntidadLoteDTO getResolverNovedadEntidadLote(String fechaCorte, TokenDTO token) throws WebClientException;

    ResponseGetDetalleLiquidacion getDetalleLiquidacion(RequestNitFechaCorteDTO dto, TokenDTO token) throws WebClientException;

    ResponseGenericTransactionWResDTO insertDetalleNovedades(RequestInsertDetallesNovedadDTO insert, TokenDTO token) throws WebClientException;

    ResponseGenericTransactionDTO insertDetalleNovedadesRadicado(RequestInsertDetallesNovedadRadicadoDTO insert, TokenDTO token) throws WebClientException;

    ResponseGenericTransactionDTO insertDocumentoNovedad(RequestInsertDocumentoNovedadDTO insert, TokenDTO token) throws WebClientException;

    ResponseGetDetalleNovedadesDTO getDetalleNovedades(RequestNitFechaCorteWithSecurityDTO rq, TokenDTO token) throws WebClientException;

    ResponseGetListaDocumentosNovedadDTO getListaDocumentosNovedad(Integer idDetalleNovedad, TokenDTO token) throws WebClientException;

    ResponseGetNombresDTO getNombresContacto(RequestGeNombresContactoDTO rq, TokenDTO token) throws WebClientException;

    ResponseGetPosicionLaboralContactoDTO getPosicionLaboralContacto(RequestGetPosLaboralDTO rq, TokenDTO token) throws WebClientException;

    ResponseGetCorreosDTO getCorreosContacto(String numeroIdent, TokenDTO token) throws WebClientException;

    ResponseGetDireccionDTO getDireccionesContacto(String numeroIdent, TokenDTO token) throws WebClientException;

    ResponseGetTelefonosDTO getTelefonosContacto(String numeroIdent, TokenDTO token) throws WebClientException;

    ResponseFindIdDominioDTO getDominioContable(String nombreSetComprobante, TokenDTO token) throws WebClientException;

    ResponseGetValueBusinessContabilidadDTO getBusinessValuesContabilidad(RequestGetBusinessValuesContabilidadDTO rq, TokenDTO token) throws WebClientException;

    ResponseDataEntidadDTO getEntidadByCode(Integer pCodEntidad, TokenDTO token) throws WebClientException;

    ResponseResolverComprobanteContableDTO getResolverComprobanteContable(RequestResolverComprobanteContableDTO rq, TokenDTO token) throws WebClientException;

    ResponseTrazabilidadNotificacionDTO getTrazabilidadNotificacion(TokenDTO token) throws WebClientException;

    ResponseGetEntidadesInscritasDTO getEntidadesInscritas(TokenDTO token) throws WebClientException;

    ResponseGenTransactionDTO resolverNotificacionInscritas(RequestGenericJsonDTO jsonDTO, TokenDTO token) throws WebClientException;

    ResponseGetFechaCorteDTO getFechaCorte(TokenDTO token) throws WebClientException;

    ResponseGetFechaCorteMaximaNotificacionDTO getFechaMaximaCorte(TokenDTO token) throws WebClientException;
}
