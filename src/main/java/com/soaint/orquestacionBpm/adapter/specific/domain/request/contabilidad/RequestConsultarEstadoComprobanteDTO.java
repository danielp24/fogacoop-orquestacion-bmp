package com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Clase representativa del proceso de solicitar Contabilizacion")
public class RequestConsultarEstadoComprobanteDTO {

    @ApiModelProperty(notes = "Correspondera al codigo de la transaccion.")
    private String unCodigoTransaccion;
}
