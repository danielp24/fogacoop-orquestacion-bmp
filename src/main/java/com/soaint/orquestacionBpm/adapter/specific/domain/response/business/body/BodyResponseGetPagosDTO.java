package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.details.PagosDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de la parámetros de busquedad")
public class BodyResponseGetPagosDTO implements Serializable {
    @ApiModelProperty(notes = "Lista de los pagos realizados")
    private List<PagosDTO> pagos;
    @ApiModelProperty(notes = "Suma de los valores pagados")
    private BigDecimal total;

}
