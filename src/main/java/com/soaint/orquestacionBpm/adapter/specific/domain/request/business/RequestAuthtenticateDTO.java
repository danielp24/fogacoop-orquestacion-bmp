package com.soaint.orquestacionBpm.adapter.specific.domain.request.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los parámetros para autenticar la Api de Negocio")
public class RequestAuthtenticateDTO implements Serializable {
    private String username;
    private String password;
    private Boolean expire;
}
