package com.soaint.orquestacionBpm.adapter.specific.domain.response.report.body;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BodyReportFR71DTO {

    private String base64;


}
