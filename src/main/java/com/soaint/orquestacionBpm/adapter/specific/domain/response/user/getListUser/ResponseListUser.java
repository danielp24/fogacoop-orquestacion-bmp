package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_DEFAULT)

@ApiModel(description = "Clase representativa de respuesta al proceso obtener lista de usuarios por organización")
public class ResponseListUser implements Serializable {

    @ApiModelProperty(notes = "Estado del proceso obtener lista de usuarios por organización")
    private Integer status;

    @ApiModelProperty(notes = "Cuerpo representativo de la respuesta del proceso obtener lista de usuarios por organización")
    private BodyListUser[] body;

    @ApiModelProperty(notes = "Mensaje de la respuesta del proceso obtener lista de usuarios por organización")
    private String message;

    }
