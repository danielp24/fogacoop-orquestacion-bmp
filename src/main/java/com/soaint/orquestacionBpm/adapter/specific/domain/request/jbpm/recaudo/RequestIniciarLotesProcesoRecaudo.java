package com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo;

import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.UserDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Clase representativa para la peticion de la operacion IniciarLotesProcesoRecaudo.")
public class RequestIniciarLotesProcesoRecaudo implements Serializable {

    @RequiredParameter
    @ApiModelProperty("Datos del usuario propietario")
    private UserDto ownerUser;

    @RequiredPrimitiveParameter
    @ApiModelProperty("Fecha de corte para iniciar el proceso de recaudo")
    private String fechaCorte;

    @ApiModelProperty("Token de validacion para la api de negocio")
    private TokenDTO tokenAutenticatedBusiness;
}
