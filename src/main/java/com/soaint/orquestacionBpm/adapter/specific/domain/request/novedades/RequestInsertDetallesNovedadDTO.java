package com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa los paramteros a ingresar en detalles novedades")
public class RequestInsertDetallesNovedadDTO {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Numero de identificacion de la entidad.")
    private String numeroIdentificacionEntidad;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Tipo de identificacion del contacto.")
    private String tipoIdentificacionContacto;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Numero de identificacion del contacto.")
    private String numeroIdentificacionContacto;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha de inicio del contacto.")
    private String fechaInicioContacto;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha de corte.")
    private String fechaCorte;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Tipo de novedad.")
    private Integer tipoNovedad;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha de inicio de novedad.")
    private String fechaInicioNovedad;

    @ApiModelProperty(notes = "Fecha final de novedad.")
    private String fechaFinalNovedad = "";

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha final de novedad.")
    private String tipoIdentificacionFuncionario;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha final de novedad.")
    private String numeroIdentificacionFuncionario;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Fecha de inicio de actividad del funcionario.")
    private String fechaInicioFuncionario;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Cargo del funcionario.")
    private Integer cargoFuncionario;

    @ApiModelProperty(notes = "Fecha efectiva de la novedad.")
    private String fechaEfectiva = "";

    @ApiModelProperty(notes = "Valor de la novedad.")
    private String valorNovedad = "";

    @ApiModelProperty(notes = "Estatus de novedad aplicada.")
    private String aplicado = "NO";

    @ApiModelProperty(notes = "Estatus de novedad aplicada.")
    private String observaciones = "";

}
