package com.soaint.orquestacionBpm.adapter.specific.domain.request.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
@ApiModel(description = "Clase representativa de la solicitud de proceso")
public class RequestListUserOrgDTO implements Serializable {

    @ApiModelProperty(notes = "apitoken aplicacion")
    private String apiToken;
    @ApiModelProperty(notes = "organizacion a filtrar")
    private String organization;
}
