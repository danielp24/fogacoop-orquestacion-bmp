package com.soaint.orquestacionBpm.adapter.specific.domain.request.report;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@ApiModel(description = "Clase representativa de la captura de datos del reporte")
public class RutasDTO {
    @ApiModelProperty(notes = "Nombre del Reporte PDF")
    private String nombreReporte;
    @ApiModelProperty(notes = "Nombre de la ruta donde está el reporte Jasper")
    private String nombreJasper;
}
