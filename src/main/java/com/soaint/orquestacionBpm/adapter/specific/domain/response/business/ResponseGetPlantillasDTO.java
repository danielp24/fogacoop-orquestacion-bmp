package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetPlantillasDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@AllArgsConstructor
@Builder
@ApiOperation("Clase que representa la respuesta para obtener plantilla")
public class ResponseGetPlantillasDTO extends BaseResponseAdapter<BodyGetPlantillasDTO[]> implements Serializable {
}
