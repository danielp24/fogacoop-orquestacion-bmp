package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetResolverNovedadEntidadLote;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
public class ResponseGetResolverNovedadEntidadLoteDTO extends BaseResponseAdapter<BodyGetResolverNovedadEntidadLote[]> implements Serializable {

}
