package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetEntidadesDetallesPagos;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la solicitud de encontrar el ID del dominio")
public class ResponseGetEntidadesDetallesPagos extends BaseResponseAdapter<BodyGetEntidadesDetallesPagos[]> implements Serializable {

}
