package com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.body.BodyDescargarDocFilesystemDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.io.Serializable;

@AllArgsConstructor
@Builder
@ApiOperation("Clase que representa la respuesta para obtener plantilla")
public class ResponseDescargarDocFilesystemDTO extends BaseResponseAdapter<BodyDescargarDocFilesystemDTO> implements Serializable {
}
