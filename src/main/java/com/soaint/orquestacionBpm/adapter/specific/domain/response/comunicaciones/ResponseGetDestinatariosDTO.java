package com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones.body.BodyGetDestinatariosDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa de la solicitud de obtener los destinatarios a notificar")
public class ResponseGetDestinatariosDTO extends BaseResponseAdapter<List<BodyGetDestinatariosDTO>> implements Serializable {

}
