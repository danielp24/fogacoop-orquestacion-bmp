package com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ToString
@ApiModel(description = "Clase representativa de los valores de los parámetros")
public class BodyFindIdDominioDTO implements Serializable, Comparable<BodyFindIdDominioDTO> {

    @ApiModelProperty(notes = "ID de los parámetros")
    private BigDecimal idParametro;
    @ApiModelProperty(notes = "ID del dominio")
    private BigDecimal idDominio;
    @ApiModelProperty(notes = "ID Del padre de los parametros")
    private BigDecimal idPadre;
    @ApiModelProperty(notes = "Nombre parámetros")
    private String nombre;
    @ApiModelProperty(notes = "Valor por siglas")
    private String valor;
    @ApiModelProperty(notes = "Descripcion de los parámetros")
    private String descripcion;

    @Override
    public int compareTo(BodyFindIdDominioDTO o) {
        return this.valor.compareTo(o.getValor());
    }
}
