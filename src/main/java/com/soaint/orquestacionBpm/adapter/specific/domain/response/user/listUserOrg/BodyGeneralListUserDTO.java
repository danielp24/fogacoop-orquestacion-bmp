package com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BodyGeneralListUserDTO implements Serializable {

    private ResponseListUserOrg body;
    private String status;

}
