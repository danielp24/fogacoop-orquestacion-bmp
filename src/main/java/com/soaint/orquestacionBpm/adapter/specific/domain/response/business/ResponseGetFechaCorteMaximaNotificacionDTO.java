package com.soaint.orquestacionBpm.adapter.specific.domain.response.business;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyResponseGetFechaCorteMaximaNotificacionDTO;
import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@ApiModel(description = "Clase representativa del cuerpo de la busqueda denotificacion maxima")
public class ResponseGetFechaCorteMaximaNotificacionDTO extends BaseResponseAdapter<BodyResponseGetFechaCorteMaximaNotificacionDTO> {
}
