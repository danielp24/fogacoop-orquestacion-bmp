package com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.user;

import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser.BodyGeneralGetListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg.BodyGeneralListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login.ResponseUser;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;

public interface IUserApiClient {
    BodyGeneralListUserDTO listUserOrg(String apitoken, String organization) throws WebClientException;
    BodyGeneralGetListUserDTO getListUser(String apitoken) throws WebClientException;

    ResponseUser login(String username, String password) throws WebClientException;
}
