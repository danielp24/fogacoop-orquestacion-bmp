package com.soaint.orquestacionBpm.commons.domains.request.jbpm.process;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor

public class AllInstance implements Serializable {

    private OwnerUser ownerUser;
}
