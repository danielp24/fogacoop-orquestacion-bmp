package com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo;

import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa del guardado de documentos dependiendo de la novedad")

public class ResolverGuardadoDocumentosDTO {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "ID de la plantilla a usar")
    private String idNovedad;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Id del Expediente")
    private String nit;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Usuario que clasifica el documento")
    private String usuarioClasificador;

    @RequiredParameter
    @ApiModelProperty(notes = "Listado de documentos a negociar")
    private List<DocumentoDTO> listaDocumentos;

}
