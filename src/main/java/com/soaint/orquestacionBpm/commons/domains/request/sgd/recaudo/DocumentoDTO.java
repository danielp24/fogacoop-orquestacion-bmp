package com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.ArrMetaDataDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa del guardado de documentos dependiendo de la novedad")

public class DocumentoDTO {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nombre del documento")
    private String nomDocumento;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Tipo de documento, depende de la subserie donde se está guardando el documento")
    private String idTipDoc;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Información del archivo Codificado en base 64 ")
    private String fileBase64;

    @ApiModelProperty(notes = "Metadata documento")
    private List<ArrMetaDataDTO> arrMetaData;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nombre visible “Amigable” del documento.")
    private String nomDocVisible;

}
