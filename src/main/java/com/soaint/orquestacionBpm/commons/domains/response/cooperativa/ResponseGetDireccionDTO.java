package com.soaint.orquestacionBpm.commons.domains.response.cooperativa;

import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body.DireccionDTO;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.io.Serializable;

@AllArgsConstructor
@Builder
@ApiOperation("Clase que representa la respuesta para obtener el resultado de la transaccion generica")
public class ResponseGetDireccionDTO extends BaseResponseAdapter<DireccionDTO[]> implements Serializable {

}
