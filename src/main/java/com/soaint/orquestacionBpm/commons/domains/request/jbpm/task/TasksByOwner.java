package com.soaint.orquestacionBpm.commons.domains.request.jbpm.task;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class TasksByOwner implements Serializable {

    public OwnerUser ownerUser;
}
