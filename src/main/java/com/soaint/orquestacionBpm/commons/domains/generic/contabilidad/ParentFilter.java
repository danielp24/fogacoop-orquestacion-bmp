package com.soaint.orquestacionBpm.commons.domains.generic.contabilidad;


import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ParentFilter {
    private String transversalParentId;
    private String comprobanteParentId;
    private String registroContableId;
}
