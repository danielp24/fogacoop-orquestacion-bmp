package com.soaint.orquestacionBpm.commons.domains.request.jbpm.variable;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;

import java.io.Serializable;

public class VariableByProcess implements Serializable {

    private String containerId;
    private String processInstance;
    private OwnerUser ownerUser;
}
