package com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@ApiOperation("Clase que representa la respuesta genericas para transacciones.")
public class DireccionDTO {


    @ApiModelProperty(notes = "Fecha de inicio del correo")
    private String fechaInicio;
    @ApiModelProperty(notes = "Fecha final del correo")
    private String fechaFinal = null;
    @ApiModelProperty(notes = "Direccion detallada del contacto")
    private String direccion;
    @ApiModelProperty(notes = "Id Departamento de la direccion")
    private Integer departamento;
    @ApiModelProperty(notes = "Id Municipio de la direccion")
    private Integer municipio;
    @ApiModelProperty(notes = "Tipo de estado de la direccion")
    private Integer tipoEstado;
    @ApiModelProperty(notes = "Estado de la direccion")
    private Integer estado;


}
