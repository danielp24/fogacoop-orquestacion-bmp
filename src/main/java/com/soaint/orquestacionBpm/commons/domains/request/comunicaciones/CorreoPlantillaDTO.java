package com.soaint.orquestacionBpm.commons.domains.request.comunicaciones;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.lstAdjuntosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa de la entrada de datos para enviar un correo")

public class CorreoPlantillaDTO  {
    @ApiModelProperty(notes = "ID de la plantilla a usar")
    private String idPlantilla;
    @ApiModelProperty(notes = "IdDominio que se usará")
    private String idDominio;
    @ApiModelProperty(notes = "NIT de la empresa registrada")
    private String nit;
    @ApiModelProperty(notes = "Lista de parámetros de negocio que se usarán para consultar los datos")
    private List<String> businessList;
    @ApiModelProperty(notes = "Tipo de parámetro que se ejecutará")
    private Integer typeParameter;
    @ApiModelProperty(notes = "Adjuntos que irán en el correo")
    private List<lstAdjuntosDTO> lstAdjuntos;
    private TokenDTO tokenAutenticatedBusiness;
}
