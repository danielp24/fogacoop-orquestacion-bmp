package com.soaint.orquestacionBpm.commons.domains.request.contabilidad;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(description = "Clase representativa de envio de de comprobantes contables segun estado del pago")
public class RequestEnviarComprobanteContabilidadDTO {

    @ApiModelProperty(notes = "Datos basico de recaudo")
    private RequestNitFechaCorteDTO basic;
    @ApiModelProperty(notes = "Id del pago asociado solo si hay un pago de por medio")
    private Integer idPago;
    @ApiModelProperty(notes = "Conjunto de comprobantes atados")
    private String conjuntoComprobantes;
    @ApiModelProperty(notes = "Token de autenticación")
    private TokenDTO tokenAutenticatedBusiness;
}
