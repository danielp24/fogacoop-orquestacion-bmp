package com.soaint.orquestacionBpm.commons.domains.request.jbpm.task;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

public class ActiveTask {

    private OwnerUser ownerUser;
}
