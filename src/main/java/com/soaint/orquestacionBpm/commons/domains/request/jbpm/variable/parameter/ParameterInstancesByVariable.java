package com.soaint.orquestacionBpm.commons.domains.request.jbpm.variable.parameter;

import lombok.Data;

import java.io.Serializable;

@Data

public class ParameterInstancesByVariable implements Serializable {

    private ParameterValueInstanceByVariable value;
}
