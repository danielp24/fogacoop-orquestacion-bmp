package com.soaint.orquestacionBpm.commons.domains.response.business.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@ApiOperation("Clase que representa la respuesta genericas para transacciones.")
public class BodyGenericTransactionDTO {

    @ApiModelProperty(notes = "Codigo de transaccion")
    private BigDecimal code;
    @ApiModelProperty(notes = "Respuesta de transaccion")
    private String response;
    @ApiModelProperty(notes = "Mensaje de transaccion")
    private String message;

}
