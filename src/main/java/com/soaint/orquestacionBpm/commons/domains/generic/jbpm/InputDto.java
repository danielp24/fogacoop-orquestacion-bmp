package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 
 * @author jjmorales
 *
 */
@ApiModel(description = "Representative class of the rule entries.")
public class InputDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Rule entry identifier.")
	private String inputId;
	
	@ApiModelProperty(notes = "Rule entry name.")
	private String inputName;
	
	public InputDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @return the inputId
	 */
	public String getInputId() {
		return inputId;
	}

	/**
	 * 
	 * @param inputId the inputId to set
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * 
	 * @return the inputName
	 */
	public String getInputName() {
		return inputName;
	}

	/**
	 * 
	 * @param inputName the inputName to set
	 */
	public void setInputName(String inputName) {
		this.inputName = inputName;
	}

}
