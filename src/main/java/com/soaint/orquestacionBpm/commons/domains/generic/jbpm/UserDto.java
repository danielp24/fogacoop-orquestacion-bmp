
/**
 * 
 */
package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class for handling user data of the process or task.")
public class UserDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "User owner of the task.")
    private String user;

    @RequiredPrimitiveParameter
	@ApiModelProperty(notes = "Owner user password.")
	private String password;
	
	@ApiModelProperty(notes = "target user for reassignments.")
	private String targetUser;

	/**
	 * 
	 */
	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the targetUser
	 */
	public String getTargetUser() {
		return targetUser;
	}

	/**
	 * @param targetUser the targetUser to set
	 */
	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	/**
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
}