package com.soaint.orquestacionBpm.commons.domains.request.jbpm.task.parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ParametersTaskStatus {

    private Values values;
}
