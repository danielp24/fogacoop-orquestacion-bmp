package com.soaint.orquestacionBpm.commons.domains.request.jbpm.process;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import com.soaint.orquestacionBpm.commons.domains.request.jbpm.process.parameter.ParameterStartProcess;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor

public class StartProcessInstance implements Serializable {

    private String containerId;
    private String processesId;
    private OwnerUser ownerUser;
    private ParameterStartProcess parametros;

}

