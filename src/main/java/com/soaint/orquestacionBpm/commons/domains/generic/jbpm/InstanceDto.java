/**
 * 
 */
package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the process instance")
public class InstanceDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Unique identifier of the process instance.")
	private String instanceId;

	/**
	 * 
	 */
	public InstanceDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the instanceId
	 */
	public String getInstanceId() {
		return instanceId;
	}

	/**
	 * @param instanceId the instanceId to set
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
}
