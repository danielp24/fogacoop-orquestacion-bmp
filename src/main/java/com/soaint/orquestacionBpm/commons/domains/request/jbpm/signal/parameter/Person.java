package com.soaint.orquestacionBpm.commons.domains.request.jbpm.signal.parameter;

import lombok.Data;

import java.io.Serializable;

@Data

public class Person implements Serializable {

    private CentroSamarPersona comBpmCentrosamarPersona;
}
