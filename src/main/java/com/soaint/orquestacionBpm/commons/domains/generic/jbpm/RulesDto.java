package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author jjmorales
 *
 */
@ApiModel(description = "Representative class of the rule.")
public class RulesDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Namespace of the rule.")
	private String ruleNamespace;
	
	@ApiModelProperty(notes = "Name of the rule.")
	private String ruleName;
	
	@ApiModelProperty(notes = "Unique identifier of the rule.")
	private String ruleId;
	
	@ApiModelProperty(notes = "List of decisions associated with the rule.")
	private List<DecisionDto> decisionList;
	
	@ApiModelProperty(notes = "List of inputs associated with the rule.")
	private List<InputDto> inputList;
	
	@ApiModelProperty(notes = "Context attributes of the rule.")
	private Map<String, Object> context;
	
	public RulesDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @return the RuleNamespace
	 */
	public String getRuleNamespace() {
		return ruleNamespace;
	}

	/**
	 * 
	 * @param ruleNamespace the RuleNamespace to set
	 */
	public void setRuleNamespace(String ruleNamespace) {
		this.ruleNamespace = ruleNamespace;
	}

	/**
	 * 
	 * @return the RuleName
	 */
	public String getRuleName() {
		return ruleName;
	}

	/**
	 * 
	 * @param ruleName the RuleName to set
	 */
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	/**
	 * 
	 * @return the ruleId
	 */
	public String getRuleId() {
		return ruleId;
	}

	/**
	 * 
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * 
	 * @return the decisionList
	 */
	public List<DecisionDto> getDecisionList() {
		return decisionList;
	}

	/**
	 * 
	 * @param decisionList the decisionList to set
	 */
	public void setDecisionList(List<DecisionDto> decisionList) {
		this.decisionList = decisionList;
	}

	/**
	 * 
	 * @return the inputList
	 */
	public List<InputDto> getInputList() {
		return inputList;
	}

	/**
	 * 
	 * @param inputList the inputList to set
	 */
	public void setInputList(List<InputDto> inputList) {
		this.inputList = inputList;
	}

	/**
	 * 
	 * @return the context
	 */
	public Map<String, Object> getContext() {
		return context;
	}

	/**
	 * 
	 * @param context the context to set
	 */
	public void setContext(Map<String, Object> context) {
		this.context = context;
	}
}
