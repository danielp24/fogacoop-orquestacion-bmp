package com.soaint.orquestacionBpm.commons.domains.response.jbpm;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.RequestBpmDTO;
import lombok.Data;

@Data
public class FinishCollectionProcessDTO {

    private RequestBpmDTO body;
    private String status;
    private String timeResponse;
    private String message;
    private String path;
    private String transactionState;
}
