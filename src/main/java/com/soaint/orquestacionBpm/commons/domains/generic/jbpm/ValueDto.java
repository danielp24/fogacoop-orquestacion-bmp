package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 
 * @author jjmorales
 *
 */
@ApiModel(description = "Representative class for handling attributes of a process instance or a task.")
public class ValueDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Name of the attribute.")
	private String key;
	
	@ApiModelProperty(notes = "Attribute value.")
	private String value;

	/**
	 * 
	 */
	public ValueDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
