package com.soaint.orquestacionBpm.commons.domains.request.comunicaciones;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class NotificacionEntidadInscritaJsonDTO {

    private String fechaCorte;
    private List<NumeroIdentificacionCoopDTO> listEntidades;
    private Long codigoPlantilla;
}
