package com.soaint.orquestacionBpm.commons.domains.generic.contabilidad;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad.RequestRegistrarComprobanteDTO;
import lombok.*;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ContextComprobantes {

    private List<RequestRegistrarComprobanteDTO> listRequestCont;
    private List<String> listRegContables;
    private List<Comprobante> listComprobantes;

}
