package com.soaint.orquestacionBpm.commons.domains.request.jbpm.variable;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import com.soaint.orquestacionBpm.commons.domains.request.jbpm.variable.parameter.ParameterInstancesByVariable;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor

public class InstancesByVariable implements Serializable {

    private OwnerUser ownerUser;
    private ParameterInstancesByVariable parametros;
}
