package com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@ApiOperation("Clase que representa la respuesta genericas para transacciones.")
public class AsociacionEntidadDTO {

    @ApiModelProperty(notes = "Tipo de identificacion del la entidad")
    private String tipoIdentificacionEntidad;
    @ApiModelProperty(notes = "Numero de identificacion de la entidad")
    private Integer numIdentificacionEntidad;
    @ApiModelProperty(notes = "Numero de identificacion de la entidad")
    private List<PosicionLaboralEntidadDTO> posicionLaboral;


}
