package com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa del expediente de la entidad del SGDA")

public class ExpendienteEntidadDTO {

    @ApiModelProperty(notes = "Dependencia del documento")
    private String idExpediente;

    @ApiModelProperty(notes = "Serie del documento")
    private String codExpediente;

    @ApiModelProperty(notes = "Subserie del documento")
    private String nombreEntidad;

}
