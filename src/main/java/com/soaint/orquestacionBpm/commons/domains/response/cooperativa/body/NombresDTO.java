package com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.math.BigInteger;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@ApiOperation("Clase que representa la respuesta genericas para transacciones.")
public class NombresDTO {

    @ApiModelProperty(notes = "Tipo de identificacion del Contacto")
    private String tipoIdentificacion;
    @ApiModelProperty(notes = "Numero de identificacion del Contacto")
    private BigInteger numIdentificacion;
    @ApiModelProperty(notes = "Fecha de inicio del Nombre del Contacto")
    private String fechaInicio;
    @ApiModelProperty(notes = "Fecha final del Nombre del Contacto")
    private String fechaFin;
    @ApiModelProperty(notes = "Nombre del Contacto")
    private String nombre;
    @ApiModelProperty(notes = "Primer Apellido del Contacto")
    private String primerApellido;
    @ApiModelProperty(notes = "Segundo Apellido del Contacto")
    private String segundoApellido;


}
