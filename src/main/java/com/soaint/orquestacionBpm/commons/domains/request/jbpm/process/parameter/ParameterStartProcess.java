package com.soaint.orquestacionBpm.commons.domains.request.jbpm.process.parameter;

import lombok.Data;

import java.io.Serializable;

@Data

public class ParameterStartProcess implements Serializable {

    private ParameterValueStartProcess values;
}
