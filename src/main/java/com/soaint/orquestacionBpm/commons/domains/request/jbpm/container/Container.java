package com.soaint.orquestacionBpm.commons.domains.request.jbpm.container;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor

public class Container implements Serializable {

    private OwnerUser ownerUser;
}
