package com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@ApiOperation("Clase que representa la respuesta genericas para transacciones.")
public class CorreoDTO {


    @ApiModelProperty(notes = "Fecha de inicio del correo")
    private String fechaInicio;
    @ApiModelProperty(notes = "Fecha final del correo")
    private String fechaFinal;
    @SerializedName("correoContacto")
    @ApiModelProperty(notes = "Correo electronico")
    private String correoElectronico;


}
