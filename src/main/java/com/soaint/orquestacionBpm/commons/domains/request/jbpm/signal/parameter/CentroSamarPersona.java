package com.soaint.orquestacionBpm.commons.domains.request.jbpm.signal.parameter;

import lombok.Data;

import java.io.Serializable;

@Data

public class CentroSamarPersona implements Serializable {

    private Integer id;
    private String name;
    private String lastName;
}
