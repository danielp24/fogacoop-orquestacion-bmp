package com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@ApiOperation("Clase que representa la respuesta genericas para transacciones.")
public class PosicionLaboralEntidadDTO {


    @ApiModelProperty(notes = "Fecha de inicio de la posicion laboral")
    private String fechaInicio;
    @ApiModelProperty(notes = "Fecha final de la posicion laboral")
    private String fechaFinal;
    @ApiModelProperty(notes = "id Area de la posicion laboral")
    private Integer area;
    @ApiModelProperty(notes = "id Cargo de la posicion laboral")
    private Integer cargo;


}
