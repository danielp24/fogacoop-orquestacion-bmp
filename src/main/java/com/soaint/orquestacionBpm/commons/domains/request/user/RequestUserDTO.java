package com.soaint.orquestacionBpm.commons.domains.request.user;

import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa de la entrada de datos para enviar un correo")

public class RequestUserDTO {
    private String email;
    private TokenDTO tokenAutenticatedBusiness;

}
