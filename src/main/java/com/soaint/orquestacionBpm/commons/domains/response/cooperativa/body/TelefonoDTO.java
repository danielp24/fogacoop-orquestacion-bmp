package com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@ApiOperation("Clase que representa la respuesta genericas para transacciones.")
public class TelefonoDTO {


    @ApiModelProperty(notes = "Fecha de inicio del correo")
    private String fechaInicio;
    @ApiModelProperty(notes = "Fecha final del correo")
    private String fechaFinal = null;
    @ApiModelProperty(notes = "Id de tipo de telefono")
    private String tipoTelefono;
    @ApiModelProperty(notes = "Numero de telefono")
    private BigDecimal numeroTelefono;
    @ApiModelProperty(notes = "Detalle de telefono o extension del telefono")
    private Integer detalleTelefono;
    @ApiModelProperty(notes = "Id Departamento de la direccion")
    private Integer departamento;
    @ApiModelProperty(notes = "Id Municipio de la direccion")
    private Integer municipio;
    @ApiModelProperty(notes = "Tipo de estado de la direccion")
    private Integer tipoEstado;
    @ApiModelProperty(notes = "Estado de la direccion")
    private Integer estado;

}
