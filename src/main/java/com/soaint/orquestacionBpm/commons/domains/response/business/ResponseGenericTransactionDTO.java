package com.soaint.orquestacionBpm.commons.domains.response.business;

import com.soaint.orquestacionBpm.commons.domains.response.BaseResponseAdapter;
import com.soaint.orquestacionBpm.commons.domains.response.business.body.BodyGenericTransactionDTO;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.io.Serializable;

@AllArgsConstructor
@Builder
@ApiOperation("Clase que representa la respuesta para obtener plantilla")
public class ResponseGenericTransactionDTO extends BaseResponseAdapter<BodyGenericTransactionDTO> implements Serializable {
}
