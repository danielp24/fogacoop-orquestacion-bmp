package com.soaint.orquestacionBpm.commons.domains.generic.contabilidad;

import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAttribute;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@ToString
@AllArgsConstructor
@XmlRootElement(name = "comprobante")
@XmlAccessorType(XmlAccessType.FIELD)
public class Comprobante {

    //Attributes
    @XmlAttribute(name = "codigoContabilidad")
    private String codigoContabilidad;

    @XmlAttribute(name = "descripcion")
    private String descripcion;

    @XmlAttribute(name = "periodoFiscal")
    private String periodoFiscal;

    @XmlAttribute(name = "mes")
    private String mes;

    @XmlAttribute(name = "usuarioapoteosys")
    private String usuarioapoteosys;

    //Child
    @MatchAttribute
    private List<RegistroContable> registrocontable;

    public Comprobante(){
        this.registrocontable = new ArrayList<RegistroContable>();
    }

}
