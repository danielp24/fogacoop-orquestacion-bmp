package com.soaint.orquestacionBpm.commons.domains.response.cooperativa;

import com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body.*;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@ApiOperation("Clase que representa la respuesta para obtener el resultado de la transaccion generica")
public class ResponseGetContactosEntidadDTO extends NombresDTO implements Serializable {

    @ApiModelProperty(notes = "Segundo Apellido del Contacto")
    private AsociacionEntidadDTO asocEntidad;
    @ApiModelProperty(notes = "Segundo Apellido del Contacto")
    private List<CorreoDTO> correos;
    @ApiModelProperty(notes = "Segundo Apellido del Contacto")
    private List<DireccionDTO> direcciones;
    @ApiModelProperty(notes = "Segundo Apellido del Contacto")
    private List<TelefonoDTO> telefonos;

}
