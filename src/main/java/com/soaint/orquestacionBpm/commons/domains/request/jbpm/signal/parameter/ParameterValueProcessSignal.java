package com.soaint.orquestacionBpm.commons.domains.request.jbpm.signal.parameter;

import lombok.Data;

import java.io.Serializable;

@Data

public class ParameterValueProcessSignal implements Serializable {

    private String numeroSolicitud;
    private String initiator;
    private Person Person;
    private Boolean estadoSolicitud;

}
