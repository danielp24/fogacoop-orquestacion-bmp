package com.soaint.orquestacionBpm.commons.domains.response.repository;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa del guardado de documentos dependiendo de la novedad")

public class ResponseGetDocumentRepositoryDTO {


    @ApiModelProperty(notes = "Extension Documento")
    private String extension;

    @ApiModelProperty(notes = "Documento encodeado en Base64")
    private String docBase64;

}
