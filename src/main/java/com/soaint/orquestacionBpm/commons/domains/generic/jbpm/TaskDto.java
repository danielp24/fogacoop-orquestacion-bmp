/**
 * 
 */
package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class for information management of a task.")
public class TaskDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Unique identifier of the task.")
	private Integer taskId;
	
	@ApiModelProperty(notes = "Name of the process task.")
	private String taskName;
	
	@ApiModelProperty(notes = "Subject of the process task.")
	private String taskSubject;
	
	@ApiModelProperty(notes = "Description of the task process.")
	private String taskDescription;
	
	@ApiModelProperty(notes = "Status of process task.")
	private String taskStatus;
	
	@ApiModelProperty(notes = "Priority level of the process task.")
	private Integer taskPriority;
	
	@ApiModelProperty(notes = "Current owner user of the process task.")
	private String taskActualOwner;
	
	@ApiModelProperty(notes = "User creating the process task.")
	private String taskCreatedBy;
	
	@ApiModelProperty(notes = "Date creation of the task.")
	private String taskCreatedOn;
	
	@ApiModelProperty(notes = "Activation date of the task.")
	private String taskActivationTime;
	
	@ApiModelProperty(notes = "Expiration date of the task.")
	private String taskExpirationTime;
	
	@ApiModelProperty(notes = "task identifier name in a process.")
	private String taskProcDefId;
	
	@ApiModelProperty(notes = "Unique identifier of the container.")
	private String containerId;
	
	@ApiModelProperty(notes = "Unique identifier of the process instance.")
	private String instanceId;

	/**
	 * Super
	 */
	public TaskDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the taskId
	 */
	public Integer getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return the taskName
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * @param taskName the taskName to set
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * @return the taskSubject
	 */
	public String getTaskSubject() {
		return taskSubject;
	}

	/**
	 * @param taskSubject the taskSubject to set
	 */
	public void setTaskSubject(String taskSubject) {
		this.taskSubject = taskSubject;
	}

	/**
	 * @return the taskDescription
	 */
	public String getTaskDescription() {
		return taskDescription;
	}

	/**
	 * @param taskDescription the taskDescription to set
	 */
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	/**
	 * @return the taskStatus
	 */
	public String getTaskStatus() {
		return taskStatus;
	}

	/**
	 * @param taskStatus the taskStatus to set
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	/**
	 * @return the taskPriority
	 */
	public Integer getTaskPriority() {
		return taskPriority;
	}

	/**
	 * @param taskPriority the taskPriority to set
	 */
	public void setTaskPriority(Integer taskPriority) {
		this.taskPriority = taskPriority;
	}

	/**
	 * @return the taskActualOwner
	 */
	public String getTaskActualOwner() {
		return taskActualOwner;
	}

	/**
	 * @param taskActualOwner the taskActualOwner to set
	 */
	public void setTaskActualOwner(String taskActualOwner) {
		this.taskActualOwner = taskActualOwner;
	}

	/**
	 * @return the taskCreatedBy
	 */
	public String getTaskCreatedBy() {
		return taskCreatedBy;
	}

	/**
	 * @param taskCreatedBy the taskCreatedBy to set
	 */
	public void setTaskCreatedBy(String taskCreatedBy) {
		this.taskCreatedBy = taskCreatedBy;
	}

	/**
	 * @return the taskCreatedOn
	 */
	public String getTaskCreatedOn() {
		return taskCreatedOn;
	}

	/**
	 * @param taskCreatedOn the taskCreatedOn to set
	 */
	public void setTaskCreatedOn(String taskCreatedOn) {
		this.taskCreatedOn = taskCreatedOn;
	}

	/**
	 * @return the taskActivationTime
	 */
	public String getTaskActivationTime() {
		return taskActivationTime;
	}

	/**
	 * @param taskActivationTime the taskActivationTime to set
	 */
	public void setTaskActivationTime(String taskActivationTime) {
		this.taskActivationTime = taskActivationTime;
	}

	/**
	 * @return the taskExpirationTime
	 */
	public String getTaskExpirationTime() {
		return taskExpirationTime;
	}

	/**
	 * @param taskExpirationTime the taskExpirationTime to set
	 */
	public void setTaskExpirationTime(String taskExpirationTime) {
		this.taskExpirationTime = taskExpirationTime;
	}

	/**
	 * 
	 * @return the taskProcDefId
	 */
	public String getTaskProcDefId() {
		return taskProcDefId;
	}

	/**
	 * 
	 * @param taskProcDefId the taskProcDefId to set
	 */
	public void setTaskProcDefId(String taskProcDefId) {
		this.taskProcDefId = taskProcDefId;
	}

	/**
	 * 
	 * @return the containerId
	 */
	public String getContainerId() {
		return containerId;
	}

	/**
	 * 
	 * @param containerId the containerId to set
	 */
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	/**
	 * 
	 * @return the instanceId
	 */
	public String getInstanceId() {
		return instanceId;
	}

	/**
	 * 
	 * @param instanceId the instanceId to set
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

}
