package com.soaint.orquestacionBpm.commons.domains.request.novedades;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDetallesNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDetallesNovedadRadicadoDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo.DocumentoDTO;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo.ResolverGuardadoDocumentosDTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa del guardado de documentos dependiendo de la novedad")

public class RequestResolverIngresoNovedadesDTO {

    @RequiredParameter
    @ApiModelProperty(notes = "Datos de la novedad que se va a registrar")
    private RequestInsertDetallesNovedadDTO novedad;

    @ApiModelProperty(notes = "Datos de documentos a utilizar")
    private ResolverGuardadoDocumentosDTO documentos;

    @ApiModelProperty(notes = "Datos de documentos a utilizar")
    private RequestInsertDetallesNovedadRadicadoDTO novedadRadicado;

    @RequiredParameter
    @ApiModelProperty("Token asociados al negocio")
    private TokenDTO tokenAutenticatedBusiness;

}
