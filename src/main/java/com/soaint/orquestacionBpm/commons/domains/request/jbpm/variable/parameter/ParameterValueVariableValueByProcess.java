package com.soaint.orquestacionBpm.commons.domains.request.jbpm.variable.parameter;

import lombok.Data;

import java.io.Serializable;

@Data

public class ParameterValueVariableValueByProcess implements Serializable {

    private String numeroSolicitud;
    private String estadoSolicitud;
    private String key;
}
