package com.soaint.orquestacionBpm.commons.domains.request.cooperativa;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los parámetros de Contactos")
public class RequestGetPosLaboralDTO {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Tipo Entidad")
    private String tipoIdentContacto;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Codigo Entidad")
    private BigDecimal numeroIdentContacto;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "nombre Entidad")
    private String tipoIdentEntidad;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Nit Entidad")
    private BigDecimal numeroIdentEntidad;
}
