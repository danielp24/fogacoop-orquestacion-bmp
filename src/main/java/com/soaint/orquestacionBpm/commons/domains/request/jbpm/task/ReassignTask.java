package com.soaint.orquestacionBpm.commons.domains.request.jbpm.task;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.Assignment;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
public class ReassignTask implements Serializable {

    public Assignment assignment;
}
