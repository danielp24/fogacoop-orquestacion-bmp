package com.soaint.orquestacionBpm.commons.domains.request.jbpm.signal;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import com.soaint.orquestacionBpm.commons.domains.request.jbpm.signal.parameter.SignalProcessSignal;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor

public class ProcessSignal implements Serializable {

    private String containerId;
    private String processInstance;
    private OwnerUser ownerUser;
    private SignalProcessSignal signal;
}
