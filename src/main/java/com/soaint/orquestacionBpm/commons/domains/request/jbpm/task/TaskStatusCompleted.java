package com.soaint.orquestacionBpm.commons.domains.request.jbpm.task;


import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import com.soaint.orquestacionBpm.commons.domains.request.jbpm.task.parameter.ParametersTaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class TaskStatusCompleted implements Serializable {

    private String containerId;
    private Integer taskId;
    private String taskStatus;
    private OwnerUser ownerUser;
    private ParametersTaskStatus parametros;
}
