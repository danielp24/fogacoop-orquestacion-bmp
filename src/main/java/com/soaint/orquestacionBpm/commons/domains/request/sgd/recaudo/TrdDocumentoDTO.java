package com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa del guardado de documentos dependiendo de la novedad")

public class TrdDocumentoDTO {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Dependencia del documento")
    private String dependencia;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Serie del documento")
    private String serie;

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Subserie del documento")
    private String subserie;

    @ApiModelProperty(notes = "Tipo Documental")
    private String tipoDoc;


}
