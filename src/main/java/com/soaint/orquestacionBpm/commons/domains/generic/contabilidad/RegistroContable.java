package com.soaint.orquestacionBpm.commons.domains.generic.contabilidad;

import lombok.*;

import javax.xml.bind.annotation.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@XmlRootElement(name = "registrocontable")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder={
        "contabilidad",
        "tipoComprobante",
        "conceptoComprobante",
        "fecha",
        "numeroMovimiento",
        "periodoFiscal",
        "mes",
        "planCuentas",
        "cuenta",
        "centroUtilidad",
        "nitTercero",
        "documentoSoporte",
        "numeroDocSoporte",
        "numeroVencimiento",
        "fechaEmision",
        "fechaVencimiento",
        "valorDescuento",
        "porcentajeDescuento",
        "valorIva",
        "valorRetefuente",
        "valorBase",
        "valorBaseOrigen",
        "fechaTasaCambio",
        "valorDebitoMonedaOr",
        "valorCreditoMonedaOr",
        "valorDebitoMonedaLoc",
        "valorCreditoMonedaLoc",
        "observaciones",
        "tipoMovimiento",
        "prefijoProveedor",
        "numeroCuotas",
        "fechaDescuento",
        "referencia"
})
public class RegistroContable {

    private String contabilidad = "";
    private String tipoComprobante = "";
    private String conceptoComprobante = "";
    private String fecha = "";
    private String numeroMovimiento = "";
    private String periodoFiscal = "";
    private String mes = "";
    private String planCuentas = "";
    private String cuenta = "";
    private String centroUtilidad = "";
    private String nitTercero = "";
    private String documentoSoporte = "";
    private String numeroDocSoporte = "";
    private String numeroVencimiento = "";
    private String fechaEmision = "";
    private String fechaVencimiento = "";
    private String valorDescuento = "";
    private String porcentajeDescuento = "";
    private String valorIva = "";
    private String valorRetefuente = "";
    private String valorBase = "";
    private String valorBaseOrigen = "";
    private String fechaTasaCambio = "";
    private String valorDebitoMonedaOr = "";
    private String valorCreditoMonedaOr = "";
    private String valorDebitoMonedaLoc = "";
    private String valorCreditoMonedaLoc = "";
    private String observaciones = "";
    private String tipoMovimiento = "";
    private String prefijoProveedor = "";
    private String numeroCuotas = "";
    private String fechaDescuento = "";
    private String referencia = "";

    public RegistroContable(){
        this.valorDescuento = "0";
        this.porcentajeDescuento = "0";
        this.valorIva = "0";
        this.valorRetefuente = "0";
        this.valorBase = "0";
        this.valorBaseOrigen = "0";
        this.valorDebitoMonedaOr = "0";
        this.valorCreditoMonedaOr = "0";
        this.valorCreditoMonedaOr = "0";
        this.numeroVencimiento = "1";
        this.numeroMovimiento = "1";
        this.valorDebitoMonedaLoc = "0";
        this.valorCreditoMonedaLoc = "0";
    }

}
