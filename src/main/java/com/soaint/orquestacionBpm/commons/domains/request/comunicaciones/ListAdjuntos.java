package com.soaint.orquestacionBpm.commons.domains.request.comunicaciones;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ListAdjuntos {

    private String nombre;
    private String contentType;
    private String tipoAdjunto;
    private String fileBase64;
}
