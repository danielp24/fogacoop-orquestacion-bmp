package com.soaint.orquestacionBpm.commons.domains.request.cooperativa;

import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa de la entrada de datos para enviar un correo")

public class RequestGeNombresContactoDTO {

    @ApiModelProperty(notes = "Tipo Id del conatcto")
    private String tipoId;

    @ApiModelProperty(notes = "Num Id del conatcto")
    private String numId;
}
