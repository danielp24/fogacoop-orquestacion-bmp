package com.soaint.orquestacionBpm.commons.domains.request.sgd.repository;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredParameter;
import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo.DocumentoDTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiOperation("Clase representativa del guardado de documentos dependiendo de la novedad")

public class RequestGetDocumentRepositoryDTO {


    @ApiModelProperty(notes = "Id SGDA documento")
    private BigDecimal idDocSgdea;

    @ApiModelProperty(notes = "Ruta de de documento")
    private String rutaDocumento;

}
