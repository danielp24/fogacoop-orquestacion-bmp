package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 
 * @author jjmorales
 *
 */
@ApiModel(description = "Representative class of the rule decisions.")
public class DecisionDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Rule decision identifier.")
	private String decisionId;
	
	@ApiModelProperty(notes = "Rule decision name.")
	private String decisionName;
	
	@ApiModelProperty(notes = "Rule decision result.")
	private boolean decisionResult;
	
	@ApiModelProperty(notes = "Rule decision status.")
	private String decisionStatus;
	
	public DecisionDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @return the decisionId
	 */
	public String getDecisionId() {
		return decisionId;
	}

	/**
	 * 
	 * @param decisionId the decisionId to set
	 */
	public void setDecisionId(String decisionId) {
		this.decisionId = decisionId;
	}

	/**
	 * 
	 * @return the decisionName
	 */
	public String getDecisionName() {
		return decisionName;
	}

	/**
	 * 
	 * @param decisionName the decisionName to set
	 */
	public void setDecisionName(String decisionName) {
		this.decisionName = decisionName;
	}

	/**
	 * 
	 * @return the decisionResult
	 */
	public boolean isDecisionResult() {
		return decisionResult;
	}

	/**
	 * 
	 * @param decisionResult the decisionResult to set
	 */
	public void setDecisionResult(boolean decisionResult) {
		this.decisionResult = decisionResult;
	}

	/**
	 * 
	 * @return the decisionStatus
	 */
	public String getDecisionStatus() {
		return decisionStatus;
	}

	/**
	 * 
	 * @param decisionStatus the decisionStatus to set
	 */
	public void setDecisionStatus(String decisionStatus) {
		this.decisionStatus = decisionStatus;
	}
	
}
