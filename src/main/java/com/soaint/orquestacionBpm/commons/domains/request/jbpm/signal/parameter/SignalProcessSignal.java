package com.soaint.orquestacionBpm.commons.domains.request.jbpm.signal.parameter;

import lombok.Data;

import java.io.Serializable;

@Data

public class SignalProcessSignal implements Serializable {

    private String signalName;
    private ParameterProcessSignal parametros;
}
