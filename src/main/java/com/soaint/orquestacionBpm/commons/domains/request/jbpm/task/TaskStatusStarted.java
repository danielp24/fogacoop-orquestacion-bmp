package com.soaint.orquestacionBpm.commons.domains.request.jbpm.task;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class TaskStatusStarted implements Serializable {

    public String containerId;
    public Integer taskId;
    public String taskStatus;
    public OwnerUser ownerUser;
}
