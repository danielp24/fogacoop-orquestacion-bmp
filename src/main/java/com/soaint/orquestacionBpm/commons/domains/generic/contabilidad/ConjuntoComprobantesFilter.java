package com.soaint.orquestacionBpm.commons.domains.generic.contabilidad;

import lombok.*;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ConjuntoComprobantesFilter {

    private Map<String, List<ComprobanteFilter>> comprobantes;

}
