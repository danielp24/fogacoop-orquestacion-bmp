package com.soaint.orquestacionBpm.commons.domains.request.comunicaciones;

import com.soaint.orquestacionBpm.commons.Utils.annotations.RequiredPrimitiveParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Clase representativa de los parámetros de GenericBusiness")
public class RequestGenericJsonDTO {

    @RequiredPrimitiveParameter
    @ApiModelProperty(notes = "Json request generico a resolver en backend")
    private String json;

}
