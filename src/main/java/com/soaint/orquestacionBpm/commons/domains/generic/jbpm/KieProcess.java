/**
 *
 */
package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovillamil
 *
 */
public class KieProcess {


    private boolean associatedEntities;

    private boolean serviceTasks;

    private boolean processVariables;

    private boolean reusableSubProcesses;

    private boolean nodes;

    private boolean timers;

    private String processInstanceId;

    private String initiator;

    private Timestamp startDate;

    private String processId;

    private String processName;

    private String processVersion;

    private String _package;

    private String containerId;

    private Boolean dynamic;

    private String processInstanceDesc;

    private String correlationKey;

    private Integer parentInstanceId;

    private Integer slaCompliance;

    private Timestamp slaDueDate;

    private TaskResponse activeUserTasks;

    private String processInstanceVariables;

    /**
     *
     */
    public KieProcess() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @return the associatedEntities
     */
    public boolean isAssociatedEntities() {
        return associatedEntities;
    }

    /**
     * @param associatedEntities the associatedEntities to set
     */
    public void setAssociatedEntities(boolean associatedEntities) {
        this.associatedEntities = associatedEntities;
    }

    /**
     * @return the serviceTasks
     */
    public boolean isServiceTasks() {
        return serviceTasks;
    }

    /**
     * @param serviceTasks the serviceTasks to set
     */
    public void setServiceTasks(boolean serviceTasks) {
        this.serviceTasks = serviceTasks;
    }

    /**
     * @return the processVariables
     */
    public boolean isProcessVariables() {
        return processVariables;
    }

    /**
     * @param processVariables the processVariables to set
     */
    public void setProcessVariables(boolean processVariables) {
        this.processVariables = processVariables;
    }

    /**
     * @return the reusableSubProcesses
     */
    public boolean isReusableSubProcesses() {
        return reusableSubProcesses;
    }

    /**
     * @param reusableSubProcesses the reusableSubProcesses to set
     */
    public void setReusableSubProcesses(boolean reusableSubProcesses) {
        this.reusableSubProcesses = reusableSubProcesses;
    }

    /**
     * @return the nodes
     */
    public boolean isNodes() {
        return nodes;
    }

    /**
     * @param nodes the nodes to set
     */
    public void setNodes(boolean nodes) {
        this.nodes = nodes;
    }

    /**
     * @return the timers
     */
    public boolean isTimers() {
        return timers;
    }

    /**
     * @param timers the timers to set
     */
    public void setTimers(boolean timers) {
        this.timers = timers;
    }

    /**
     * @return the processInstanceId
     */
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    /**
     * @param processInstanceId the processInstanceId to set
     */
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    /**
     * @return the initiator
     */
    public String getInitiator() {
        return initiator;
    }

    /**
     * @param initiator the initiator to set
     */
    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    /**
     * @return the startDate
     */
    public Timestamp getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the processId
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * @param processId the processId to set
     */
    public void setProcessId(String processId) {
        this.processId = processId;
    }

    /**
     * @return the processName
     */
    public String getProcessName() {
        return processName;
    }

    /**
     * @param processName the processName to set
     */
    public void setProcessName(String processName) {
        this.processName = processName;
    }

    /**
     * @return the processVersion
     */
    public String getProcessVersion() {
        return processVersion;
    }

    /**
     * @param processVersion the processVersion to set
     */
    public void setProcessVersion(String processVersion) {
        this.processVersion = processVersion;
    }

    /**
     * @return the _package
     */
    public String get_package() {
        return _package;
    }

    /**
     * @param _package the _package to set
     */
    public void set_package(String _package) {
        this._package = _package;
    }

    /**
     * @return the containerId
     */
    public String getContainerId() {
        return containerId;
    }

    /**
     * @param containerId the containerId to set
     */
    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    /**
     * @return the dynamic
     */
    public Boolean getDynamic() {
        return dynamic;
    }

    /**
     * @param dynamic the dynamic to set
     */
    public void setDynamic(Boolean dynamic) {
        this.dynamic = dynamic;
    }

    /**
     * @return the processInstanceDesc
     */
    public String getProcessInstanceDesc() {
        return processInstanceDesc;
    }

    /**
     * @param processInstanceDesc the processInstanceDesc to set
     */
    public void setProcessInstanceDesc(String processInstanceDesc) {
        this.processInstanceDesc = processInstanceDesc;
    }

    /**
     * @return the correlationKey
     */
    public String getCorrelationKey() {
        return correlationKey;
    }

    /**
     * @param correlationKey the correlationKey to set
     */
    public void setCorrelationKey(String correlationKey) {
        this.correlationKey = correlationKey;
    }

    /**
     * @return the parentInstanceId
     */
    public Integer getParentInstanceId() {
        return parentInstanceId;
    }

    /**
     * @param parentInstanceId the parentInstanceId to set
     */
    public void setParentInstanceId(Integer parentInstanceId) {
        this.parentInstanceId = parentInstanceId;
    }

    /**
     * @return the slaCompliance
     */
    public Integer getSlaCompliance() {
        return slaCompliance;
    }

    /**
     * @param slaCompliance the slaCompliance to set
     */
    public void setSlaCompliance(Integer slaCompliance) {
        this.slaCompliance = slaCompliance;
    }

    /**
     * @return the slaDueDate
     */
    public Timestamp getSlaDueDate() {
        return slaDueDate;
    }

    /**
     * @param slaDueDate the slaDueDate to set
     */
    public void setSlaDueDate(Timestamp slaDueDate) {
        this.slaDueDate = slaDueDate;
    }

    /**
     * @return the activeUserTasks
     */
    public TaskResponse getActiveUserTasks() {
        return activeUserTasks;
    }

    /**
     * @param activeUserTasks the activeUserTasks to set
     */
    public void setActiveUserTasks(TaskResponse activeUserTasks) {
        this.activeUserTasks = activeUserTasks;
    }

    /**
     * @return the processInstanceVariables
     */
    public String getProcessInstanceVariables() {
        return processInstanceVariables;
    }

    /**
     * @param processInstanceVariables the processInstanceVariables to set
     */
    public void setProcessInstanceVariables(String processInstanceVariables) {
        this.processInstanceVariables = processInstanceVariables;
    }

}
