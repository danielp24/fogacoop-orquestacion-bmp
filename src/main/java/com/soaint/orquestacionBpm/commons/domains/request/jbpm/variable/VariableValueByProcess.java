package com.soaint.orquestacionBpm.commons.domains.request.jbpm.variable;

import com.soaint.orquestacionBpm.commons.domains.generic.accounts.OwnerUser;
import com.soaint.orquestacionBpm.commons.domains.request.jbpm.variable.parameter.ParameterVariableValueByProcess;

import java.io.Serializable;

public class VariableValueByProcess implements Serializable {

    private String containerId;
    private String processInstance;
    private OwnerUser ownerUser;
    private ParameterVariableValueByProcess parametros;
}
