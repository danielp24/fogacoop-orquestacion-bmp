/**
 * 
 */
package com.soaint.orquestacionBpm.commons.domains.generic.jbpm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the process container")
public class ContainerDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Unique identifier of the container.")
	private String containerId;
	
	@ApiModelProperty(notes = "Version of container.")
	private String version;
	
	@ApiModelProperty(notes = "Status of container.")
	private String status;
	
	@ApiModelProperty(notes = "Alias Name for each container.")
	private String containerAlias;

	@ApiModelProperty(notes = "List of Process associate to each container.")
	private List<ProcessesDto> processes;
	
	@ApiModelProperty(notes = "List of possible responses for each container.")
	private List<MessagesDto> messages;
	
	@ApiModelProperty(notes = "List of rules associate to each container.")
	private List<RulesDto> rules;

	/**
	 * 
	 */
	public ContainerDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the containerId
	 */
	public String getContainerId() {
		return containerId;
	}

	/**
	 * @param containerId the containerId to set
	 */
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the containerAlias
	 */
	public String getContainerAlias() {
		return containerAlias;
	}

	/**
	 * @param containerAlias the containerAlias to set
	 */
	public void setContainerAlias(String containerAlias) {
		this.containerAlias = containerAlias;
	}

	/**
	 * @return the processes
	 */
	public List<ProcessesDto> getProcesses() {
		return processes;
	}

	/**
	 * @param processes the processes to set
	 */
	public void setProcesses(List<ProcessesDto> processes) {
		this.processes = processes;
	}

	/**
	 * @return the messages
	 */
	public List<MessagesDto> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<MessagesDto> messages) {
		this.messages = messages;
	}

	/**
	 * 
	 * @return the rules
	 */
	public List<RulesDto> getRules() {
		return rules;
	}

	/**
	 * 
	 * @param rules the rules to set
	 */
	public void setRules(List<RulesDto> rules) {
		this.rules = rules;
	}
	
}
