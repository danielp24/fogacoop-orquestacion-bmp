package com.soaint.orquestacionBpm.commons.Utils.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlUtils {


    public static String jaxbObjectToXMLString(Object obj) {
        try {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(obj, sw);

            //Verify XML Content
            String xmlContent = sw.toString();
            return xmlContent;

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return "BAD_CONVERTER";
    }


    public static <T> T xmlStringToObject(Class<T> clazz, String xml)
            throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        T obj = clazz.cast(unmarshaller.unmarshal(new StringReader(xml)));
        return obj;
    }


    public static <T> T xmlToObject(Class<T> clazz, String urlXml)
            throws JAXBException {

        File xmlFile = new File(urlXml);
        JAXBContext jc = JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        T obj = clazz.cast(unmarshaller.unmarshal(xmlFile));
        return obj;
    }


}
