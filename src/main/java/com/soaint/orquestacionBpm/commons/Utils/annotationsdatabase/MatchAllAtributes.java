package com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The Interface PayloadAttribute.
 */
@Retention(RetentionPolicy.RUNTIME)

public @interface MatchAllAtributes {
}
