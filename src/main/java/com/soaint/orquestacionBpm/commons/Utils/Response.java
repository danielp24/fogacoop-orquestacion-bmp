package com.soaint.orquestacionBpm.commons.Utils;

public class Response {

    public void setType(ResponseType type) {
        this.type = type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    public ResponseType getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String[] getData() {
        return data;
    }

    public Response() {
    }

    private ResponseType type = ResponseType.EXCEPTION;
    private String message = "";
    private String[] data = {};

}