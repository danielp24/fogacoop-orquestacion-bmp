package com.soaint.orquestacionBpm.commons.Utils.business;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.user.IUserApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.business.RequestAuthtenticateDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseAuthenticateDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser.BodyGeneralGetListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg.BodyGeneralListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login.ResponseUser;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.login.Role;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.UserDto;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BusinessUtils {

    private static String PRIVILEGES_BPM_ADMIN;

    public static TokenDTO authentication(IBusinessApiClient businessApiClient, UserDto ownerUser) throws WebClientException {
        TokenDTO tokenBusiness = null;

        try {
            //Iniciar Authenticaciones
            RequestAuthtenticateDTO reqAuth = new RequestAuthtenticateDTO();
            reqAuth.setExpire(Boolean.FALSE);
            reqAuth.setUsername(ownerUser.getUser());
            reqAuth.setPassword(ownerUser.getPassword());

            ResponseAuthenticateDTO resAuth = businessApiClient.authenticate(reqAuth);
            tokenBusiness = new TokenDTO();
            if(resAuth.getBody().getToken() != null && !"".equals(resAuth.getBody().getToken())){
                tokenBusiness.setTokenBusiness(resAuth.getBody().getToken());
            }
        } catch (Exception e) {
            LogUtils.error(e);
            throw new WebClientException(BusinessException.codeBadOperationGeneric, "Error: " + e.getMessage());
        }

        return tokenBusiness;
    }

    public static TokenDTO authenticationWithAdminJbpm(IUserApiClient userApiClient, IBusinessApiClient businessApiClient, UserDto ownerUser) throws WebClientException {
        ResponseUser responseUser = userApiClient.login(ownerUser.getUser(), ownerUser.getPassword());
        Boolean isGranted = Boolean.FALSE;
        for (Role rol : responseUser.getBody().getBody().getRoles()) {
            isGranted = rol.getPrivileges().stream().anyMatch(
                    priv -> PRIVILEGES_BPM_ADMIN.equals(priv.getName())
            );
        }
        if (!isGranted) {
            throw new WebClientException(BusinessException.codeInvalidGrant, BusinessException.valueInvalidGrant);
        }
        return authentication(businessApiClient, ownerUser);

    }

    @Autowired
    private void setBPMPrivilegeAdmin(@Value("${privilege.bpm.admin}") String value) {
        PRIVILEGES_BPM_ADMIN = value;
    }
}
