package com.soaint.orquestacionBpm.commons.Utils.list;


import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAttribute;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.NoMatchAttribute;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

@Component
public class ListUtils {

    public static Boolean IDENTIFY_NULL_VALUES;
    public static Boolean RESULT_SIZE;
    private static RestTemplate template;


    @Autowired
    private void setIdentifyNullValue(@Value("${generic.logs.identifyNullValues}") String value) {
        IDENTIFY_NULL_VALUES = Boolean.parseBoolean(value);
    }

    @Autowired
    private void setResultSize(@Value("${generic.logs.resultSize}") String value) {
        RESULT_SIZE = Boolean.parseBoolean(value);
    }

    private void setRestTemplate() {
        template = new RestTemplate();
    }


    public static void cargarListaFromQueryResult(List lista, Class clase, StoredProcedureQuery query)  throws Exception{
        cargarListaFromQueryResult(lista, clase, query.getResultList());
    }


    public static void cargarListaFromQueryResult(List lista, Class clase, Query query)  throws Exception{
        cargarListaFromQueryResult(lista, clase, query.getResultList());
    }

    private static void cargarListaFromQueryResult(List lista, Class clase, List<Object[]> resul) throws Exception{
        try {
            if (ListUtils.RESULT_SIZE)
                System.out.println(">>> Size List [" + clase.getName() + "] = " + resul.size() + " <<<");
            if(resul.size()>0){
                Iterator<Object[]> rsi = resul.iterator();
                int k = 0;
                Boolean validateAll = determineMatchAllAtributes(clase);
                while (rsi.hasNext()) {
                    k++;
                    final Object[] row = rsi.next();
                    final Object obj = clase.newInstance();
                    final Field[] attr = obj.getClass().getDeclaredFields();
                    try {
                        int j = 0;
                        int z = determineMatchAttributes(validateAll,clase,attr);

                        for (int i = 0; i < attr.length; i++) {
                            final Field temp = attr[i];
                            temp.setAccessible(true);
                            if (determineAnotations(validateAll,temp)) {
                                if (!java.lang.reflect.Modifier.isStatic(temp.getModifiers())) {
                                    try {
                                        if ((z - row.length) != 0) {
                                            throw new ArrayIndexOutOfBoundsException();
                                        }
                                        if (row[j] == null) {
                                            throw new IllegalArgumentException();
                                        }
                                        temp.set(obj, row[j]);
                                        j++;
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    } catch (ArrayIndexOutOfBoundsException e) {
                                        if (z > row.length){
                                            System.out.println("ERROR:'" + attr[attr.length-1].getName() + "' No encontrado en respuesta de la consulta ");
                                            throw new Exception("ERROR:'" + attr[attr.length-1].getName() + "' No encontrado en respuesta de la consulta ");
                                        }else{
                                            System.out.println("ERROR: El valor '" + ((row[row.length-1] != null) ? row[row.length-1] : "") + "' No tiene un atributo en el DTO '" + obj.getClass().getName() + "' para almacenarse.");
                                            throw new Exception("ERROR: El valor '" + ((row[row.length-1] != null) ? row[row.length-1] : "") + "' No tiene un atributo en el DTO '" + obj.getClass().getName() + "' para almacenarse.");
                                        }

                                    } catch (IllegalArgumentException e) {
                                        //Se debe verificar que el DTO de la consulta tenga el campo
                                        if (ListUtils.IDENTIFY_NULL_VALUES)
                                            System.out.println("INFO: [ObjectList:" + (k) + "] Atributo '" + temp.getName() + "' de la clase '" + obj.getClass().getName() + "' esta recibiendo null.");
                                        j++;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        lista.add(obj);
                    } catch (NoClassDefFoundError e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    private static Boolean determineMatchAllAtributes(Class clase) {
        return clase.isAnnotationPresent(MatchAllAtributes.class);
    }

    public static int determineMatchAttributes(Boolean validateAll,Class clase,Field[] attr){
        int count = 0;
        if(attr.length>0) {
            for (int i = 0; i < attr.length; i++) {
                final Field temp = attr[i];
                temp.setAccessible(true);
                if (determineAnotations(validateAll,temp)) {
                    count++;
                }
            }
        }
        return count;
    }

    public static Boolean determineAnotations(Boolean validateAll, Field temp){
        Boolean validate = Boolean.FALSE;
        if(!validateAll) {
            if (temp.isAnnotationPresent(MatchAttribute.class)) {
                validate = Boolean.TRUE;
            } else if (temp.isAnnotationPresent(NoMatchAttribute.class)) {
                validate = Boolean.FALSE;
            } else {
                validate = Boolean.TRUE;
            }
        }else{
            if (temp.isAnnotationPresent(NoMatchAttribute.class)) {
                validate = Boolean.FALSE;
            }else{
                validate = Boolean.TRUE;
            }
        }
        return validate;
    }

    public static void cargarObjectFromQueryResult(Object object, Query query)  throws Exception{
        cargarObjectFromQueryResult(object,query.getResultList());
    }

    private static void cargarObjectFromQueryResult(Object object, List<Object[]> resul) throws Exception{
        try {
            if (ListUtils.RESULT_SIZE)
                System.out.println(">>> Size List [" + object.getClass().getName() + "] = " + resul.size() + " <<<");
            if(resul.size()>0){
                Iterator<Object[]> rsi = resul.iterator();
                int k = 0;
                Boolean validateAll = determineMatchAllAtributes(object.getClass());
                while (rsi.hasNext()) {
                    k++;
                    final Object row = rsi.next();
                    final Field[] attr = object.getClass().getDeclaredFields();
                    try {
                        determineMatchAttributes(validateAll,object.getClass(),attr);

                        for (int i = 0; i < attr.length; i++) {
                            final Field temp = attr[i];
                            temp.setAccessible(true);
                            if (determineAnotations(validateAll,temp)) {
                                if (!java.lang.reflect.Modifier.isStatic(temp.getModifiers())) {
                                    try {
                                        if (row == null) {
                                            throw new IllegalArgumentException();
                                        }
                                        temp.set(object, row);
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    } catch (IllegalArgumentException e) {
                                        //Se debe verificar que el DTO de la consulta tenga el campo
                                        if (ListUtils.IDENTIFY_NULL_VALUES)
                                            System.out.println("INFO: [ObjectList:" + (k) + "] Atributo '" + temp.getName() + "' de la clase '" + object.getClass().getName() + "' esta recibiendo null.");
                                    } catch (Exception e) {
                                        LogUtils.error(e);
                                    }
                                }
                            }
                        }
                    } catch (NoClassDefFoundError e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            LogUtils.error(e);
            throw new Exception(e);
        }
    }




}
