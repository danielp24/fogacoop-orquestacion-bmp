package com.soaint.orquestacionBpm.commons.Utils.log;

public enum TipoLog {
    DEBUG, ERROR, FATAL, INFO, WARNING
}