package com.soaint.orquestacionBpm.commons.generic;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.soaint.orquestacionBpm.commons.Utils.annotationsdatabase.MatchAllAtributes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@MatchAllAtributes
@ApiModel(description = "Clase representativa de la respuesta de transaccion generica")
public class ResponseGenericTransactionDTO {

    @ApiModelProperty(notes = "Codigo de transaccion")
    private BigDecimal code;
    @ApiModelProperty(notes = "Respuesta de transaccion")
    private String response;
    @ApiModelProperty(notes = "Mensaje de transaccion")
    private String message;

}
