package com.soaint.orquestacionBpm.commons.constants.api.enviarCorreoPlantilla;

public interface EndpointComunicaciones {

    String ENVIAR_CORREO_PLANTILLA = "comunicaciones/enviarCorreoPlantilla";
    String NOTIFICACION_FECHA_PAGO_INSCRITA = "comunicaciones/notiFechaPagoEntidadInscrita";

}
