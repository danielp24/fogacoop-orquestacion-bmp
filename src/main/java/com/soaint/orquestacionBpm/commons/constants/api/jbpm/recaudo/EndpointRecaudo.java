package com.soaint.orquestacionBpm.commons.constants.api.jbpm.recaudo;

public interface EndpointRecaudo {

    String BASE = "/jbpm";
    String INICIAR_PROCESO_RECAUDO="/iniciarLoteProcesosRecaudo";
    String ENVIAR_PAGO_RECAUDO="/enviarPagoRecaudo";

}
