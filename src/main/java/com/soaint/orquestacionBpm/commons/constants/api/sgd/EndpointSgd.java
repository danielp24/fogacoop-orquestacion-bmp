package com.soaint.orquestacionBpm.commons.constants.api.sgd;

public interface EndpointSgd {

    String SGD = "sgd/";
    String RESOLVER_GUARDAR_DOCS_RECAUDO_NOVEDAD = "/resolverGuardarDocumentosRecaudoNovedad";
    String GET_DOCUMENTO_REPOSITORY = "/getDocumentoRepository";


}
