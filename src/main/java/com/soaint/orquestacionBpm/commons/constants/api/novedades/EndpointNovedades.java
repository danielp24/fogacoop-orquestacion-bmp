package com.soaint.orquestacionBpm.commons.constants.api.novedades;

public interface EndpointNovedades {

    String NOVEDADES = "novedades/";
    String RESOLVER_INGRESO_NOVEDAD = "/resolverIngresoNovedad";
    String GET_LISTA_NOVEDADES_DOCUMENTOS = "/getListaDetallesNovedades";


}
