package com.soaint.orquestacionBpm.commons.constants.api.pagos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PagoConstants {


    public static String APPLIED;
    public static String NOT_APPLIED;
    public static String VALUE_NOT_LIQUID;

    //set All PagoConstants
    @Autowired
     private void setPagoConstants(
            @Value("${business.pagos.constants.APPLIED}") String applied,
            @Value("${business.pagos.constants.NOT_APPLIED}") String notApplied,
            @Value("${business.pagos.constants.VALUE_NOT_LIQUID}") String valueNotLiquid

    ) {
        APPLIED = applied;
        VALUE_NOT_LIQUID = valueNotLiquid;
        NOT_APPLIED = notApplied;
    }


}
