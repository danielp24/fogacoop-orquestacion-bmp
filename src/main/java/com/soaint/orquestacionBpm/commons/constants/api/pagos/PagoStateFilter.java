package com.soaint.orquestacionBpm.commons.constants.api.pagos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PagoStateFilter {

    public static String CREATED;
    public static String PENDING;
    public static String OK;
    public static String FAILED;
    public static String NOT_AUTHORIZED;
    public static String NO_APLICADO;
    public static String NO_COMPROBANTE;
    public static String NO_LIQUIDADO;
    public static String EXPIRED;
    public static String EXITOSO;

    //set All PagoStateFilter
    @Autowired
    private void setPagoStateFilter(
            @Value("${business.pagos.state.CREATED}") String created,
            @Value("${business.pagos.state.PENDING}") String pending,
            @Value("${business.pagos.state.OK}") String ok,
            @Value("${business.pagos.state.FAILED}") String failed,
            @Value("${business.pagos.state.NOT_AUTHORIZED}") String notAuthorized,
            @Value("${business.pagos.state.NO_APLICADO}") String noAplicado,
            @Value("${business.pagos.state.NO_LIQUIDADO}") String noLiquidado,
            @Value("${business.pagos.state.NO_COMPROBANTE}") String noComprobante,
            @Value("${business.pagos.state.EXPIRED}") String expired,
            @Value("${business.pagos.state.EXITOSO}") String exitoso
    ) {
        CREATED = created;
        PENDING = pending;
        OK = ok;
        FAILED = failed;
        NOT_AUTHORIZED = notAuthorized;
        NO_APLICADO = noAplicado;
        NO_COMPROBANTE = noComprobante;
        NO_LIQUIDADO = noLiquidado;
        EXPIRED = expired;
        EXITOSO = exitoso;
    }


}
