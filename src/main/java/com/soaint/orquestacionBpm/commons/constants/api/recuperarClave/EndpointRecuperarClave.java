package com.soaint.orquestacionBpm.commons.constants.api.recuperarClave;

public interface EndpointRecuperarClave {

    String RECUPERAR_CONTRASEÑA="users/recoveryPassword";
}
