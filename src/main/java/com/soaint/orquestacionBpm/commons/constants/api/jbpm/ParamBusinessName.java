package com.soaint.orquestacionBpm.commons.constants.api.jbpm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ParamBusinessName {

    //Flujo Recaudo - RecaudoDTO
    public static String RECAUDO_OBJECT;
    public static String TYPE_OBJECT_RECAUDO;
        //Atributes de RecaudoDTO
        public static String ATTR_FECHACORTE;
        public static String ATTR_NIT;
        public static String ATTR_DECISIONFINALRECAUDO;
        public static String ATTR_DECISION_NORMAL;
        public static String ATTR_DECISION_MORA;
        public static String ATTR_SIGNAL_PAGO_INTERMEDIO;
        public static String ATTR_SIGNAL_PAGO_GESTIONRECAUDO;

    //Flujo VerificarPagos - VerificarPagoDTO
    public static String VERIFICAR_PAGO_OBJECT;
    public static String TYPE_OBJECT_VERIFICARPAGO;
        //Atributes de VerificarPagoDTO
        public static String ATTR_TIPOPAGO;
        public static String ATTR_TIPOPAGO_PSE;
        public static String ATTR_TIPOPAGO_CT;
        public static String ATTR_TIPOPAGO_INFOPAGONOLIQ;
        public static String ATTR_ID_TRANSAC;
        public static String ATTR_ENTITYCODE_NAME;
        public static String ATTR_ENTITYCODE_VALUE;
        public static String ATTR_ESTADOPAGO;
        public static String ATTR_ESLIQ;
        public static String ATTR_IDTICKET;

    //Flujo VerificarPagos - VerificarPagoDTO
    public static String OWNERUSER_OBJECT;
    public static String TYPE_OBJECT_OWNERUSER;
        //Atributes de OwnerUserDTO
        public static String ATTR_OWNERUSER_USERNAME;
        public static String ATTR_OWNERUSER_PASSWORD;


    //Flujo VerificarPagos - VerificarPagoDTO
    public static String TOKENS_OBJECT;
    public static String TYPE_OBJECT_TOKENS;
    //Atributes de TOKENS
    public static String ATTR_TOKEN_BUSINESS;


    //set All Paramas Business Name
    @Autowired
     private void setStatesSecurityJbpm(

            //Flujo Recaudo - RecaudoDTO
            @Value("${jbpm.recaudo.object.structure.recaudo}") String recaudo,
            @Value("${jbpm.recaudo.object.structure.objectType}") String objecTypeRecaudo,
            @Value("${jbpm.recaudo.object.structure.fechaCorte}") String fechaCorte,
            @Value("${jbpm.recaudo.object.structure.nit}") String nit,
            @Value("${jbpm.recaudo.object.structure.decisionInicialRecaudo}") String decisionInicialRecaudo,
            @Value("${jbpm.recaudo.object.structure.decision.normal}") String normal,
            @Value("${jbpm.recaudo.object.structure.decision.mora}") String mora,
            @Value("${jbpm.recaudo.object.structure.pago.signal.pagoIntermedio}") String signalNamePagoIntermedio,
            @Value("${jbpm.recaudo.object.structure.pago.signal.pagoGestionRecaudo}") String signalNamePagoGestionRecaudo,

            //Flujo VerificarPagos - VerificarPagoDTO
            @Value("${jbpm.verificarPago.object.structure.verificarPago}") String verificarPago,
            @Value("${jbpm.verificarPago.object.structure.objectTypeVerificarPago}") String objectTypeVerificarPago,
            @Value("${jbpm.verificarPago.object.structure.tipoPago}") String tipoPago,
            @Value("${jbpm.verificarPago.object.structure.tipoPagoPse}") String tipoPagoPse,
            @Value("${jbpm.verificarPago.object.structure.tipoPagoCT}") String tipoPagoCT,
            @Value("${jbpm.verificarPago.object.structure.tipoPagoInforPagoNoLiq}") String tipoPagoInforPagoNoLiq,
            @Value("${jbpm.verificarPago.object.structure.idTransac}") String idTransac,
            @Value("${jbpm.verificarPago.object.structure.entityCodeName}") String entityCodeName,
            @Value("${jbpm.verificarPago.object.structure.entityCodeValue}") String entityCodeValue,
            @Value("${jbpm.verificarPago.object.structure.estadoPago}") String estadoPago,
            @Value("${jbpm.verificarPago.object.structure.esLiq}") String esLiq,
            @Value("${jbpm.verificarPago.object.structure.idTicket}") String idTicket,

            //Flujo VerificarPagos - OwnerUserDTO
            @Value("${jbpm.ownerUser.object.structure.ownerUser}") String ownerUser,
            @Value("${jbpm.ownerUser.object.structure.user}") String user,
            @Value("${jbpm.ownerUser.object.structure.password}") String password,
            @Value("${jbpm.ownerUser.object.structure.objectTypeOwnerUser}") String objectTypeOwnerUser,

            //Flujo VerificarPagos - TokenDTO
            @Value("${jbpm.tokens.object.structure.tokens}") String tokens,
            @Value("${jbpm.tokens.object.structure.business}") String tokenBusiness,
            @Value("${jbpm.tokens.object.structure.objectTypeTokens}") String objectTypeTokens


    ) {
        //Flujo Recaudo - RecaudoDTO
        RECAUDO_OBJECT = recaudo;
        TYPE_OBJECT_RECAUDO = objecTypeRecaudo;
        ATTR_FECHACORTE = fechaCorte;
        ATTR_NIT = nit;
        ATTR_DECISIONFINALRECAUDO = decisionInicialRecaudo;
        ATTR_DECISION_NORMAL = normal;
        ATTR_DECISION_MORA = mora;
        ATTR_SIGNAL_PAGO_INTERMEDIO = signalNamePagoIntermedio;
        ATTR_SIGNAL_PAGO_GESTIONRECAUDO = signalNamePagoGestionRecaudo;

        //Flujo VerificarPagos - VerificarPagoDTO
        VERIFICAR_PAGO_OBJECT = verificarPago;
        TYPE_OBJECT_VERIFICARPAGO = objectTypeVerificarPago;
        ATTR_TIPOPAGO = tipoPago;
        ATTR_TIPOPAGO_PSE = tipoPagoPse;
        ATTR_TIPOPAGO_CT = tipoPagoCT;
        ATTR_TIPOPAGO_INFOPAGONOLIQ = tipoPagoInforPagoNoLiq;
        ATTR_ID_TRANSAC = idTransac;
        ATTR_ENTITYCODE_NAME = entityCodeName;
        ATTR_ENTITYCODE_VALUE = entityCodeValue;
        ATTR_ESTADOPAGO = estadoPago;
        ATTR_ESLIQ = esLiq;
        ATTR_IDTICKET = idTicket;

        //Flujo VerificarPagos - OwnerUserDTO
        OWNERUSER_OBJECT = ownerUser;
        TYPE_OBJECT_OWNERUSER = objectTypeOwnerUser;
        ATTR_OWNERUSER_USERNAME = user;
        ATTR_OWNERUSER_PASSWORD = password;

        //Flujo VerificarPagos - TokenDTO
        TOKENS_OBJECT = tokens;
        TYPE_OBJECT_TOKENS = objectTypeTokens;
        ATTR_TOKEN_BUSINESS = tokenBusiness;

    }


}
