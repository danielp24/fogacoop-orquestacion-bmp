package com.soaint.orquestacionBpm.commons.constants.api.sgd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DocumentSendType {


    public static Integer ONLY_SGD;
    public static Integer ONLY_FILESYSTEM;
    public static Integer ALL_INTEGRATIONS_DOCS;

    //set All Securitys States
    @Autowired
    private void setStatesSecurityJbpm(
            @Value("${document.type.onlySgd}") String onlySgd,
            @Value("${document.type.onlyFileSystem}") String onlyFileSystem,
            @Value("${document.type.allIntegrationsDocs}") String allIntegrationsDocs
    ) {
        ONLY_SGD = Integer.parseInt(onlySgd);
        ONLY_FILESYSTEM = Integer.parseInt(onlyFileSystem);
        ALL_INTEGRATIONS_DOCS = Integer.parseInt(allIntegrationsDocs);
    }


}
