package com.soaint.orquestacionBpm.commons.constants.api.cooperativa;

public interface EndpointCooperativa {

    String BASE = "/cooperativa";
    String GET_CONTACTOS_ENTIDAD = "/getContactosEntidad";

}
