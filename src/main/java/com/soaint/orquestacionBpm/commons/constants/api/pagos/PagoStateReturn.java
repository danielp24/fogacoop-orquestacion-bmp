package com.soaint.orquestacionBpm.commons.constants.api.pagos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PagoStateReturn {


    public static String PAGAR_TOTAL;
    public static String PAGAR_PARCIAL;
    public static String PAGAR_EXCEDENTE;
    public static String PAGADO;

    //set All Process Version
    @Autowired
     private void setProcessVerison(
            @Value("${business.pagos.state.PAGAR_TOTAL}") String pagarTotal,
            @Value("${business.pagos.state.PAGAR_PARCIAL}") String pagarParcial,
            @Value("${business.pagos.state.PAGAR_EXCEDENTE}") String pagarExcedente,
            @Value("${business.pagos.state.PAGADO}") String pagado
    ) {
        PAGAR_TOTAL = pagarTotal;
        PAGAR_PARCIAL = pagarParcial;
        PAGADO = pagado;
        PAGAR_EXCEDENTE = pagarExcedente;
    }


}
