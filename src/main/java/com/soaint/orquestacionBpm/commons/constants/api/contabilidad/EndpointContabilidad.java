package com.soaint.orquestacionBpm.commons.constants.api.contabilidad;

public interface EndpointContabilidad {

    String BASE = "/contabilidad";
    String ENVIAR_COMPROBANTE = "/enviarComprobante";

}
