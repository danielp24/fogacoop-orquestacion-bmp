package com.soaint.orquestacionBpm.commons.constants.api.securityjbpm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ContainersCurrentVersion {


    public static String CONTAINER;
    public static String GESTION_RECAUDO_PROCESS_ID;
    public static String VERIFICACION_PAGOS_PROCESS_ID;
    public static String GESTION_MORA_PSD_PROCESS_ID;

    //set All Process Version
    @Autowired
     private void setProcessVerison(
            @Value("${process.current.container}") String container,
            @Value("${process.current.gestionRecaudo.processid}") String gestionRecaudoProcessId,
            @Value("${process.current.verificacionPagos.processid}") String verificacionPagosProcessId,
            @Value("${process.current.GestionMoraPSD.processid}") String gestionMoraPSDProcessId
    ) {
        CONTAINER = container;
        GESTION_RECAUDO_PROCESS_ID = gestionRecaudoProcessId;
        VERIFICACION_PAGOS_PROCESS_ID = verificacionPagosProcessId;
        GESTION_MORA_PSD_PROCESS_ID = gestionMoraPSDProcessId;
    }


}
