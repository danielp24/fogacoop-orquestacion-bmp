package com.soaint.orquestacionBpm.commons.constants.api.pagos;

public interface EndpointPagos {

    String BASE = "/pagos";
    String COMPARAR_VALOR_PAGAR ="/consultarCompararValor";
    String INICIAR_VERIFICACION_PAGO ="/iniciarVerificacionPago";

}
