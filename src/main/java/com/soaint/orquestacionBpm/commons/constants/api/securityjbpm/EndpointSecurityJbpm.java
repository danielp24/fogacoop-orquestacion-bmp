package com.soaint.orquestacionBpm.commons.constants.api.securityjbpm;

public interface EndpointSecurityJbpm {

    String BASE ="/security";
    String INSTANCES_BY_STATUS = "/instancesBystatus/";
    String INSTANCES_BY_LAST_FECHACORTE = "/instancesByLastFechaCorte/";
    String INSERT_INSTANCE = "/insertInstance";
    String UPDATE_INSTANCE = "/updateInstance";
    String SEARCH_TOKEN_PASSWORD= "/searchToken/";
    String INSTANCES_LAST_BY_NIT = "/instanceLastByNit/";

}
