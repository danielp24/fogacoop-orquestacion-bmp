package com.soaint.orquestacionBpm.commons.constants.api.securityjbpm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class StatesSecurityJbpm {


    public static Integer STARTED;
    public static Integer FINISH;
    public static String NEW;
    public static String OLD;

    //set All Securitys States
    @Autowired
     private void setStatesSecurityJbpm(
            @Value("${jbpm.security.state.started}") String started,
            @Value("${jbpm.security.state.finish}") String finish,
            @Value("${jbpm.security.state.new}") String pnew,
            @Value("${jbpm.security.state.old}") String old
    ) {
        STARTED = Integer.parseInt(started);
        FINISH = Integer.parseInt(finish);
        NEW = pnew;
        OLD = old;
    }


}
