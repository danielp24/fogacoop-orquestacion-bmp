package com.soaint.orquestacionBpm.service.jbpm.impl;


import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.jbpm.JbpmApiCliente;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessRequestDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessResponseDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessesInstances;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.service.jbpm.IJbpmApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JbpmApiServiceImpl implements IJbpmApiService {

    @Autowired
    private final JbpmApiCliente jbpmApicliente;

    public JbpmApiServiceImpl(JbpmApiCliente jbpmApicliente) {
        this.jbpmApicliente = jbpmApicliente;
    }

    @Override
    public ProcessResponseDto iniciarInstanciaProceso(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        return this.jbpmApicliente.iniciarInstanciaProceso(procesoRecaudoRq);
    }

    @Override
    public ProcessResponseDto eliminarInstancia(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        return this.jbpmApicliente.eliminarInstancia(procesoRecaudoRq);
    }

    @Override
    public ProcessResponseDto iniciarSignal(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        return this.jbpmApicliente.iniciarSignal(procesoRecaudoRq);
    }

    @Override
    public ProcessResponseDto terminarInstancia(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        return this.jbpmApicliente.terminarInstancia(procesoRecaudoRq);
    }

    @Override
    public ProcessesInstances consultarSubprocesosByIdProcess(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        return this.jbpmApicliente.consultarSubprocesosByIdProcess(procesoRecaudoRq);
    }

    @Override
    public ProcessResponseDto instancesByVariable(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        return this.jbpmApicliente.instancesByVariable(procesoRecaudoRq);
    }

    @Override
    public Map<String, Object> getInstanceByProcessId(ProcessRequestDto procesoRecaudoRq) throws WebClientException {
        return this.jbpmApicliente.getInstanceByProcessId(procesoRecaudoRq);
    }
}
