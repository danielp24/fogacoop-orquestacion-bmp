package com.soaint.orquestacionBpm.service.jbpm;

import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessRequestDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessResponseDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessesInstances;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;

import java.util.Map;

public interface IJbpmApiService {

    ProcessResponseDto iniciarInstanciaProceso(ProcessRequestDto procesoRecaudoRq) throws WebClientException;

    ProcessResponseDto eliminarInstancia(ProcessRequestDto procesoRecaudoRq) throws WebClientException;

    ProcessResponseDto iniciarSignal(ProcessRequestDto procesoRecaudoRq) throws WebClientException;

    ProcessResponseDto terminarInstancia(ProcessRequestDto procesoRecaudoRq) throws WebClientException;

    ProcessResponseDto instancesByVariable(ProcessRequestDto procesoRecaudoRq) throws WebClientException;

    ProcessesInstances consultarSubprocesosByIdProcess(ProcessRequestDto procesoRecaudoRq) throws WebClientException;

    Map<String, Object> getInstanceByProcessId(ProcessRequestDto procesoRecaudoRq) throws WebClientException;


}
