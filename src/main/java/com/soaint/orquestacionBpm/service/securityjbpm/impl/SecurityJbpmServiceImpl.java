package com.soaint.orquestacionBpm.service.securityjbpm.impl;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertRecoveryPassword;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestUpdateInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastByNitDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesStatusDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchRecoveryPassDTO;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;
import com.soaint.orquestacionBpm.repository.securityjbpm.ISecurityRepository;
import com.soaint.orquestacionBpm.service.securityjbpm.ISecurityJbpmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityJbpmServiceImpl implements ISecurityJbpmService {

    @Autowired
    private final ISecurityRepository repository;

    public SecurityJbpmServiceImpl(ISecurityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ResponseSearchInstancesStatusDTO> getInstancesByStatus(String status) throws WebClientException {
        return repository.getInstancesByStatus(status);
    }

    @Override
    public List<ResponseGenericTransactionDTO> insertInstance(RequestInsertInstance instance) throws WebClientException {
        return repository.insertInstance(instance);
    }


    @Override
    public List<ResponseGenericTransactionDTO> updateInstance(RequestUpdateInstance instance) throws WebClientException {
        return repository.updateInstance(instance);
    }


    @Override
    public List<ResponseSearchInstancesLastFechaCorteDTO> getInstancesByLastFechaCorte(String fechaCorte) throws WebClientException {
        return repository.getInstancesByLastFechaCorte(fechaCorte);
    }

    @Override
    public Integer insertRecoveryPassword(RequestInsertRecoveryPassword recoveryPassword) throws WebClientException {
        return repository.insertRecoveryPassword(recoveryPassword);
    }

    @Override
    public ResponseSearchRecoveryPassDTO searchTokenPassword (String token)throws WebClientException{
        return repository.searchTokenPassword(token);
    }

    @Override
    public List<ResponseSearchInstancesLastByNitDTO> getLastInstanceByNit(String nit) throws WebClientException {
        return repository.getLastInstanceByNit(nit);
    }
}
