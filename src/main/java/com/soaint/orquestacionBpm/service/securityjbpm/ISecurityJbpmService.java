package com.soaint.orquestacionBpm.service.securityjbpm;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertRecoveryPassword;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestUpdateInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastByNitDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesStatusDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchRecoveryPassDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;

import java.util.List;

public interface ISecurityJbpmService {


    List<ResponseSearchInstancesStatusDTO> getInstancesByStatus(String status) throws WebClientException;

    List<ResponseGenericTransactionDTO> insertInstance(RequestInsertInstance instance) throws WebClientException;

    List<ResponseGenericTransactionDTO> updateInstance(RequestUpdateInstance instance) throws WebClientException;
    List<ResponseSearchInstancesLastFechaCorteDTO> getInstancesByLastFechaCorte(String fechaCorte) throws WebClientException;
    Integer insertRecoveryPassword(RequestInsertRecoveryPassword recoveryPassword) throws WebClientException;
    ResponseSearchRecoveryPassDTO searchTokenPassword(String token) throws WebClientException;
    List<ResponseSearchInstancesLastByNitDTO> getLastInstanceByNit(String nit)throws WebClientException;

}
