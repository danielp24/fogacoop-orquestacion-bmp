package com.soaint.orquestacionBpm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableJpaRepositories
@EnableSwagger2
@EnableAsync
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class OrquestacionBPMApplication {

    public static void main(String[] args) {

        SpringApplication.run(OrquestacionBPMApplication.class, args);
    }

}
