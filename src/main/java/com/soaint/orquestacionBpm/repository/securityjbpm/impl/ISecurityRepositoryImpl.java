package com.soaint.orquestacionBpm.repository.securityjbpm.impl;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertRecoveryPassword;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestUpdateInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastByNitDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesStatusDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchRecoveryPassDTO;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.list.ListUtils;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;
import com.soaint.orquestacionBpm.repository.securityjbpm.ISecurityRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
@Transactional
public class ISecurityRepositoryImpl implements ISecurityRepository {
    @PersistenceContext
    private EntityManager em;

    @Value("${pl.storeprocedure.SP_SEARCH_INSTANCES_STATUS}")
    public String SP_SEARCH_INSTANCES_STATUS;

    @Value("${pl.storeprocedure.FN_INSERT_JBPM_INSTANCE}")
    public String FN_INSERT_JBPM_INSTANCE;

    @Value("${pl.storeprocedure.FN_UPDATE_JBPM_INSTANCE}")
    public String FN_UPDATE_JBPM_INSTANCE;

    @Value("${pl.storeprocedure.SP_SEARCH_INSTANCES_FECHACORTE}")
    public String SP_SEARCH_INSTANCES_FECHACORTE;

    @Value("${pl.storeprocedure.SP_INSERT_RECOVERY_PASSWORD}")
    public String SP_INSERT_RECOVERY_PASSWORD;

    @Value("${pl.storeprocedure.SP_SEARCH_RECOVERY_PASSWORD}")
    public String SP_SEARCH_RECOVERY_PASSWORD;

    @Value("${pl.storeprocedure.SP_SEARCH_LAST_INSTANCE_BY_NIT}")
    public String SP_SEARCH_LAST_INSTANCE_BY_NIT;

    @Override
    public List<ResponseSearchInstancesStatusDTO> getInstancesByStatus(String status) throws WebClientException {
        try {
            List lista = new ArrayList<ResponseSearchInstancesStatusDTO>();
            StoredProcedureQuery query = this.em.createStoredProcedureQuery(SP_SEARCH_INSTANCES_STATUS);
            query.registerStoredProcedureParameter("stateInstance", int.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("INSTANCES", void.class, ParameterMode.REF_CURSOR);
            query.setParameter("stateInstance", Integer.parseInt(status));
            query.execute();

            ListUtils.cargarListaFromQueryResult(lista, ResponseSearchInstancesStatusDTO.class, query);

            return lista;
        } catch (Exception e) {
            ValidationUtils.catchSQlException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public List<ResponseGenericTransactionDTO> insertInstance(RequestInsertInstance instance) throws WebClientException {
        try {

            List<ResponseGenericTransactionDTO> response = new ArrayList<>();
            Query query = this.em.createNativeQuery(FN_INSERT_JBPM_INSTANCE);
            query.setParameter("idInstance", instance.getIdInstance());
            query.setParameter("sequenceInstance", instance.getSequenceInstance());
            query.setParameter("fechaCorte", instance.getFechaCorte());
            query.setParameter("nit", instance.getNit());
            query.setParameter("stateInstance", instance.getStateInstance());

            ListUtils.cargarListaFromQueryResult(response, ResponseGenericTransactionDTO.class, query);

            return response;

        } catch (Exception e) {
            ValidationUtils.catchSQlException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public List<ResponseGenericTransactionDTO> updateInstance(RequestUpdateInstance instance) throws WebClientException {
        try {

            List<ResponseGenericTransactionDTO> response = new ArrayList<>();
            Query query = this.em.createNativeQuery(FN_UPDATE_JBPM_INSTANCE);
            query.setParameter("idInstance", instance.getIdInstance());
            query.setParameter("stateInstance", instance.getStateInstance());

            ListUtils.cargarListaFromQueryResult(response, ResponseGenericTransactionDTO.class, query);

            return response;

        } catch (Exception e) {
            ValidationUtils.catchSQlException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public List<ResponseSearchInstancesLastFechaCorteDTO> getInstancesByLastFechaCorte(String fechaCorte) throws WebClientException {
        try {
            List lista = new ArrayList<ResponseSearchInstancesLastFechaCorteDTO>();
            StoredProcedureQuery query = this.em.createStoredProcedureQuery(SP_SEARCH_INSTANCES_FECHACORTE);
            query.registerStoredProcedureParameter("pFechaCorte", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("INSTANCES", void.class, ParameterMode.REF_CURSOR);
            query.setParameter("pFechaCorte", fechaCorte);
            query.execute();

            ListUtils.cargarListaFromQueryResult(lista, ResponseSearchInstancesLastFechaCorteDTO.class, query);

            return lista;
        } catch (Exception e) {
            ValidationUtils.catchSQlException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public Integer insertRecoveryPassword(RequestInsertRecoveryPassword recoveryPassword) throws WebClientException {
        try {
            StoredProcedureQuery query = this.em.createStoredProcedureQuery(SP_INSERT_RECOVERY_PASSWORD);
            query.registerStoredProcedureParameter("pEmail", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("pToken", String.class, ParameterMode.IN);
            query.setParameter("pEmail", recoveryPassword.getEmail());
            query.setParameter("pToken", recoveryPassword.getToken());

            return query.executeUpdate();

        } catch (Exception e) {
            ValidationUtils.catchSQlException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public ResponseSearchRecoveryPassDTO searchTokenPassword(String token) throws WebClientException {
        try {
            ResponseSearchRecoveryPassDTO list = new ResponseSearchRecoveryPassDTO();
            StoredProcedureQuery query = this.em.createStoredProcedureQuery(SP_SEARCH_RECOVERY_PASSWORD);
            query.registerStoredProcedureParameter("pToken",String.class , ParameterMode.IN);
            query.registerStoredProcedureParameter("RESULT",void.class ,ParameterMode.REF_CURSOR );
            query.setParameter("pToken",token);
            query.execute();

            ListUtils.cargarObjectFromQueryResult(list, query);

            return list;

        } catch (Exception e) {
            ValidationUtils.catchSQlException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }

    @Override
    public List<ResponseSearchInstancesLastByNitDTO> getLastInstanceByNit(String nit) throws WebClientException {
        try {
            List<ResponseSearchInstancesLastByNitDTO> lista = new ArrayList<ResponseSearchInstancesLastByNitDTO>();
            StoredProcedureQuery query = this.em.createStoredProcedureQuery(SP_SEARCH_LAST_INSTANCE_BY_NIT);
            query.registerStoredProcedureParameter("pNit",String.class , ParameterMode.IN);
            query.registerStoredProcedureParameter("INSTANCES",void.class ,ParameterMode.REF_CURSOR );
            query.setParameter("pNit",nit);
            query.execute();
            ListUtils.cargarListaFromQueryResult(lista, ResponseSearchInstancesLastByNitDTO.class, query);

            return lista;

        } catch (Exception e) {
            ValidationUtils.catchSQlException(e);
            throw new WebClientException(BusinessException.codeCatchAllException, e.getMessage());
        }
    }
}
