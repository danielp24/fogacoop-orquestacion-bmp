package com.soaint.orquestacionBpm.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;


@EnableSwagger2
@Configuration
public class SwaggerConfig {
    /**
     * Swagger Doc
     * @return
     */
    @Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Orquestacion")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.soaint.orquestacionBpm"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    /**
     * Aplication Info
     * @return
     */
    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "ORQUESTACION REST API",
                "Esta API orquesta las demás APIS relacionadas",
                "1.0.0",
                "Rest",
                new Contact("SOAINT","http://soaint.com/","soaint@email.com"),
                "2019 - Soaint",
                "http://soaint.com/",
                Collections.emptyList()
        );
    }
}
