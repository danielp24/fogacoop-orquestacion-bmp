package com.soaint.orquestacionBpm.web.api.rest.jbpm;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarLotesProcesoRecaudo;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarProcesoVerificarPagos;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IJbpmApiController {

    ResponseEntity iniciarLotesProcesoRecaudo(RequestIniciarLotesProcesoRecaudo procesoRecaudoRq, HttpServletRequest request) throws WebClientException;
    ResponseEntity enviarPagoProcesoRecaudo(RequestIniciarProcesoVerificarPagos rq, HttpServletRequest request) throws WebClientException;

}