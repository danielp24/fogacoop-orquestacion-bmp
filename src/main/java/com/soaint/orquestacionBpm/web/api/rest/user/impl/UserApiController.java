package com.soaint.orquestacionBpm.web.api.rest.user.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.ISgdApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.user.IUserApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertRecoveryPassword;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.lstAdjuntosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetPlantillasDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetPlantillasDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseDataEntidadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseGetEntityByCodeDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseRecoveryPasswordDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.opeCorreoGuarda.ResponseOpeCorreoDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser.BodyGeneralGetListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.getListUser.BodyListUser;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg.BodyGeneralListUserDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.user.listUserOrg.BodyListUserOrgDTO;
import com.soaint.orquestacionBpm.commons.Utils.date.DateUtil;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.recuperarClave.EndpointRecuperarClave;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.CorreoPlantillaDTO;
import com.soaint.orquestacionBpm.commons.domains.request.user.RequestUserDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.service.securityjbpm.ISecurityJbpmService;
import com.soaint.orquestacionBpm.web.api.rest.comunicaciones.IComunicacionesApiController;
import com.soaint.orquestacionBpm.web.api.rest.user.IUserApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping(value = EndpointBase.BASE)
@Api(description = "Servicio que contiene las operaciones atadadas a la recuperación de contraseña de un usuario.")
public class UserApiController implements IUserApiController {

    @Autowired
    private final IUserApiClient iUserApiClient;

    @Autowired
    private final ISecurityJbpmService iSecurityJbpmService;

    @Autowired
    private final IBusinessApiClient businessApiCliente;

    @Autowired
    private final ISgdApiClient iSgdApiClient;

    @Autowired
    private final IComunicacionesApiController iComunicacionesApiController;
    @Value("1")
    public Integer typeParameter;

    @Value("${api.user.token}")
    public String apiToken;
    @Value("${api.user.organization}")
    public String organization;
    @Value("${header.JWT}")
    public final String AuthorizationJwt = "Authorization";
    @Value("${sgd.authorized.user}")
    public String usuario;
    @Value("${sgd.authorized.password}")
    public String password;
    @Value("${id.dominio}")
    public String idDominio;
    @Value("${id.plantilla}")
    public String idPlantilla;

    public UserApiController(IUserApiClient iUserApiClient, ISecurityJbpmService iSecurityJbpmService, IBusinessApiClient businessApiCliente, ISgdApiClient iSgdApiClient,
                             IComunicacionesApiController comunicaciones) {
        this.iUserApiClient = iUserApiClient;
        this.iSecurityJbpmService = iSecurityJbpmService;
        this.businessApiCliente = businessApiCliente;
        this.iSgdApiClient = iSgdApiClient;
        this.iComunicacionesApiController = comunicaciones;
    }

    // Variables de envío notificación opeCorreoGuarda
    @Value("${comunicaciones.helper.object.structure.correoEnvio}")
    public String correoEnvio;

    @Value("${comunicaciones.helper.object.structure.setIdTipNotifica}")
    public String setIdTipNotifica;

    @Value("${comunicaciones.helper.object.structure.userService}")
    public String setUsuario;

    @PostMapping(EndpointRecuperarClave.RECUPERAR_CONTRASEÑA + "{email}")
    @ApiOperation("Controlador que envia correo de recuperación de contraseña")
    public ResponseEntity recoveryPassword(@RequestBody RequestUserDTO dto, HttpServletRequest request) {

        ResponseBuilder responseBuilder = ResponseBuilder.newBuilder();
        try {
            String tokenNoConvert;
            String tokenConvert = "";
            Date fecha = new Date();
            String usuarioLogin = "";
            ResponseOpeCorreoDTO correoDTO = new ResponseOpeCorreoDTO();
            String org = "";
            Integer codEntity = 0;
            String nit = "";
            List<String> bussineList = new ArrayList<>();
            List<lstAdjuntosDTO> adjuntosDTOList = null;
            ResponseEntity passwordDTO = null;


            //Listar Usuarios
            BodyGeneralGetListUserDTO listUserDTO = this.iUserApiClient.getListUser(apiToken);
            //recorrer lista para sacar organizacion
            for (BodyListUser generalListUserDTO : listUserDTO.getBody().getBody()) {
                if (dto.getEmail().equals((generalListUserDTO.getEmail()))) {
                    org = generalListUserDTO.getOrganization();
                }
            }
            codEntity = Integer.valueOf(org);
            //Consumo de Listas por Organizacion
            BodyGeneralListUserDTO listUserOrg = this.iUserApiClient.listUserOrg(apiToken, org);

            //Se consultan los datos de entidad según el código
            ResponseDataEntidadDTO dataEntity = this.businessApiCliente.getEntidadByCode(codEntity, dto.getTokenAutenticatedBusiness());

            for (ResponseGetEntityByCodeDTO entity : dataEntity.getBody()) {
                nit = String.valueOf(entity.getNit());
            }

            //Validación para correo existente.
            for (BodyListUserOrgDTO orgDTO : listUserOrg.getBody().getBody()) {
                if (dto.getEmail().equals(orgDTO.getEmail())) {

                    usuarioLogin = orgDTO.getUserName();

                    tokenNoConvert = usuarioLogin + "," + dto.getEmail() + fecha.toString() + DateUtil.getTimeFromDate(fecha);
                    tokenConvert = UserHelperController.converBase64(tokenNoConvert);

                    RequestInsertRecoveryPassword spRecovery = UserHelperController.buildRequestRecovery(tokenConvert, dto.getEmail());
                    Integer sp = this.iSecurityJbpmService.insertRecoveryPassword(spRecovery);


                    bussineList.add(nit);
                    bussineList.add(tokenConvert);

                    CorreoPlantillaDTO plantillaDTO = UserHelperController.buildBussinesParameters(
                            idPlantilla, idDominio, nit, bussineList, typeParameter,
                            adjuntosDTOList, dto.getTokenAutenticatedBusiness());
                    if (sp == -1) {
                        passwordDTO = iComunicacionesApiController.enviarCorreoPlantilla(plantillaDTO, request);
                    }


                } else {
                    responseBuilder.withResponse(passwordDTO)
                            .withStatus(HttpStatus.BAD_REQUEST)
                            .withBusinessStatus(BusinessException.cEmailErrorException)
                            .withMessage(BusinessException.vEmailErrorException)
                            .withTransactionState(TransactionState.FAIL);
                }
            }
            //Excepciones y respuesta
            if (passwordDTO.getBody() != null) {
                responseBuilder.withResponse(passwordDTO)
                        .withStatus(HttpStatus.OK)
                        .withMessage("Correo de recuperación enviado.");
            } else {
                throw new WebClientException(BusinessException.codeCatchAllException,
                        BusinessException.valueBadOperationGeneric + correoDTO.getBody().getObjRta().getMensaje());
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            responseBuilder.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeFailList)
                    .withMessage(BusinessException.valueFailList)
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            LogUtils.error(e);
            responseBuilder.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return responseBuilder
                    .withPath(request.getRequestURI())
                    .withTransactionState(TransactionState.OK)
                    .buildResponse();
        }
    }
}
