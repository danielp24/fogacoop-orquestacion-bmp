package com.soaint.orquestacionBpm.web.api.rest.jbpm.impl;


import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarProcesoVerificarPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastByNitDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.jbpm.ParamBusinessName;
import com.soaint.orquestacionBpm.commons.constants.api.securityjbpm.ContainersCurrentVersion;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.*;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.service.jbpm.IJbpmApiService;
import com.soaint.orquestacionBpm.service.securityjbpm.ISecurityJbpmService;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JbpmApiHelperAsync  {


    private static void fillInstanceIdGestionMoraPSD(List<ProcessesDto> processes, ProcessRequestDto instance) {
        for (ProcessesDto itemProcess : processes) {
            if (itemProcess.getProcessId().equals(ContainersCurrentVersion.GESTION_MORA_PSD_PROCESS_ID)) {
                instance.setProcessInstance(itemProcess.getInstance().getInstanceId());
            }
        }
    }

    @Async
    public void enviarPagoProcesoRecaudo(ISecurityJbpmService securityJbpmService, IJbpmApiService jbpmApiService, RequestIniciarProcesoVerificarPagos requestService)  {

        try {
            Thread.sleep(5000);
            ProcessResponseDto response = null;
            ProcessRequestDto instance = new ProcessRequestDto();
            ParametrosDto params = new ParametrosDto();
            instance.setParametros(params);

            List<ResponseSearchInstancesLastByNitDTO> regInstance = securityJbpmService.getLastInstanceByNit(requestService.getBasic().getNit());

            //Set Process Instance
            if(regInstance.size()>0)
                instance.setProcessInstance(regInstance.get(0).getIdInstance());
            else
                throw new WebClientException(BusinessException.codeSizeIsEmpty, BusinessException.valueSizeIsEmpty);

            //Set Container Process Recaudo
            instance.setContainerId(ContainersCurrentVersion.CONTAINER);

            //Set Owner User
            instance.setOwnerUser(requestService.getOwnerUser());


            //Set Signal params
            SignalDto signal = new SignalDto();
            signal.setSignalName(ParamBusinessName.ATTR_SIGNAL_PAGO_GESTIONRECAUDO);
            signal.setParametros(params);
            instance.setSignal(signal);

            response = jbpmApiService.iniciarSignal(instance);
            if (!response.getResponse().getStatusCode()
                    .equals(String.valueOf(HttpStatus.OK.value()))) {
                throw new WebClientException(BusinessException.codeBadResponseJbpm,
                        BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
            }

            instance.getSignal().setSignalName(ParamBusinessName.ATTR_SIGNAL_PAGO_INTERMEDIO);

            response = jbpmApiService.iniciarSignal(instance);
            if (!response.getResponse().getStatusCode()
                    .equals(String.valueOf(HttpStatus.OK.value()))) {
                throw new WebClientException(BusinessException.codeBadResponseJbpm,
                        BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
            }

            ValueDto value = new ValueDto();
            value.setKey(ParamBusinessName.ATTR_NIT);
            value.setValue(requestService.getBasic().getNit());
            instance.getParametros().setValue(value);
            ProcessResponseDto resProcessVariable = jbpmApiService.instancesByVariable(instance);
            if (resProcessVariable.getResponse().getStatusCode().equals(String.valueOf(HttpStatus.OK.value()))) {
                if (resProcessVariable.getContainers() != null
                        && resProcessVariable.getContainers().get(0).getProcesses().size() > 0) {
                    JbpmApiHelperAsync.fillInstanceIdGestionMoraPSD(resProcessVariable.getContainers().get(0).getProcesses(), instance);
                    response = jbpmApiService.iniciarSignal(instance);
                    if (!response.getResponse().getStatusCode()
                            .equals(String.valueOf(HttpStatus.OK.value()))) {
                        throw new WebClientException(BusinessException.codeBadResponseJbpm,
                                BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
                    }
                }
            }


        } catch (Exception e) {
            LogUtils.error(e);
        }

    }

    @Async
    public void iniciarProcesoVerificarPago(IJbpmApiService jbpmApiService,
                                                   RequestIniciarProcesoVerificarPagos requestService,
                                                   String idTransac,
                                                   String estadoPago,
                                                   String esLiquidado,
                                                   String idTicket){
        ProcessResponseDto response = null;
        ProcessRequestDto instance = new ProcessRequestDto();
        ParametrosDto params = new ParametrosDto();
        Map<String, Object> values = new HashMap<>();

        try {
            //Set Recaudo Object
            Map<String, Object> objectRecaudo = new HashMap<>();
            Map<String, String> recaudo = new HashMap<>();
            recaudo.put(ParamBusinessName.ATTR_FECHACORTE,requestService.getBasic().getFechaCorte());
            recaudo.put(ParamBusinessName.ATTR_NIT, requestService.getBasic().getNit());
            objectRecaudo.put(ParamBusinessName.TYPE_OBJECT_RECAUDO, recaudo);
            recaudo.put(ParamBusinessName.ATTR_DECISIONFINALRECAUDO,"");
            values.put(ParamBusinessName.RECAUDO_OBJECT, objectRecaudo);

            //Set VerificarPago Object
            Map<String, Object> objectVerificarPago = new HashMap<>();
            Map<String, String> verificarPago = new HashMap<>();
            verificarPago.put(ParamBusinessName.ATTR_ID_TRANSAC,idTransac);
            verificarPago.put(ParamBusinessName.ATTR_ENTITYCODE_NAME,ParamBusinessName.ATTR_ENTITYCODE_VALUE);
            verificarPago.put(ParamBusinessName.ATTR_TIPOPAGO,requestService.getOrigenPago());
            verificarPago.put(ParamBusinessName.ATTR_ESTADOPAGO,estadoPago);
            verificarPago.put(ParamBusinessName.ATTR_ESLIQ,esLiquidado);
            verificarPago.put(ParamBusinessName.ATTR_IDTICKET,idTicket);
            objectVerificarPago.put(ParamBusinessName.TYPE_OBJECT_VERIFICARPAGO,verificarPago);
            values.put(ParamBusinessName.VERIFICAR_PAGO_OBJECT, objectVerificarPago);

            //Set OwnerUser
            Map<String, Object> objectOwnerUser = new HashMap<>();
            Map<String, String> ownerUser = new HashMap<>();
            ownerUser.put(ParamBusinessName.ATTR_OWNERUSER_USERNAME,requestService.getOwnerUser().getUser());
            ownerUser.put(ParamBusinessName.ATTR_OWNERUSER_PASSWORD,requestService.getOwnerUser().getPassword());
            objectOwnerUser.put(ParamBusinessName.TYPE_OBJECT_OWNERUSER,ownerUser);
            values.put(ParamBusinessName.OWNERUSER_OBJECT, objectOwnerUser);

            //Set Tokens
            Map<String, Object> objectTokens = new HashMap<>();
            Map<String, String> tokens = new HashMap<>();
            tokens.put(ParamBusinessName.ATTR_TOKEN_BUSINESS,requestService.getTokenAutenticatedBusiness().getTokenBusiness());
            objectTokens.put(ParamBusinessName.TYPE_OBJECT_TOKENS,tokens);
            values.put(ParamBusinessName.TOKENS_OBJECT, objectTokens);

            //Set all Objects
            params.setValues(values);
            instance.setParametros(params);

            //Set Container
            instance.setContainerId(ContainersCurrentVersion.CONTAINER);
            //Set Process Id
            instance.setProcessesId(ContainersCurrentVersion.VERIFICACION_PAGOS_PROCESS_ID);
            //Set Owner User
            instance.setOwnerUser(requestService.getOwnerUser());

            response = jbpmApiService.iniciarInstanciaProceso(instance);
            if (!response.getResponse().getStatusCode()
                    .equals(String.valueOf(HttpStatus.OK.value()))) {
                throw new WebClientException(BusinessException.codeBadResponseJbpm,
                        BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
            }
        } catch (Exception e) {
            LogUtils.error(e);
        }
    }

}
