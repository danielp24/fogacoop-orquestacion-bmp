package com.soaint.orquestacionBpm.web.api.rest.pagos.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.impl.BusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarProcesoVerificarPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestConsultarPagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseConsultarCompararValorDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseConsultarLiquidacionTotalDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetDetalleLiquidacion;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetDetallePagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetallePagosDTO;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.pagos.EndpointPagos;
import com.soaint.orquestacionBpm.commons.constants.api.pagos.PagoConstants;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.service.jbpm.IJbpmApiService;
import com.soaint.orquestacionBpm.web.api.rest.jbpm.impl.JbpmApiHelperAsync;
import com.soaint.orquestacionBpm.web.api.rest.pagos.IPagosApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@RestController
@RequestMapping(value = EndpointBase.BASE + EndpointPagos.BASE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@Api(description = "Servicio que contiene las operaciones atadadas a las operaciones en relación a los pagos que se realizan a Fogacoop.")
public class PagosApiController implements IPagosApiController {

    @Autowired
    private final BusinessApiClient businessApiCliente;

    @Autowired
    private final IJbpmApiService jbpmApiService;

    @Autowired
    private final JbpmApiHelperAsync jbpmApiHelperAsync;

    public PagosApiController(BusinessApiClient businessApiCliente, IJbpmApiService jbpmApiService, JbpmApiHelperAsync jbpmApiHelperAsync) {
        this.businessApiCliente = businessApiCliente;
        this.jbpmApiService = jbpmApiService;
        this.jbpmApiHelperAsync = jbpmApiHelperAsync;
    }

    @Override
    @ApiOperation("Orquestado que permite dependiendo del estado comparar el valor de los pagos exitosos y los pagos en diferentes estados respecto a la liquidacion atada a la fecha de corte actual.")
    @PostMapping(value = EndpointPagos.COMPARAR_VALOR_PAGAR)
    public ResponseEntity<?> consultarCompararValor(@RequestBody RequestConsultarPagosDTO rq, HttpServletRequest request) throws WebClientException {
        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            ValidationUtils.validateNullEmptyObject(rq);
            RequestNitFechaCorteDTO rqDetLiq = new RequestNitFechaCorteDTO();
            rqDetLiq.setNit(rq.getNit());
            rqDetLiq.setFechaCorte(rq.getFechaCorte());
            ResponseGetDetalleLiquidacion resLiq = this.businessApiCliente.getDetalleLiquidacion(rqDetLiq, rq.getTokenAutenticatedBusiness());
            if (resLiq.getStatus().equals(HttpStatus.OK)) {
                ResponseGetDetallePagosDTO responsePagos = this.businessApiCliente.getDetallePagos(rq, rq.getTokenAutenticatedBusiness());
                if (responsePagos.getStatus().equals(HttpStatus.OK)) {
                    ResponseConsultarCompararValorDTO res = new ResponseConsultarCompararValorDTO();
                    PagosApiHelper.filtrarPagosHabilitadosComparacion(responsePagos);
                    Boolean pagosCorteActual = (responsePagos.getBody().size() > 0) ? Boolean.TRUE : Boolean.FALSE;
                    res.setResult(PagosApiHelper.resolverSaldos(resLiq.getBody(), pagosCorteActual));
                    response.withResponse(res)
                            .withStatus(HttpStatus.OK)
                            .withMessage("Comparación de valores (pagos Vs liquidacion exitosa)");
                } else {
                    throw new WebClientException(BusinessException.codeCatchAllException,
                            BusinessException.valueBadOperationGeneric + resLiq.getMessage());
                }
            } else {
                throw new WebClientException(BusinessException.codeLiquidacionVacia,
                        BusinessException.valueLiquidacionsVacia);
            }

        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @Override
    @ApiOperation("Operacion que verifica los pagos de la cooperativa")
    @PostMapping(value = EndpointPagos.INICIAR_VERIFICACION_PAGO)
    public ResponseEntity<?> iniciarVerificacionPago(@RequestBody RequestIniciarProcesoVerificarPagos rq, HttpServletRequest request) throws WebClientException {

        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {

            ValidationUtils.validateNullEmptyObject(rq);
            /*//Se elimina el _
            String user= rq.getOwnerUser().getUser();
            String userTrans= user.replaceAll("_","");;

            rq.getOwnerUser().setUser(userTrans);

            System.out.println(userTrans);*/

            //Verificando liquidacion de la cooperativa
            RequestNitFechaCorteDTO rqLiqTotal = new RequestNitFechaCorteDTO();
            rqLiqTotal.setNit(rq.getBasic().getNit());
            rqLiqTotal.setFechaCorte(rq.getBasic().getFechaCorte());
            ResponseConsultarLiquidacionTotalDTO resLiq = this.businessApiCliente.getLiquidacionTotal(rqLiqTotal, rq.getTokenAutenticatedBusiness());

            //Pagos con estatus ok
            RequestConsultarPagosDTO rqPagos = new RequestConsultarPagosDTO();
            rqPagos.setNit(rq.getBasic().getNit());
            rqPagos.setFechaCorte(rq.getBasic().getFechaCorte());
            ResponseGetDetallePagosDTO responsePagos = this.businessApiCliente.getDetallePagos(rqPagos, rq.getTokenAutenticatedBusiness());

            PagosApiHelper.filtrarByOrigen(responsePagos, rq.getOrigenPago());

            //Verificar si esta liquidada la cooperativa
            Boolean entidadLiquidada = !resLiq.getBody().getVrPagarPsd().toString().equalsIgnoreCase(PagoConstants.VALUE_NOT_LIQUID);

            if (responsePagos.getBody().size() > 0) {
                for (BodyGetDetallePagosDTO pago : responsePagos.getBody()) {
                    if (pago.getIdPago() != null
                            && !"".equals(pago.getIdPago().toString())) {
                        this.jbpmApiHelperAsync.iniciarProcesoVerificarPago(jbpmApiService,
                                rq,
                                pago.getIdPago().toString(),
                                pago.getCodigoEstadoPago().toString(),
                                Boolean.toString(entidadLiquidada),
                                pago.getIdTicket());
                    } else {
                        throw new WebClientException(BusinessException.codeCatchAllException,
                                BusinessException.valueBadOperationGeneric + "El pago no percibe el numero de la transaccion");
                    }
                }
                response.withStatus(HttpStatus.OK)
                        .withMessage("Validacion de pagos exitosa");

            } else {
                response.withStatus(HttpStatus.OK)
                        .withBusinessStatus(BusinessException.codeSizeIsEmpty)
                        .withMessage(BusinessException.valueSizeIsEmpty);
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}
