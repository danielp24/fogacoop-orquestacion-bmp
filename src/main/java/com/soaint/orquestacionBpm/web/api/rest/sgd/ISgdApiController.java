package com.soaint.orquestacionBpm.web.api.rest.sgd;

import com.soaint.orquestacionBpm.commons.domains.request.sgd.repository.RequestGetDocumentRepositoryDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface ISgdApiController {

    ResponseEntity descargarDocRepository(RequestGetDocumentRepositoryDTO dto, HttpServletRequest request) throws WebClientException;


}
