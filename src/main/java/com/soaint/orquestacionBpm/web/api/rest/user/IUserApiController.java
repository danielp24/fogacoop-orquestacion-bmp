package com.soaint.orquestacionBpm.web.api.rest.user;

import com.soaint.orquestacionBpm.commons.domains.request.user.RequestUserDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IUserApiController {

    ResponseEntity recoveryPassword(RequestUserDTO dto, HttpServletRequest request) throws WebClientException;


}
