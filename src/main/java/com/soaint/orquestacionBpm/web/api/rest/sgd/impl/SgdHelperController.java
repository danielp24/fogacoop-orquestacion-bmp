package com.soaint.orquestacionBpm.web.api.rest.sgd.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.ISgdApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.RequestDescargarDocFilesystemDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.RequestDescargarDocSgdDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.descargarDocSgd.ResponseDescargaDocSgdDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.ResponseDescargarDocFilesystemDTO;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.repository.RequestGetDocumentRepositoryDTO;
import com.soaint.orquestacionBpm.commons.domains.response.repository.ResponseGetDocumentRepositoryDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class SgdHelperController {


    public static Boolean descargarDocFilesystem(ISgdApiClient iSgdApiClient, RequestGetDocumentRepositoryDTO rq, ResponseGetDocumentRepositoryDTO respons) throws WebClientException {

        RequestDescargarDocFilesystemDTO docFile = new RequestDescargarDocFilesystemDTO();
        docFile.setRutaArchivo(rq.getRutaDocumento());
        ResponseDescargarDocFilesystemDTO rsFilesystem = iSgdApiClient.descargarDocFilesystem(docFile, "");
        if (rsFilesystem.getStatus().equals(HttpStatus.OK)) {
            if (!"".equals(rsFilesystem.getBody().getBinBase64())) {
                respons.setDocBase64(rsFilesystem.getBody().getBinBase64());
                respons.setExtension(rq.getRutaDocumento().replace(".", ",").split(",")[(rq.getRutaDocumento().replace(".", ",").split(",").length - 1)]);
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static Boolean descargarDocSgd(ISgdApiClient iSgdApiClient, String SGD_USER, String SGD_PASSWORD, RequestGetDocumentRepositoryDTO rq, ResponseGetDocumentRepositoryDTO respons) throws WebClientException {
        RequestDescargarDocSgdDTO docSgd = new RequestDescargarDocSgdDTO();
        docSgd.setUsername(SGD_USER);
        docSgd.setPassword(Integer.parseInt(SGD_PASSWORD));
        docSgd.setIdSoporte(rq.getIdDocSgdea().toString());
        ResponseDescargaDocSgdDTO rsSgd = iSgdApiClient.descargarDocSgd(docSgd, "");
        if (rsSgd.getStatus().equals(HttpStatus.OK)) {
            if (!rsSgd.getBody().getObjRta().getRta().isEmpty()) {
                respons.setDocBase64(rsSgd.getBody().getObjRta().getRta().get(0).getFileBase64());
                respons.setExtension(rsSgd.getBody().getObjRta().getRta().get(0).getNombre().replace(".", ",").split(",")[(rsSgd.getBody().getObjRta().getRta().get(0).getNombre().replace(".", ",").split(",").length - 1)]);
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
