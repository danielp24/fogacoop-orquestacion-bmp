package com.soaint.orquestacionBpm.web.api.rest.jbpm.impl;

import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarLotesProcesoRecaudo;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestUpdateInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetEntidadesDetallesPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetEntidadesDetallesPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.jbpm.recaudo.ResponseIniciarLotesProcesoRecaudo;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.constants.api.jbpm.ParamBusinessName;
import com.soaint.orquestacionBpm.commons.constants.api.securityjbpm.ContainersCurrentVersion;
import com.soaint.orquestacionBpm.commons.constants.api.securityjbpm.StatesSecurityJbpm;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ParametrosDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessRequestDto;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.ProcessResponseDto;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;
import com.soaint.orquestacionBpm.service.jbpm.IJbpmApiService;
import com.soaint.orquestacionBpm.service.securityjbpm.ISecurityJbpmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;


import java.math.BigDecimal;
import java.util.*;

@Component
public class JbpmApiHelper {

    public static long TIME_SLEEP_BATCH_INSTANCES;

    public JbpmApiHelper() {
    }

    public static void resolveInstance(RequestIniciarLotesProcesoRecaudo requestInicio,
                                       ResponseSearchInstancesLastFechaCorteDTO instanceDB,
                                       IJbpmApiService jbpmApiService,
                                       ISecurityJbpmService securityJbpmService,
                                       ProcessRequestDto instance) throws WebClientException {
        System.out.println("Entra a  RESOLVEINSTANCE--->" );
        ProcessResponseDto response = null;
        Map<String,Object> instanceId;
        Integer processInstanceStatus;

        Gson gson = new Gson();
        if (instanceDB.getTipoInstancia().equalsIgnoreCase(StatesSecurityJbpm.OLD)) {
            if (instanceDB.getState().equals(new BigDecimal(StatesSecurityJbpm.STARTED))) {
                System.out.println("entra a if si la instancia de bd es vieja --->" + instanceDB.getTipoInstancia() );

                System.out.println("Entra a buscar instancia por id --->" );
                instanceId = jbpmApiService.getInstanceByProcessId(instance);
                System.out.println("sale de buscar instancia por id --->"+ gson.toJson(instanceId));


                String map =  String.valueOf(instanceId.get("process-instance-state"));

                System.out.println("process instance convert--->" + map);

                if (map.equals("2.0")){
                    List<ResponseGenericTransactionDTO> upd = JbpmApiHelper.updateInstance(instanceDB, securityJbpmService);
                }else {

                    //- la instancia actual se debe eliminar
                    System.out.println("Entra a eliminar o terminar instancia actual --->");
                    response = jbpmApiService.terminarInstancia(instance);
                    System.out.println("sale de eliminar o terminar instancia actual --->");

                    if (response.getResponse().getStatusCode()
                            .equals(String.valueOf(HttpStatus.CREATED.value()))) {

                        //- se debe actualizar en el security jbpm el estado a finalizado
                        System.out.println("entra a actualizar en el security jbpm el estado a finalizado --->");
                        List<ResponseGenericTransactionDTO> upd = JbpmApiHelper.updateInstance(instanceDB, securityJbpmService);
                        System.out.println("sale actualizar en el security jbpm el estado a finalizado --->");

                        if (upd.size() > 0) {
                            if (upd.get(0).getCode().toString()
                                    .equals(String.valueOf(HttpStatus.OK.value()))) {
                                System.out.println("entra a if, si actualizó correctamente --->");
                                try {
                                    //- se debe crear una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio por mora.
                                    System.out.println("entra a crear una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio por mora. --->");
                                    response = jbpmApiService.iniciarInstanciaProceso(instance);
                                    Thread.sleep(TIME_SLEEP_BATCH_INSTANCES);
                                    System.out.println("salecrear una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio por mora.---->");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                //- se debe crear el registro de las instancia en BD
                                if (response.getResponse().getStatusCode()
                                        .equals(String.valueOf(HttpStatus.OK.value()))) {
                                    System.out.println("entra a crear el registro de las instancia en BD --->");
                                    JbpmApiHelper.insertInstance(response, instanceDB, requestInicio, securityJbpmService);
                                    System.out.println("sale crear el registro de las instancia en BD --->");


                                } else {
                                    throw new WebClientException(BusinessException.codeBadResponseJbpm,
                                            BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
                                }
                            } else {
                                throw new WebClientException(BusinessException.codeBadResponseJbpm,
                                        upd.get(0).getMessage());
                            }
                        }
                    } else {
                        throw new WebClientException(BusinessException.codeBadResponseJbpm,
                                BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
                    }
                }
            } else if (instanceDB.getState().equals(new BigDecimal(StatesSecurityJbpm.FINISH))) {
                System.out.println("entra a if si el estado es 2 (finish)----" );
                // 2) Si estado = 2 (FINISH)
                //- se crea una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio via normal.
                try {
                    System.out.println("entra a crea una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio via normal.-----" );
                    response = jbpmApiService.iniciarInstanciaProceso(instance);
                    Thread.sleep(TIME_SLEEP_BATCH_INSTANCES);
                    System.out.println("sale de crea una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio via normal.-----" );

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //- se crea un nuevo registro de historico en securityjbpm
                if (response.getResponse().getStatusCode()
                        .equals(String.valueOf(HttpStatus.OK.value()))) {
                    //- se debe crear el registro de las instancia en BD
                    System.out.println("entra a rea un nuevo registro de historico en securityjbpm si fue exitoso.-----" );
                    JbpmApiHelper.insertInstance(response, instanceDB, requestInicio, securityJbpmService);
                    System.out.println("sale de  crea un nuevo registro de historico en securityjbpm-----" );

                } else {
                    throw new WebClientException(BusinessException.codeBadResponseJbpm,
                            BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
                }
            }
        } else {
            if (instanceDB.getState().equals(new BigDecimal(StatesSecurityJbpm.STARTED))) {
                System.out.println("entra if si el instanceBD es nuevo----->" + instanceDB.getState());


                //- la instancia actual se debe eliminar
                System.out.println("Entra a eliminar o terminar instancia actual --->" );
                response = jbpmApiService.terminarInstancia(instance);
                System.out.println("sale de eliminar o terminar instancia actual --->" );

                if (response.getResponse().getStatusCode()
                        .equals(String.valueOf(HttpStatus.CREATED.value()))) {
                    System.out.println("entra a actualizar en el security jbpm el estado a finalizado --->" );

                    //- se debe actualizar en el security jbpm el estado a finalizado

                    List<ResponseGenericTransactionDTO> upd = JbpmApiHelper.updateInstance(instanceDB, securityJbpmService);
                    System.out.println("sale de  actualizar en el security jbpm el estado a finalizado --->" );

                    if (upd.size() > 0) {
                        if (upd.get(0).getCode().toString()
                                .equals(String.valueOf(HttpStatus.OK.value()))) {
                            //- se debe crear una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio por mora.
                            System.out.println("entra a if, si actualizó correctamente --->" );

                            try {
                                System.out.println("entra a crear una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio por mora. --->" );
                                response = jbpmApiService.iniciarInstanciaProceso(instance);
                                Thread.sleep(TIME_SLEEP_BATCH_INSTANCES);
                                System.out.println("sale de crear una nueva instancia en el jbpm con el parametro decisionInicialRecaudo para ir a inicio por mora. --->" );

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            //- se debe crear el registro de las instancia en BD
                            if (response.getResponse().getStatusCode()
                                    .equals(String.valueOf(HttpStatus.OK.value()))) {
                                System.out.println("entra a crear el registro de las instancia en BD --->" );
                                JbpmApiHelper.insertInstance(response, instanceDB, requestInicio, securityJbpmService);
                                System.out.println("sale de crear el registro de las instancia en BD --->" );


                            } else {
                                throw new WebClientException(BusinessException.codeBadResponseJbpm,
                                        BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
                            }
                        } else {
                            throw new WebClientException(BusinessException.codeBadResponseJbpm,
                                    upd.get(0).getMessage());
                        }
                    } else {
                        throw new WebClientException(BusinessException.codeBadResponseJbpm,
                                BusinessException.valueBadResponseJbpm);
                    }

                } else {
                    throw new WebClientException(BusinessException.codeBadResponseJbpm,
                            BusinessException.valueBadResponseJbpm + response.getResponse().getMsg());
                }

            }
        }
        System.out.println("sale de resolver instancia--->" );

    }

    public static ResponseIniciarLotesProcesoRecaudo inicializarResponse() {
        System.out.println("Entra a  InicializarResponse ---");
        ResponseIniciarLotesProcesoRecaudo response = new ResponseIniciarLotesProcesoRecaudo();
        response.setEntidadesPreviamenteEjecutados(new HashSet<>());
        response.setEntidadesProcesosEjecutados(new HashSet<>());
        response.setEntidadesSinEjecutar(new HashSet<>());
        response.setEntidadesAplicacionAbono(new HashSet<>());
        System.out.println("Response InicializarResponse --->" + response);
        System.out.println("Sale de  InicializarResponse --->" );
        return response;
    }

    public static void fillInstance(ProcessRequestDto instance,
                                    ResponseSearchInstancesLastFechaCorteDTO instanceDB,
                                    RequestIniciarLotesProcesoRecaudo requestInicio,
                                    TokenDTO token) {
        System.out.println("entra a fillInstance ");
        Gson gson = new Gson();
        ParametrosDto params = new ParametrosDto();
        Map<String, Object> values = new HashMap<>();
        Map<String, Object> objectRecaudo = new HashMap<>();
        Map<String, String> recaudo = new HashMap<>();
        recaudo.put(ParamBusinessName.ATTR_FECHACORTE, requestInicio.getFechaCorte());
        recaudo.put(ParamBusinessName.ATTR_NIT, instanceDB.getNit());
        recaudo.put(ParamBusinessName.ATTR_DECISIONFINALRECAUDO, (
                instanceDB.getState().equals(new BigDecimal(StatesSecurityJbpm.STARTED)) ? ParamBusinessName.ATTR_DECISION_MORA : ParamBusinessName.ATTR_DECISION_NORMAL));
        objectRecaudo.put(ParamBusinessName.TYPE_OBJECT_RECAUDO, recaudo);
        values.put(ParamBusinessName.RECAUDO_OBJECT, objectRecaudo);


        //Set Tokens
        Map<String, Object> objectTokens = new HashMap<>();
        Map<String, String> tokens = new HashMap<>();
        tokens.put(ParamBusinessName.ATTR_TOKEN_BUSINESS, token.getTokenBusiness());
        objectTokens.put(ParamBusinessName.TYPE_OBJECT_TOKENS, tokens);
        values.put(ParamBusinessName.TOKENS_OBJECT, objectTokens);

        params.setValues(values);
        instance.setParametros(params);

        //Set Container
        instance.setContainerId(ContainersCurrentVersion.CONTAINER);
        //Set Process Id
        instance.setProcessesId(ContainersCurrentVersion.GESTION_RECAUDO_PROCESS_ID);
        //Set Owner User
        instance.setOwnerUser(requestInicio.getOwnerUser());
        //Set Id Instance
        instance.setProcessInstance(instanceDB.getIdInstance());
        System.out.println("Genera las instancias de bpm --->" + gson.toJson(instance));
        System.out.println("sale de fillInstance ---");

    }


    public static ResponseSearchInstancesLastFechaCorteDTO fillInstance(ProcessRequestDto instance,
                                                                        BodyGetEntidadesDetallesPagos instanceDB,
                                                                        RequestIniciarLotesProcesoRecaudo requestInicio,
                                                                        TokenDTO token) {
        ResponseSearchInstancesLastFechaCorteDTO instanceConvert = new ResponseSearchInstancesLastFechaCorteDTO();
        instanceConvert.setNit(instanceDB.getNit().toString());
        instanceConvert.setIdInstance("");
        instanceConvert.setConsecutivoProceso(instanceDB.getConsecutivoProceso());
        instanceConvert.setState(new BigDecimal(StatesSecurityJbpm.FINISH));
        instanceConvert.setTipoInstancia(StatesSecurityJbpm.OLD);
        fillInstance(instance, instanceConvert, requestInicio, token);
        return instanceConvert;
    }


    public static List<ResponseGenericTransactionDTO> updateInstance(ResponseSearchInstancesLastFechaCorteDTO instanceDB,
                                                                     ISecurityJbpmService securityJbpmService) throws WebClientException {
        RequestUpdateInstance update = new RequestUpdateInstance();
        update.setIdInstance(instanceDB.getIdInstance());
        update.setStateInstance(StatesSecurityJbpm.FINISH);
        return securityJbpmService.updateInstance(update);
    }


    public static void insertInstance(ProcessResponseDto response, ResponseSearchInstancesLastFechaCorteDTO instanceDB,
                                      RequestIniciarLotesProcesoRecaudo requestInicio,
                                      ISecurityJbpmService securityJbpmService) throws WebClientException {
        RequestInsertInstance insertInstanceDB = new RequestInsertInstance();
        insertInstanceDB.setIdInstance(response.getContainers().get(0)
                .getProcesses().get(0)
                .getInstance().getInstanceId());
        insertInstanceDB.setSequenceInstance(Integer.parseInt(instanceDB.getConsecutivoProceso().toString()));
        insertInstanceDB.setNit(instanceDB.getNit());
        insertInstanceDB.setFechaCorte(requestInicio.getFechaCorte());
        insertInstanceDB.setStateInstance(StatesSecurityJbpm.STARTED);
        ValidationUtils.validateNullEmptyObject(insertInstanceDB);
        securityJbpmService.insertInstance(insertInstanceDB);
    }

    public static void resolveInstanceLoteRecaudo(
            TokenDTO token,
            RequestIniciarLotesProcesoRecaudo requestInicio,
            ResponseGetEntidadesDetallesPagos body,
            List<ResponseSearchInstancesLastFechaCorteDTO> listaLastInstances,
            ResponseIniciarLotesProcesoRecaudo response,
            IJbpmApiService jbpmApiService,
            ISecurityJbpmService securityJbpmService) throws WebClientException {
        System.out.println("entra a resolveInstanceLoteRecaudo con nit  ---");

        for (BodyGetEntidadesDetallesPagos itemCore : body.getBody()) {
            System.out.println("entra a for donde recorre las entidades detalle pagos ---");
            Boolean setEntity = Boolean.TRUE;
            Boolean exitsEntity = Boolean.FALSE;
            Integer maxConsecutivoProcesoCore = Integer.parseInt(itemCore.getConsecutivoProceso().toString());
            System.out.println("maxConsecutivoProcesoCore ---" + maxConsecutivoProcesoCore);
            for (ResponseSearchInstancesLastFechaCorteDTO itemSecurity : listaLastInstances) {
                System.out.println("entra a for donde recorre las instancias en el security bpm por nit y la ultima fecha de corte ");
                System.out.println(" NIT--->" + itemSecurity.getNit());
                System.out.println(" FECHA CORTE--->" + itemSecurity.getFechaCorte());
                System.out.println(" ID INSTANCIA--->" + itemSecurity.getIdInstance());
                if (itemCore.getNit().toString()
                        .equals(itemSecurity.getNit())) {
                    exitsEntity = Boolean.TRUE;

                    Integer maxConsecutivoProcesoSecurity = Integer.parseInt(itemSecurity.getConsecutivoProceso().toString());
                    System.out.println("maxConsecutivoProcesoSecurity ---" + maxConsecutivoProcesoSecurity);
                    if ((itemSecurity.getTipoInstancia().equalsIgnoreCase(StatesSecurityJbpm.NEW) && maxConsecutivoProcesoCore > maxConsecutivoProcesoSecurity) //Determinando si se genera un nuevo consecutivo del proceso (Una nueva liquidacion o reliquidacion)
                            || (itemSecurity.getTipoInstancia().equalsIgnoreCase(StatesSecurityJbpm.OLD) && itemSecurity.getState().equals(new BigDecimal(StatesSecurityJbpm.STARTED)))) { //Determinando si hay una instancia viva del periodo anterior indicando que la entidad se encuentra en mora
                        System.out.println("entra a if  donde Determinando si se genera un nuevo consecutivo del proceso (Una nueva liquidacion o reliquidacion)");
                        System.out.println("y donde Determinando si hay una instancia viva del periodo anterior indicando que la entidad se encuentra en mora");
                        ProcessRequestDto instance = new ProcessRequestDto();
                        JbpmApiHelper.fillInstance(instance, itemSecurity, requestInicio, token);
                        //Sabiendo que esto solo apsa con retransmision se añade el consecutivo del proceso
                        itemSecurity.setConsecutivoProceso(new BigDecimal(maxConsecutivoProcesoCore));
                        setEntity = Boolean.FALSE;
                        JbpmApiHelper.resolveInstance(requestInicio,
                                itemSecurity,
                                jbpmApiService,
                                securityJbpmService,
                                instance);
                        response.getEntidadesProcesosEjecutados().add(itemCore);
                        break;
                    }
                }
            }
            System.out.println("termina for donde recorre las instancias en el security bpm por nit y la ultima fecha de corte ---");


            if (!exitsEntity) {
                if (!"0".equals(itemCore.getSaldoLiquidacion().toString())
                        || !"0".equals(itemCore.getTotalAPagar().toString())) {
                    System.out.println("entra a if si no existe entidad ---");
                    ProcessRequestDto instance = new ProcessRequestDto();
                    System.out.println("entra a fillInstance---");
                    ResponseSearchInstancesLastFechaCorteDTO itemSecurity = JbpmApiHelper.fillInstance(instance, itemCore, requestInicio, token);
                    System.out.println("sale de fillInstance---");
                    System.out.println("entra a resolveInstance---");
                    JbpmApiHelper.resolveInstance(requestInicio,
                            itemSecurity,
                            jbpmApiService,
                            securityJbpmService,
                            instance);
                    System.out.println("sale de resolveInstance---");

                    response.getEntidadesProcesosEjecutados().add(itemCore);
                } else {
                    response.getEntidadesSinEjecutar().add(itemCore);
                }
            } else {
                //Agregando la cooperativa que ya se encuentra ejecutada
                if (setEntity) {
                    response.getEntidadesPreviamenteEjecutados().add(itemCore);
                }
            }
        }
        System.out.println("sale de  resolveInstanceLoteRecaudo---");

    }


    public static void verifyInstances(List<ResponseSearchInstancesLastFechaCorteDTO> listaLastInstances, List<ResponseSearchInstancesLastFechaCorteDTO> listaInstanciasExistentes) {
        for (ResponseSearchInstancesLastFechaCorteDTO instance : listaLastInstances) {
            if (instance.getTipoInstancia().equalsIgnoreCase(StatesSecurityJbpm.NEW)) {
                listaInstanciasExistentes.add(instance);
            }
        }
    }


    public static Boolean checkLiquidacion(BigDecimal valorTotalLiquidacion) {
        if (valorTotalLiquidacion.compareTo(new BigDecimal(0)) == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;

    }

    @Autowired
    private void setInitialVars(@Value("${processBatch.sleep.time}") String sleepTime) {
        TIME_SLEEP_BATCH_INSTANCES = Long.parseLong(sleepTime);
    }
}
