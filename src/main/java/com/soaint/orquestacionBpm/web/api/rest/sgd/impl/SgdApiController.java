package com.soaint.orquestacionBpm.web.api.rest.sgd.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.ISgdApiClient;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.sgd.EndpointSgd;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.repository.RequestGetDocumentRepositoryDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.domains.response.repository.ResponseGetDocumentRepositoryDTO;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.web.api.rest.sgd.ISgdApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping(value = EndpointBase.BASE + EndpointSgd.SGD)
@Api(description = "Servicio que contiene las operaciones atadadas al repositorio de gestión documental.")
public class SgdApiController implements ISgdApiController {

    @Autowired
    private final ISgdApiClient iSgdApiClient;
    @Value("${sgd.authorized.user}")
    public String SGD_USER;
    @Value("${sgd.authorized.password}")
    public String SGD_PASSWORD;

    public SgdApiController(ISgdApiClient iSgdApiClient) {
        this.iSgdApiClient = iSgdApiClient;
    }

    @Override
    @PostMapping(EndpointSgd.GET_DOCUMENTO_REPOSITORY)
    @ApiOperation("Controlador que descarga el documento dependiendo del repositorio donde se encuentra almacenado")
    public ResponseEntity descargarDocRepository(@RequestBody RequestGetDocumentRepositoryDTO rq, HttpServletRequest request) throws WebClientException {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyObject(rq);
            ResponseGetDocumentRepositoryDTO respons = new ResponseGetDocumentRepositoryDTO();
            Boolean getDocument = Boolean.FALSE;
            if (rq.getIdDocSgdea() != null) {
                try {
                    getDocument = SgdHelperController.descargarDocSgd(this.iSgdApiClient, SGD_USER, SGD_PASSWORD, rq, respons);
                } catch (WebClientException e) {
                    if (!"".equals(rq.getRutaDocumento())) {
                        getDocument = SgdHelperController.descargarDocFilesystem(this.iSgdApiClient, rq, respons);
                    }
                }
            }
            if (!getDocument && !"".equals(rq.getRutaDocumento())) {
                getDocument = SgdHelperController.descargarDocFilesystem(this.iSgdApiClient, rq, respons);
            }
            if (getDocument && !"".equals(respons.getDocBase64())) {
                response.withResponse(respons)
                        .withStatus(HttpStatus.OK)
                        .withBusinessStatus(String.valueOf(HttpStatus.OK.value()))
                        .withMessage("Documento descargado exitosamente.");
            } else {
                throw new WebClientException(BusinessException.codeCatchAllException,
                        BusinessException.valueBadOperationGeneric + ", No se retorno el documento, no se encontro en los repositorios parametrizados");
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}
