package com.soaint.orquestacionBpm.web.api.rest.comunicaciones.impl;

import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.ISgdApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.business.RequestBusinessDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.comunicaciones.RequestGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.RequestSaveCorreoOpDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.lstAdjuntosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.*;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetEntidadesInscritasDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetPlantillasDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetTrazabilidadNotificacionDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.ResponseGetFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones.ResponseGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.opeCorreoGuarda.ResponseOpeCorreoDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.business.BusinessUtils;
import com.soaint.orquestacionBpm.commons.Utils.date.DateUtil;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.enviarCorreoPlantilla.EndpointComunicaciones;
import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.UserDto;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.CorreoPlantillaDTO;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.NotificacionEntidadInscritaJsonDTO;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.NumeroIdentificacionCoopDTO;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.RequestGenericJsonDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.web.api.rest.comunicaciones.IComunicacionesApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping(value = EndpointBase.BASE)
@Api(description = "Servicio que contiene las operaciones atadas a la comunicación por medio de envio de correos electrónicos.")
public class ComunicacionesApiController implements IComunicacionesApiController {

    @Value("${header.JWT}")
    public final String AuthorizationJwt = "Authorization";

    @Value("${sgd.authorized.user}")
    public String usuario;

    @Value("${sgd.authorized.password}")
    public String password;

    @Value("${business.exception.apiOrquestacion.CodeNoHayCooperativasParaEnviar}")
    public String CodBusinessNotificacion;

    @Value("${business.exception.apiOrquestacion.valueNoHayCooperativasParaEnviar}")
    public String valueNotificacionException;

    @Value("${business.exception.apiOrquestacion.CorreosEnviadosCoop}")
    public String CorreosEnviados;

    @Autowired
    private final IBusinessApiClient businessApiCliente;

    @Autowired
    private final ISgdApiClient iSgdApiClient;

    // Variables de envío notificación opeCorreoGuarda
    @Value("${comunicaciones.helper.object.structure.correoEnvio}")
    public String correoEnvio;

    @Value("${comunicaciones.helper.object.structure.setIdTipNotifica}")
    public String setIdTipNotifica;

    @Value("${comunicaciones.helper.object.structure.userService}")
    public String setUsuario;

    @Value("${notificacion.id.plantilla}")
    public String plantillaNotificacion;

    @Value("${notificacion.id.dominio}")
    public String dominioNotificacion;

    public ComunicacionesApiController(IBusinessApiClient businessApiCliente, ISgdApiClient iSgdApiClient) {
        this.businessApiCliente = businessApiCliente;
        this.iSgdApiClient = iSgdApiClient;
    }

    @PostMapping(EndpointComunicaciones.ENVIAR_CORREO_PLANTILLA)
    @ApiOperation("Controlador que permite generar la lógica para obtener la fecha de vencimiento y valor a pagar y enviarla a traves de un correo electrónico")
    public ResponseEntity enviarCorreoPlantilla(@RequestBody CorreoPlantillaDTO plantillaDTO, HttpServletRequest request) throws WebClientException {

        ResponseBuilder responseBuilder = ResponseBuilder.newBuilder();

        try {
            ResponseGetDestinatariosDTO destinatariosDTO;
            Map<String, String> template = new HashMap<>();
            String cadena;
            List<String> destinatariosPrincipales = new ArrayList<>();
            List<String> destinatariosSecundarios = new ArrayList<>();
            List<String> pTemplate = new ArrayList<>();
            String asunto = "";
            String cuerpo = "";
            String businessGeneric = "";
            ResponseOpeCorreoDTO correoDTO = new ResponseOpeCorreoDTO();
            List<lstAdjuntosDTO> adjuntos = new ArrayList<>();


            //Consultamos la plantilla
            ResponseGetPlantillasDTO responseGetPlantillasDTO = this.businessApiCliente.getPlantilla(plantillaDTO.getIdPlantilla(), plantillaDTO.getTokenAutenticatedBusiness());
            if (ObjectUtils.isEmpty(responseGetPlantillasDTO.getBody())) {
                throw new WebClientException(BusinessException.codeServiceGetPlantillaReturnEmpty, BusinessException.valueServiceGetPlantillaReturnEmpty);
            }
            for (BodyGetPlantillasDTO bodyGetPlantillasDTO : responseGetPlantillasDTO.getBody()) {
                asunto = bodyGetPlantillasDTO.getAsunto();
                cuerpo = bodyGetPlantillasDTO.getCuerpo();
            }

            //consumimos el servicio de obtener destinatarios
            RequestGetDestinatariosDTO requestGetDestinatariosDTO;
            requestGetDestinatariosDTO = new RequestGetDestinatariosDTO();
            requestGetDestinatariosDTO.setIdPlantilla(new BigDecimal(plantillaDTO.getIdPlantilla()));
            requestGetDestinatariosDTO.setNit(new BigDecimal(plantillaDTO.getNit()));
            destinatariosDTO = this.businessApiCliente.getDestinatarios(requestGetDestinatariosDTO, plantillaDTO.getTokenAutenticatedBusiness());

            //consultamos la lista que traerá los los parametros
            ResponseFindIdDominioDTO dominioDTO = this.businessApiCliente.findIdDominio(plantillaDTO.getIdDominio(), plantillaDTO.getTokenAutenticatedBusiness());

            //obtenemos la lista de template (fechaVenc y montoLiq)

            pTemplate = ComunicacionesHelperController.getTemplate(pTemplate, dominioDTO);
            // }
            //Decidimos el tipo de parametro y así mismo ejecutamos las acciones a realizar, si es 1, consultamos SP
            if (plantillaDTO.getTypeParameter() == 1) {

                //concatenamos los valores ingresados, para pasarlos al SP
                businessGeneric = ComunicacionesHelperController.convertirBusinessListToString(plantillaDTO.getBusinessList());

                //consumimos los valores, y llamamos al SP business
                RequestBusinessDTO businessDTO = new RequestBusinessDTO();
                businessDTO.setParameterBusiness(businessGeneric);
                businessDTO.setIdDominio(plantillaDTO.getIdDominio());
                ResponseGenericBusinessDTO responseGenericBusinessDTO = this.businessApiCliente.businessParameterTemplate(businessDTO, plantillaDTO.getTokenAutenticatedBusiness());

                cadena = responseGenericBusinessDTO.getBody().getResult();
                String[] arreglo = cadena.split("\\|");

                //Reemplazamos el cuerpo del mensaje
                cuerpo = ComunicacionesHelperController.reemplazarCuerpoMensaje1(pTemplate, (HashMap<String, String>) template, arreglo, cuerpo);

                //consumimos el servicio para obtener los datos de los contactos y obtener el correo electronico
                // ResponseGetInfoBasicaDTO infoBasicaDTO = this.businessApiCliente.getInfoBasica(plantillaDTO.getNit(), plantillaDTO.getTokenAutenticatedBusiness());

                //obtenemos los correos de destinatarios principales y secundarios
                destinatariosPrincipales = ComunicacionesHelperController.getDestinatariosPrincipales(destinatariosDTO, destinatariosPrincipales);
                destinatariosSecundarios = ComunicacionesHelperController.getDestinatariosSecundarios(destinatariosDTO, destinatariosSecundarios);

                if (ObjectUtils.isEmpty(destinatariosPrincipales) && ObjectUtils.isEmpty(destinatariosSecundarios)) {
                    responseBuilder.
                            withStatus(HttpStatus.BAD_REQUEST)
                            .withMessage("No se encontraron destinatarios.")
                            .withTransactionState(TransactionState.FAIL);
                }

                //destinatariosPrincipales = ComunicacionesHelperController.getCorrerosElectronicos(infoBasicaDTO, destinatariosPrincipales);
                adjuntos = (plantillaDTO.getLstAdjuntos() != null ? plantillaDTO.getLstAdjuntos() : new ArrayList<>());


                RequestSaveCorreoOpDTO requestSaveCorreoOpDTO;
                requestSaveCorreoOpDTO = ComunicacionesHelperController.buildCorreo(destinatariosPrincipales, destinatariosSecundarios, asunto, cuerpo, adjuntos, correoEnvio, setUsuario, setIdTipNotifica, usuario, password);
                Gson gson = new Gson();
                System.out.println("REQUEST A ENVIAR A SGD--->" +gson.toJson(requestSaveCorreoOpDTO));
                System.out.println("AUTORIZACIÓN A ENVIAR A SGD--->" +AuthorizationJwt);
                correoDTO = this.iSgdApiClient.opeCorreoGuarda(requestSaveCorreoOpDTO, AuthorizationJwt);
            }
            if (plantillaDTO.getTypeParameter() == 2) {

                //Reemplazamos el cuerpo del mensaje
                cuerpo = ComunicacionesHelperController.reemplazarCuerpoMensaje2(pTemplate, (HashMap<String, String>) template, plantillaDTO.getBusinessList(), cuerpo);

                //consumimos el servicio para obtener los datos de los contactos y obtener el correo electronico
                //ResponseGetInfoBasicaDTO infoBasicaDTO = this.businessApiCliente.getInfoBasica(plantillaDTO.getNit(), plantillaDTO.getTokenAutenticatedBusiness());

                //obtenemos los correos de destinatarios principales y secundarios
                destinatariosPrincipales = ComunicacionesHelperController.getDestinatariosPrincipales(destinatariosDTO, destinatariosPrincipales);
                destinatariosSecundarios = ComunicacionesHelperController.getDestinatariosSecundarios(destinatariosDTO, destinatariosSecundarios);


                //destinatariosPrincipales = ComunicacionesHelperController.getCorrerosElectronicos(infoBasicaDTO, destinatariosPrincipales);
                adjuntos = (plantillaDTO.getLstAdjuntos() != null ? plantillaDTO.getLstAdjuntos() : new ArrayList<>());

                //Consumimos el servicio de opeCorreoGuarda para enviar el correo
                RequestSaveCorreoOpDTO requestSaveCorreoOpDTO;

                requestSaveCorreoOpDTO = ComunicacionesHelperController.buildCorreo(destinatariosPrincipales, destinatariosSecundarios, asunto, cuerpo, adjuntos, correoEnvio, setUsuario, setIdTipNotifica, usuario, password);
                correoDTO = this.iSgdApiClient.opeCorreoGuarda(requestSaveCorreoOpDTO, AuthorizationJwt);
            }

            //Excepciones y respuesta
            if (correoDTO.getBody().getObjRta().getEstado().equals("1")) {
                responseBuilder.withResponse(correoDTO)
                        .withStatus(HttpStatus.OK)
                        .withMessage("Información enviada Exitosamente.")
                        .withTransactionState(TransactionState.OK);
            } else {
                throw new WebClientException(BusinessException.codeCatchAllException,
                        BusinessException.valueBadOperationGeneric + correoDTO.getBody().getObjRta().getMensaje());
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            responseBuilder.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            LogUtils.error(e);
            responseBuilder.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return responseBuilder
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @PostMapping(EndpointComunicaciones.NOTIFICACION_FECHA_PAGO_INSCRITA)
    @ApiOperation("Envia una notificación de fecha de pago a las cooperativas que no se le haya enviado anteriormente")
    public ResponseEntity notiFechaPagoEntidadInscrita(@RequestBody UserDto userDto, HttpServletRequest request) throws WebClientException {
        ResponseBuilder responseBuilder = ResponseBuilder.newBuilder();
        try {
            ResponseGetEntidadesInscritasDTO entidadesInscritas;
            List<String> bussinesList = new ArrayList<>();
            NotificacionEntidadInscritaJsonDTO jsonString = new NotificacionEntidadInscritaJsonDTO();
            List<NumeroIdentificacionCoopDTO> listNit = new ArrayList<>();
            ResponseGenTransactionDTO transactionDTO = new ResponseGenTransactionDTO();
            ResponseGetFechaCorteMaximaNotificacionDTO fechaCorteMaxNotificacion = new ResponseGetFechaCorteMaximaNotificacionDTO();
            ResponseGetFechaCorteDTO fechaCorte;
            String fechaCorteConvert;
            ResponseTrazabilidadNotificacionDTO traza;
            ResponseEntity envioCorreo;

            TokenDTO tokenDTO = BusinessUtils.authentication(this.businessApiCliente, userDto);
            traza = businessApiCliente.getTrazabilidadNotificacion(tokenDTO);
            entidadesInscritas = businessApiCliente.getEntidadesInscritas(tokenDTO);
            fechaCorte = businessApiCliente.getFechaCorte(tokenDTO);
            fechaCorteMaxNotificacion = businessApiCliente.getFechaMaximaCorte(tokenDTO);

            fechaCorteConvert = DateUtil.getDateToStringBPM(fechaCorte.getBody().getFechaCorte());

            //Se guardan todos los NIT en trazabi.
            List<String> trazabi = new ArrayList<>();
            for (BodyGetTrazabilidadNotificacionDTO dto : traza.getBody()) {
                trazabi.add(dto.getNumeroIdentificacion());
            }
            //Se guardan las entidades inscritas en dos listas coopList (cooperativas que faltan por enviar) y coopListPura(todas las coop)
            List<String> coopList = new ArrayList<>();
            List<String> coopListPura = new ArrayList<>();
            for (BodyGetEntidadesInscritasDTO dto : entidadesInscritas.getBody()) {
                coopList.add(dto.getNumeroIdentificacion().toString());
                coopListPura.add(dto.getNumeroIdentificacion().toString());
            }
            coopList.removeIf(trazabi::contains);

            if (fechaCorteConvert.equals(fechaCorteMaxNotificacion.getBody().getFechaCorte())) {
                if (coopList.size() != 0) {
                    for (String s : coopList) {

                        CorreoPlantillaDTO correo = new CorreoPlantillaDTO();
                        NumeroIdentificacionCoopDTO nit = new NumeroIdentificacionCoopDTO();
                        RequestGenericJsonDTO genericJsonDTO = new RequestGenericJsonDTO();

                        correo = ComunicacionesHelperController.buildCorreoNotificacion(correo, plantillaNotificacion, dominioNotificacion, s, bussinesList, fechaCorteConvert, tokenDTO);
                        envioCorreo = enviarCorreoPlantilla(correo, request);
                        nit.setNumeroIdentificacion(correo.getNit());

                        listNit.add(nit);
                        nit = null;
                        correo = null;
                        bussinesList.clear();

                        jsonString.setListEntidades(listNit);
                        jsonString.setFechaCorte(fechaCorteConvert);
                        jsonString.setCodigoPlantilla(Long.valueOf(plantillaNotificacion));

                        Gson gson = new Gson();
                        genericJsonDTO.setJson(gson.toJson(jsonString));

                        if (envioCorreo.getStatusCodeValue() == 200) {
                            transactionDTO = businessApiCliente.resolverNotificacionInscritas(genericJsonDTO, tokenDTO);
                        } else {
                            throw new WebClientException(BusinessException.codeCatchAllException,
                                    BusinessException.valueBadOperationGeneric + transactionDTO.getBody());
                        }
                        genericJsonDTO = null;
                        listNit.clear();
                    }
                    if (transactionDTO.getBody().getResponse().equals("SUCCESS")) {
                        responseBuilder.withResponse(transactionDTO)
                                .withStatus(HttpStatus.OK)
                                .withMessage(CorreosEnviados)
                                .withTransactionState(TransactionState.OK);
                    } else {
                        throw new WebClientException(BusinessException.codeCatchAllException,
                                BusinessException.valueBadOperationGeneric + transactionDTO.getBody());
                    }
                } else {
                    responseBuilder
                            .withStatus(HttpStatus.OK)
                            .withBusinessStatus(CodBusinessNotificacion)
                            .withMessage(valueNotificacionException)
                            .withTransactionState(TransactionState.OK);
                }
            } else {
                for (String s : coopListPura) {

                    CorreoPlantillaDTO correo = new CorreoPlantillaDTO();
                    NumeroIdentificacionCoopDTO nit = new NumeroIdentificacionCoopDTO();
                    RequestGenericJsonDTO genericJsonDTO = new RequestGenericJsonDTO();

                    correo = ComunicacionesHelperController.buildCorreoNotificacion(correo, plantillaNotificacion, dominioNotificacion, s, bussinesList, fechaCorteConvert, tokenDTO);
                    envioCorreo = enviarCorreoPlantilla(correo, request);
                    nit.setNumeroIdentificacion(correo.getNit());

                    listNit.add(nit);
                    nit = null;
                    correo = null;
                    bussinesList.clear();

                    jsonString.setListEntidades(listNit);
                    jsonString.setFechaCorte(fechaCorteConvert);
                    jsonString.setCodigoPlantilla(Long.valueOf(plantillaNotificacion));

                    Gson gson = new Gson();
                    genericJsonDTO.setJson(gson.toJson(jsonString));

                    if (envioCorreo.getStatusCodeValue() == 200) {
                        transactionDTO = businessApiCliente.resolverNotificacionInscritas(genericJsonDTO, tokenDTO);
                    } else {
                        throw new WebClientException(BusinessException.codeCatchAllException,
                                BusinessException.valueBadOperationGeneric + transactionDTO.getBody());
                    }
                    genericJsonDTO = null;
                    listNit.clear();
                }
                if (transactionDTO.getBody().getResponse().equals("SUCCESS")) {
                    responseBuilder.withResponse(transactionDTO)
                            .withStatus(HttpStatus.OK)
                            .withMessage(CorreosEnviados)
                            .withTransactionState(TransactionState.OK);
                } else {
                    throw new WebClientException(BusinessException.codeCatchAllException,
                            BusinessException.valueBadOperationGeneric + transactionDTO.getBody());
                }
            }

        } catch (
                Exception e) {
            LogUtils.error(e);
            responseBuilder.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return responseBuilder
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}



