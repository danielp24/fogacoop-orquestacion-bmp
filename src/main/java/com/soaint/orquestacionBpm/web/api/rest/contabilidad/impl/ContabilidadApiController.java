package com.soaint.orquestacionBpm.web.api.rest.contabilidad.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.contabilidad.ContabilidadApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseResolverComprobanteContableDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.ResponseRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.contabilidad.EndpointContabilidad;
import com.soaint.orquestacionBpm.commons.domains.generic.contabilidad.ContextComprobantes;
import com.soaint.orquestacionBpm.commons.domains.request.contabilidad.RequestEnviarComprobanteContabilidadDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.web.api.rest.contabilidad.IContabilidadApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = EndpointBase.BASE + EndpointContabilidad.BASE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@Api(description = "Servicio que contiene las operaciones atadas a los comprobantes contables")
public class ContabilidadApiController implements IContabilidadApiController {

    @Autowired
    private final ContabilidadApiCliente contabilidadApiCliente;

    @Autowired
    private final IBusinessApiClient businessApiClient;

    @Autowired
    private final ContabilidadApiHelper contabilidadApiHelperClient;

    public ContabilidadApiController(ContabilidadApiCliente contabilidadApiCliente, IBusinessApiClient businessApiClient, ContabilidadApiHelper businessApiHelperClient) {
        this.contabilidadApiCliente = contabilidadApiCliente;
        this.businessApiClient = businessApiClient;
        this.contabilidadApiHelperClient = businessApiHelperClient;
    }

    @Override
    @ApiOperation("Orquestado que permite enviar el comprobate o los comprobantes dependiendo del pago que se haya realizado")
    @PostMapping(value = EndpointContabilidad.ENVIAR_COMPROBANTE)
    public ResponseEntity<?> enviarComprobante(@RequestBody RequestEnviarComprobanteContabilidadDTO rqComprobante, HttpServletRequest request) throws WebClientException {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        System.out.println("Entre a enviar Comprobante con el rq --->" + rqComprobante);
        try {
            ContextComprobantes contexto = this.contabilidadApiHelperClient.resolverConjuntoComprobantes(rqComprobante, this.businessApiClient);
            System.out.println("Salió de resolver conjunto comprobastes con lo siguiente: -->" + contexto.toString());

            if (contexto.getListRequestCont().size() > 0) {
                for (int posReg = 0; posReg < contexto.getListRequestCont().size(); posReg++) {
                    ResponseResolverComprobanteContableDTO resComp = this.contabilidadApiHelperClient.resolverComprobante(rqComprobante, posReg, this.businessApiClient, contexto, 1);
                    System.out.println("For--> Resolver comprobante --->" + resComp);
                    if (!resComp.getBusinessStatus().equals(String.valueOf(HttpStatus.CREATED.value()))) {
                        ResponseRegistrarComprobanteDTO responsePagos = this.contabilidadApiCliente.registrarComprobante(contexto.getListRequestCont().get(posReg));
                        System.out.println("si es estatus creado ---> registrar comprobante --->" + responsePagos);
                        if (responsePagos.getStatus() != HttpStatus.BAD_REQUEST) {
                            resComp = this.contabilidadApiHelperClient.resolverComprobante(rqComprobante, posReg, this.businessApiClient, contexto, 2);
                            System.out.println("Si es diferente de bad rq --->" + resComp);
                            if (!resComp.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                                throw new WebClientException(BusinessException.codeBadOperationGeneric,
                                        BusinessException.valueBadOperationGeneric + responsePagos.getMessage());
                            }
                        } else {
                            throw new WebClientException(BusinessException.codeBadOperationGeneric,
                                    BusinessException.valueBadOperationGeneric + responsePagos.getMessage());
                        }
                    }
                }
            }
            response.withStatus(HttpStatus.OK)
                    .withBusinessStatus(String.valueOf(HttpStatus.OK.value()))
                    .withMessage("Comprobantes resueltos exitosamente.")
                    .withTransactionState(TransactionState.OK);
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.OK)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.OK)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}
