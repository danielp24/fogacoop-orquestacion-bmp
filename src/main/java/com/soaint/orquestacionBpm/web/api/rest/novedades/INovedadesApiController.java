package com.soaint.orquestacionBpm.web.api.rest.novedades;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteWithSecurityDTO;
import com.soaint.orquestacionBpm.commons.domains.request.novedades.RequestResolverIngresoNovedadesDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface INovedadesApiController {

    ResponseEntity resolverIngresoNovedad(RequestResolverIngresoNovedadesDTO plantillaDTO, HttpServletRequest request) throws WebClientException;

    ResponseEntity getListaDetallesNovedades(RequestNitFechaCorteWithSecurityDTO rq, HttpServletRequest request) throws WebClientException;


}
