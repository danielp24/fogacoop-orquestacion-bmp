package com.soaint.orquestacionBpm.web.api.rest.cooperativa.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.contabilidad.ContabilidadApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad.RequestRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetInfoBasicaDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyResponseGetInfoBasica;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.ResponseRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.cooperativa.EndpointCooperativa;
import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGeNombresContactoDTO;
import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGetContactosEntidadDTO;
import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGetPosLaboralDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.domains.response.cooperativa.*;
import com.soaint.orquestacionBpm.commons.domains.response.cooperativa.body.*;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.web.api.rest.cooperativa.ICooperativaApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = EndpointBase.BASE + EndpointCooperativa.BASE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@Api(description = "Servicio que contiene las operaciones atadas los contactos de las Cooperativas.")
public class CooperativaApiController implements ICooperativaApiController {

    @Autowired
    private final ContabilidadApiCliente contabilidadApiCliente;

    @Autowired
    private final IBusinessApiClient businessApiClient;

    private final String TIPO_IDENTIFIACION_ENTIDAD = "NIT";

    public CooperativaApiController(ContabilidadApiCliente contabilidadApiCliente, IBusinessApiClient businessApiClient) {
        this.contabilidadApiCliente = contabilidadApiCliente;
        this.businessApiClient = businessApiClient;
    }

    @Override
    @PostMapping(EndpointCooperativa.GET_CONTACTOS_ENTIDAD)
    @ApiOperation("Orquestado que devuelve una lista de contactos con todos sus datos asociados")
    public ResponseEntity getContactosEntidad(@RequestBody RequestGetContactosEntidadDTO rq, HttpServletRequest request) throws WebClientException {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            List<ResponseGetContactosEntidadDTO> lstRes = new ArrayList<>();
            ResponseGetInfoBasicaDTO rsInfoBasica = this.businessApiClient.getInfoBasica(rq.getNit(), rq.getTokenAutenticatedBusiness());

            if (rsInfoBasica.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                if (rsInfoBasica.getBody().length > 0) {
                    for (BodyResponseGetInfoBasica itemContacto : rsInfoBasica.getBody()) {
                        ResponseGetContactosEntidadDTO res = new ResponseGetContactosEntidadDTO();
                        RequestGeNombresContactoDTO rqNombreCont = new RequestGeNombresContactoDTO();
                        rqNombreCont.setNumId(itemContacto.getNumeroIdentContacto());
                        rqNombreCont.setTipoId(itemContacto.getIdTipoIdentContacto());
                        ResponseGetNombresDTO rsNombreCont = this.businessApiClient.getNombresContacto(rqNombreCont, rq.getTokenAutenticatedBusiness());
                        if (rsNombreCont.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                            if (rsNombreCont.getBody().length > 0) {
                                List<NombresDTO> listNombresContacto = Arrays.asList(rsNombreCont.getBody());
                                if (listNombresContacto.get(0).getTipoIdentificacion() != null) {
                                    res.setTipoIdentificacion(listNombresContacto.get(0).getTipoIdentificacion());
                                }
                                if (listNombresContacto.get(0).getNumIdentificacion() != null) {
                                    res.setNumIdentificacion(listNombresContacto.get(0).getNumIdentificacion());
                                }
                                if (listNombresContacto.get(0).getFechaInicio() != null) {
                                    res.setFechaInicio(listNombresContacto.get(0).getFechaInicio());
                                }
                                if (listNombresContacto.get(0).getFechaFin() != null) {
                                    res.setFechaFin(listNombresContacto.get(0).getFechaFin());
                                }
                                if (listNombresContacto.get(0).getNombre() != null) {
                                    res.setNombre(listNombresContacto.get(0).getNombre());
                                }
                                if (listNombresContacto.get(0).getPrimerApellido() != null) {
                                    res.setPrimerApellido(listNombresContacto.get(0).getPrimerApellido());
                                }
                                if (listNombresContacto.get(0).getSegundoApellido() != null) {
                                    res.setSegundoApellido(listNombresContacto.get(0).getSegundoApellido());
                                }
                            }
                        } else {
                            throw new WebClientException(BusinessException.codeBadOperationGeneric,
                                    BusinessException.valueBadOperationGeneric + rsNombreCont.getMessage());
                        }
                        RequestGetPosLaboralDTO rqPosLabCont = new RequestGetPosLaboralDTO();
                        rqPosLabCont.setTipoIdentContacto(itemContacto.getIdTipoIdentContacto());
                        rqPosLabCont.setNumeroIdentContacto(new BigDecimal(itemContacto.getNumeroIdentContacto()));
                        rqPosLabCont.setTipoIdentEntidad(TIPO_IDENTIFIACION_ENTIDAD);
                        rqPosLabCont.setNumeroIdentEntidad(new BigDecimal(rq.getNit()));
                        ResponseGetPosicionLaboralContactoDTO rsPosLabCont = this.businessApiClient.getPosicionLaboralContacto(rqPosLabCont, rq.getTokenAutenticatedBusiness());
                        if (rsPosLabCont.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                            if (rsPosLabCont.getBody().length > 0) {
                                AsociacionEntidadDTO ascEnt = new AsociacionEntidadDTO();
                                List<PosicionLaboralEntidadDTO> lstPosLaboral = Arrays.asList(rsPosLabCont.getBody());
                                ascEnt.setPosicionLaboral(lstPosLaboral);
                                ascEnt.setTipoIdentificacionEntidad(TIPO_IDENTIFIACION_ENTIDAD);
                                ascEnt.setNumIdentificacionEntidad(Integer.parseInt(rq.getNit()));
                                res.setAsocEntidad(ascEnt);
                            }
                        } else {
                            throw new WebClientException(BusinessException.codeBadOperationGeneric,
                                    BusinessException.valueBadOperationGeneric + rsPosLabCont.getMessage());
                        }
                        ResponseGetCorreosDTO rsCorreoCont = this.businessApiClient.getCorreosContacto(itemContacto.getNumeroIdentContacto(), rq.getTokenAutenticatedBusiness());
                        if (rsCorreoCont.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                            if (rsCorreoCont.getBody().length > 0) {
                                List<CorreoDTO> lstCorreos = Arrays.asList(rsCorreoCont.getBody());
                                res.setCorreos(lstCorreos);
                            }
                        }
                        ResponseGetDireccionDTO rsDireccionCont = this.businessApiClient.getDireccionesContacto(itemContacto.getNumeroIdentContacto(), rq.getTokenAutenticatedBusiness());
                        if (rsDireccionCont.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                            if (rsDireccionCont.getBody().length > 0) {
                                List<DireccionDTO> lstDirecciones = Arrays.asList(rsDireccionCont.getBody());
                                res.setDirecciones(lstDirecciones);
                            }
                        }
                        ResponseGetTelefonosDTO rsTelefonosCont = this.businessApiClient.getTelefonosContacto(itemContacto.getNumeroIdentContacto(), rq.getTokenAutenticatedBusiness());
                        if (rsTelefonosCont.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                            if (rsTelefonosCont.getBody().length > 0) {
                                List<TelefonoDTO> lstTelefonos = Arrays.asList(rsTelefonosCont.getBody());
                                res.setTelefonos(lstTelefonos);
                            }
                        }
                        lstRes.add(res);
                    }
                }

                response.withResponse(lstRes)
                        .withStatus(HttpStatus.OK)
                        .withBusinessStatus(String.valueOf(HttpStatus.OK.value()))
                        .withMessage("Obtiene lista de contactos ");
            } else {
                throw new WebClientException(BusinessException.codeBadOperationGeneric,
                        BusinessException.valueBadOperationGeneric + rsInfoBasica.getMessage());
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.OK)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}
