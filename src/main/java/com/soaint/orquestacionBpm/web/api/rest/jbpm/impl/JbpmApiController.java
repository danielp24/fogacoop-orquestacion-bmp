package com.soaint.orquestacionBpm.web.api.rest.jbpm.impl;

import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.contabilidad.ContabilidadApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.user.IUserApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarLotesProcesoRecaudo;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarProcesoVerificarPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestConsultarPagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetDetallePagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetEntidadesDetallesPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetResolverNovedadEntidadLoteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetallePagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetResolverNovedadEntidadLote;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.jbpm.recaudo.ResponseIniciarLotesProcesoRecaudo;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.business.BusinessUtils;
import com.soaint.orquestacionBpm.commons.Utils.date.DateUtil;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.jbpm.recaudo.EndpointRecaudo;
import com.soaint.orquestacionBpm.commons.constants.api.pagos.PagoStateFilter;
import com.soaint.orquestacionBpm.commons.domains.request.contabilidad.RequestEnviarComprobanteContabilidadDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.service.jbpm.IJbpmApiService;
import com.soaint.orquestacionBpm.service.securityjbpm.ISecurityJbpmService;
import com.soaint.orquestacionBpm.web.api.rest.contabilidad.impl.ContabilidadApiHelper;
import com.soaint.orquestacionBpm.web.api.rest.jbpm.IJbpmApiController;
import com.soaint.orquestacionBpm.web.api.rest.pagos.impl.PagosApiHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = EndpointBase.BASE + EndpointRecaudo.BASE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@Api(description = "Servicio que contiene las operaciones atadas la  orquestación para el JBPM en cuanto a todos sus procesos")
public class JbpmApiController implements IJbpmApiController {

    @Autowired
    private final IUserApiClient userApiClient;

    @Autowired
    private final IJbpmApiService jbpmApiService;

    @Autowired
    private final ISecurityJbpmService securityJbpmService;

    @Autowired
    private final IBusinessApiClient businessApiClient;

    @Autowired
    private final ContabilidadApiCliente contabilidadApiCliente;

    //Properties Parameters
    @Value("${date.permited.format.pattern}")
    public String DATE_FORMAT_PATTERN;

    @Value("${contabilidad.setcomprobante.aplicacionAbonosInicioRecaudo}")
    public String SET_COMPROBANTE_APLICACIONABONOSINICIORECAUDO;

    @Autowired
    private final JbpmApiHelperAsync jbpmApiHelperAsync;

    public JbpmApiController(IJbpmApiService jbpmApicliente, ISecurityJbpmService securityJbpmService, IBusinessApiClient businessApiClient, JbpmApiHelperAsync jbpmApiHelperAsync, IUserApiClient userApiClient, ContabilidadApiCliente contabilidadApiCliente) {
        this.jbpmApiService = jbpmApicliente;
        this.securityJbpmService = securityJbpmService;
        this.businessApiClient = businessApiClient;
        this.jbpmApiHelperAsync = jbpmApiHelperAsync;
        this.userApiClient = userApiClient;
        this.contabilidadApiCliente = contabilidadApiCliente;
    }

    @Override
    @PostMapping(value = EndpointRecaudo.INICIAR_PROCESO_RECAUDO)
    @ApiOperation("Inicia el proceso de recaudo para cada cooperativa en base al corte anterior. Si ingresa una nueva cooperativa, o surgieron cooperativas que no se liquidaron, solo debe ejecutarse nuevamente el servicio.")
    public ResponseEntity iniciarLotesProcesoRecaudo(@RequestBody RequestIniciarLotesProcesoRecaudo requestInicio, HttpServletRequest request) throws WebClientException {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        System.out.println("Datos qde inicio --->" + requestInicio);
        System.out.println("Fecha corte  --->" + requestInicio.getFechaCorte());
        try {
            ValidationUtils.validateNullEmptyObject(requestInicio);
            if (!DateUtil.checkThisDateValid(requestInicio.getFechaCorte(), DATE_FORMAT_PATTERN)) {
                throw new WebClientException(BusinessException.codeCatchAllException, "Fecha de corte invalida, requiere el siguiente formato :'" + DATE_FORMAT_PATTERN + "'.");
            }
            ResponseIniciarLotesProcesoRecaudo resInitLote = JbpmApiHelper.inicializarResponse();
            res.withResponse(resInitLote);
            Gson gson = new Gson();
            //Iniciar Authenticaciones
            TokenDTO token = BusinessUtils.authenticationWithAdminJbpm(this.userApiClient, this.businessApiClient, requestInicio.getOwnerUser());

            //Seccion que verificar las novedades y las aplica a las entidades que correspondan
            System.out.println("Entra a verificar novedades------");
            ResponseGetResolverNovedadEntidadLoteDTO resEntNovLote = this.businessApiClient.getResolverNovedadEntidadLote(requestInicio.getFechaCorte(), token);
            System.out.println("Respuesta del servicio ResolverNovedadEntidadLote  --->" + resEntNovLote);
            System.out.println("Respuesta del servicio ResolverNovedadEntidadLote  con JSON --->" + gson.toJson(resEntNovLote));
            System.out.println("Respuesta del servicio ResolverNovedadEntidadLote  estatus para comparar --->" + resEntNovLote.getStatus());
            if (resEntNovLote.getStatus() == HttpStatus.OK
                    && resEntNovLote.getBody() != null
                    && resEntNovLote.getBody().length > 0) {
                System.out.println("Entra a IF Si hay novedades------");

                for (BodyGetResolverNovedadEntidadLote itemCoopAbono : resEntNovLote.getBody()) {
                    System.out.println("Entra al primer for para recorrer las novedades------");

                    ContabilidadApiHelper contabilidadHelp = new ContabilidadApiHelper();
                    RequestEnviarComprobanteContabilidadDTO rqComprobante = new RequestEnviarComprobanteContabilidadDTO();
                    RequestNitFechaCorteDTO rqBasicInfo = new RequestNitFechaCorteDTO();
                    rqBasicInfo.setFechaCorte(requestInicio.getFechaCorte());
                    System.out.println("rqBasicInfo fecha corte------"+rqBasicInfo.getFechaCorte());
                    rqBasicInfo.setNit(itemCoopAbono.getNumeroIdentificacion());
                    System.out.println("rqBasicInfo NIT------"+rqBasicInfo.getNit());
                    rqComprobante.setBasic(rqBasicInfo);
                    rqComprobante.setConjuntoComprobantes(SET_COMPROBANTE_APLICACIONABONOSINICIORECAUDO);
                    rqComprobante.setIdPago(0);
                    rqComprobante.setTokenAutenticatedBusiness(token);
                    System.out.println("rqComprobante gson------"+gson.toJson(rqComprobante));

                    if (contabilidadHelp.enviarComprobante(rqComprobante, this.contabilidadApiCliente, this.businessApiClient)) {
                        System.out.println("entra a if de enviar comprobante");
                        resInitLote.getEntidadesAplicacionAbono().add(itemCoopAbono);
                    }
                    System.out.println("sale de if de enviar comprobante");
                    System.out.println("agrega item a resInitLote --> "+ resInitLote.getEntidadesAplicacionAbono());
                    System.out.println(" item a resInitLote --> gson"+ gson.toJson(resInitLote.getEntidadesAplicacionAbono()));
                }
            }

            //Seccion que consulta todas las entidades actualziadas
            System.out.println("entra a consumo servicio getEntidadesDetallesPagos");
            ResponseGetEntidadesDetallesPagos resEntidades = this.businessApiClient.getEntidadesDetallesPago(requestInicio.getFechaCorte(), token);
            System.out.println("sale de consumo servicio getEntidadesDetallesPagos-----");
            System.out.println("respuesta consumo servicio getEntidadesDetallesPagos gson-----" + gson.toJson(resEntidades));

            //Seccion de consulta de instancias en el security bpm por nit y la ultima fecha de corte
            System.out.println("entra a consulta de instancias en el security bpm por nit y la ultima fecha de corte");
            List<ResponseSearchInstancesLastFechaCorteDTO> listaLastInstances = this.securityJbpmService.getInstancesByLastFechaCorte(requestInicio.getFechaCorte());
            System.out.println("sale de consulta de instancias en el security bpm por nit y la ultima fecha de corte");
            System.out.println(" respuesta instancias en el security bpm por nit y la ultima fecha de corte con gson--->" + gson.toJson(listaLastInstances));
            System.out.println(" respuesta instancias en el security bpm por nit y la ultima fecha de corte--->" + listaLastInstances);


            //Resolviendo las instancias dependeiendo de las condicioens de negocio
            JbpmApiHelper.resolveInstanceLoteRecaudo(
                    token,
                    requestInicio,
                    resEntidades,
                    listaLastInstances,
                    resInitLote,
                    this.jbpmApiService,
                    this.securityJbpmService
            );
            res.withStatus(HttpStatus.OK)
                    .withMessage(resInitLote.getEntidadesPreviamenteEjecutados().size() + " Entidades que se encuentran previamente ejecutadas en el periodo " + requestInicio.getFechaCorte() + ". | " +
                            resInitLote.getEntidadesSinEjecutar().size() + " Entidades que se encuentran sin ejecutar en el periodo " + requestInicio.getFechaCorte() + " bien sea por falta de datos para el corte o que no tienen saldos en contra. | " +
                            resInitLote.getEntidadesProcesosEjecutados().size() + " Entidades que fueron ejecutadas exitosamente en el periodo " + requestInicio.getFechaCorte() + " bien sea por tener saldos pendientes, ya habiendo transmitido los datos de la liquidacion, o reliquidacion. |" +
                            resInitLote.getEntidadesAplicacionAbono().size() + " Entidades que se encuentran liquidadas que se le ha aplicado el abono para el periodo actual.")
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK);

        } catch (WebClientException e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @Override
    @PostMapping(value = EndpointRecaudo.ENVIAR_PAGO_RECAUDO)
    @ApiOperation("Verifica si se puede continuar con el pago y ejecuta las señales dependiendo del caso")
    public ResponseEntity enviarPagoProcesoRecaudo(@RequestBody RequestIniciarProcesoVerificarPagos rq, HttpServletRequest request) throws WebClientException {

        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            RequestConsultarPagosDTO rqPagos = new RequestConsultarPagosDTO();
            rqPagos.setNit(rq.getBasic().getNit());
            rqPagos.setFechaCorte(rq.getBasic().getFechaCorte());
            ResponseGetDetallePagosDTO responsePagos = this.businessApiClient.getDetallePagos(rqPagos, rq.getTokenAutenticatedBusiness());

            PagosApiHelper.filtrarPagosHabilitadosComparacion(responsePagos);

            if (responsePagos.getBody().size() > 0) {

                Boolean goRecaudo = Boolean.TRUE;
                for (BodyGetDetallePagosDTO pago : responsePagos.getBody()) {
                    if (pago.getCodigoEstadoPago().toString().equals(PagoStateFilter.OK)) {
                        goRecaudo = Boolean.FALSE;
                        break;
                    }
                }

                if (goRecaudo) {
                    this.jbpmApiHelperAsync.enviarPagoProcesoRecaudo(this.securityJbpmService, this.jbpmApiService, rq);
                    response.withStatus(HttpStatus.OK)
                            .withMessage("Envio secuencia pago recaudo");
                } else {
                    response.withStatus(HttpStatus.OK)
                            .withMessage("No se han enviado todos los pagos");
                }
            } else {
                response.withStatus(HttpStatus.OK)
                        .withMessage("No has pagos para procesar");
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }


    }
}
