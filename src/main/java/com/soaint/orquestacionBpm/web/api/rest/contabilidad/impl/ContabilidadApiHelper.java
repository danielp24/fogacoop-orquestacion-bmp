package com.soaint.orquestacionBpm.web.api.rest.contabilidad.impl;

import com.google.gson.Gson;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.contabilidad.ContabilidadApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.business.RequestGetBusinessValuesContabilidadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.business.RequestResolverComprobanteContableDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.contabilidad.RequestRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseFindIdDominioDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetValueBusinessContabilidadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseResolverComprobanteContableDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyFindIdDominioDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetValuesBusinessContabilidadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.contabilidad.ResponseRegistrarComprobanteDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.Utils.xml.XmlUtils;
import com.soaint.orquestacionBpm.commons.domains.generic.contabilidad.Comprobante;
import com.soaint.orquestacionBpm.commons.domains.generic.contabilidad.ComprobanteFilter;
import com.soaint.orquestacionBpm.commons.domains.generic.contabilidad.ContextComprobantes;
import com.soaint.orquestacionBpm.commons.domains.generic.contabilidad.RegistroContable;
import com.soaint.orquestacionBpm.commons.domains.request.contabilidad.RequestEnviarComprobanteContabilidadDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ContabilidadApiHelper {


    private static void verificarTipoSetComprobantes(RequestEnviarComprobanteContabilidadDTO request,
                                                     IBusinessApiClient businessApiClient,
                                                     List<Comprobante> setComprobantes,
                                                     ContextComprobantes context) throws WebClientException {
        //Logica de negocio para determinar cual de los comprobantes de usara dependiendo del pago que se realizo y las condiciones de la entodad

        //1 Consultar el dominio del Conjunto de Comprobantes
        ResponseFindIdDominioDTO dominioContable = businessApiClient.getDominioContable(request.getConjuntoComprobantes(), request.getTokenAutenticatedBusiness());
        System.out.println("Dominios contables consultados --> " + dominioContable);
        LogUtils.info(dominioContable.toString());
        Map<String, List<ComprobanteFilter>> conjComp = new HashMap<>();
        filtrarIdRegistrosContables(dominioContable, conjComp);

        //Llena el conjunto de comprobantes de acorde al comprobante recibido
        fillSetComprobantes(dominioContable, request, conjComp, setComprobantes, businessApiClient, context);
    }

    private static void filtrarIdRegistrosContables(ResponseFindIdDominioDTO dominioContable, Map<String, List<ComprobanteFilter>> conjComp) {
        List<BodyFindIdDominioDTO> setComp = determinarRegistrosByName(dominioContable, "setComp");
        List<BodyFindIdDominioDTO> listComp = determinarRegistrosByParentId(dominioContable, setComp.get(0).getIdParametro().toString());
        List<ComprobanteFilter> listComFilt = new ArrayList<>();
        for (BodyFindIdDominioDTO comp : listComp) {
            ComprobanteFilter compFil = new ComprobanteFilter();
            List<BodyFindIdDominioDTO> element = determinarRegistrosByNameAndParentId(dominioContable, "registrocontable", comp.getIdParametro().toString());
            List<String> elmRegCont = new ArrayList<>();
            Map<String, List<String>> listElm = new HashMap<>();
            for (BodyFindIdDominioDTO elm : element) {
                elmRegCont.add(elm.getIdParametro().toString());
            }
            listElm.put(comp.getIdParametro().toString(), elmRegCont);
            compFil.setElementosContablesFilter(listElm);
            listComFilt.add(compFil);
        }
        conjComp.put(setComp.get(0).getIdParametro().toString(), listComFilt);
    }

    private static void fillSetComprobantes(ResponseFindIdDominioDTO dominioContable,
                                            RequestEnviarComprobanteContabilidadDTO request,
                                            Map<String, List<ComprobanteFilter>> conjComp,
                                            List<Comprobante> setComprobantes,
                                            IBusinessApiClient businessApiClient,
                                            ContextComprobantes context) throws WebClientException {
        List<Comprobante> listComp = new ArrayList<>();
        System.out.println("==================ENTRADA DE DATOS FILLSETCOMPROBANTES===================");
        System.out.println("dominio contable -->" + dominioContable);
        System.out.println("rq enviar comprobanteContabilidad -->" + request.toString());
        System.out.println("map string lista comprobanteFilter -->" + conjComp.toString());
        System.out.println("lista comprobante -->" + setComprobantes.toString());
        System.out.println("Bussiness Api Cliente -->" + businessApiClient.toString());
        System.out.println("Contexto Comprobantes -->" + context.toString());
        System.out.println("==================SALIDA DE DATOS FILLSETCOMPROBANTES===================");
        try {
            String idParamTransversal = determinarRegistrosByName(dominioContable, "transversalParameters").get(0).getIdParametro().toString();
            System.out.println("==================ENTRADA AL PRIMER FOR ===================");
            for (List<ComprobanteFilter> item : conjComp.values()) {
                LogUtils.info(item.toString());
                System.out.println("info item determinar registros by name--> " + item.toString());
                System.out.println("==================ENTRADA AL SEGUNDO FOR ===================");
                for (ComprobanteFilter itemComp : item) {
                    List<String> listReg = new ArrayList<>();
                    Comprobante comp = new Comprobante();
                    itemComp.getElementosContablesFilter().forEach((idParamComprobante, listRegContable) -> {
                        List<RegistroContable> listaRegistrosContables = new ArrayList<>();
                        resolveParamsStaticDB(comp, dominioContable, idParamComprobante, idParamComprobante, idParamTransversal);
                        listRegContable.forEach(regContable -> {
                            try {
                                System.out.println("==================ENTRADA AL TRY DENTRO DEL 2DO FOR ===================");
                                resolveBusinessParam(request, listaRegistrosContables, comp, dominioContable, regContable, idParamComprobante, idParamTransversal, businessApiClient, listReg);
                                System.out.println("=======================SALGO DEL TRY================================");
                            } catch (WebClientException e) {
                                LogUtils.error(e);
                            }
                        });
                        if (listaRegistrosContables.size() > 0) {
                            System.out.println("=================ENTRO A LA LISTA");
                            comp.setRegistrocontable(listaRegistrosContables);
                            System.out.println("======================SALGO DE LA LISTA");
                        }
                    });
                    if (comp.getRegistrocontable() != null
                            && comp.getRegistrocontable().size() > 0) {
                        System.out.println("===================ENTRO AL 2DO IF");
                        listComp.add(comp);
                        context.getListRegContables().add(String.join("|", listReg));
                        System.out.println("=================SALGO DEL 2DO IF");
                    }
                    System.out.println("==================SALIDA DEL SEGUNDO FOR ===================");
                }
            }
            System.out.println("==================SALIDA DEL PRIMER FOR ===================");

            System.out.println("DEVUELVE EL MÉTODO AL FINAL: ----> " +setComprobantes.toString());

            setComprobantes.addAll(listComp);
        } catch (Exception e) {
            //LogUtils.error(e);
            System.out.println("==================MENSAJE ERROR ===================");
            System.out.println(e.getMessage());
            System.out.println("==================CAUSA ERROR ===================");
            System.out.println(e.getCause());
            throw new WebClientException(BusinessException.codeBadOperationGeneric, e);
        }
    }

    private static void resolveBusinessParam(RequestEnviarComprobanteContabilidadDTO request,
                                             List<RegistroContable> listaRegistrosContables,
                                             Comprobante comp,
                                             ResponseFindIdDominioDTO dominioContable,
                                             String regContable,
                                             String idParamComprobante,
                                             String idParamTransversal,
                                             IBusinessApiClient businessApiClient,
                                             List<String> listReg) throws WebClientException {
        Gson gson = new Gson();
        System.out.println("rq enviarcomprobante contabilidad --->" + request.toString());
        System.out.println("lista registro contable --->" + gson.toJson(listaRegistrosContables));
        System.out.println("comprobante --->" + gson.toJson(comp));
        System.out.println("regContable --->" + regContable);
        System.out.println("idparamComprobante --->" + idParamComprobante);
        System.out.println("idparamTransversal --->" + idParamTransversal);
        System.out.println("iBussiness Apli cliente --->" + businessApiClient);

        List<BodyFindIdDominioDTO> listDominio = determinarRegistrosByNameAndParentId(dominioContable, "business", regContable);
        System.out.println("listDominio --->" + listDominio);
        if (listDominio.size() == 0) {
            return;
        }
        String idBusinessParam = listDominio.get(0).getIdParametro().toString();
        RequestGetBusinessValuesContabilidadDTO rqBusinessReg = new RequestGetBusinessValuesContabilidadDTO();
        rqBusinessReg.setBasic(request.getBasic());
        rqBusinessReg.setIdParentBusinessParam(Integer.parseInt(idBusinessParam));
        rqBusinessReg.setIdPago(request.getIdPago());
        System.out.println("rq que se envia a getBusinessValuesContabilidad-->" + gson.toJson(rqBusinessReg));
        ResponseGetValueBusinessContabilidadDTO resBusinessValues = businessApiClient.getBusinessValuesContabilidad(rqBusinessReg, request.getTokenAutenticatedBusiness());
        System.out.println("ANTES DEL IF response getBusinessValuesContabilidad--->:" +gson.toJson(resBusinessValues));

        if (resBusinessValues.getStatus().equals(HttpStatus.OK)) {
            if (resBusinessValues.getBody().length > 0) {
                System.out.println("=============== If getBody ================ "+resBusinessValues.getBody().length);
                for (BodyGetValuesBusinessContabilidadDTO item : resBusinessValues.getBody()) {
                    System.out.println("=====================Entre al for de getBody==============");
                    System.out.println(item.toString());
                    RegistroContable regCont = new RegistroContable();
                    System.out.println("==========Entro a resolveParamsBusinessDB=========");
                    System.out.println("=======parametro================regCont====="+regCont);
                    System.out.println("=======parametro================item.getKeyListStringResult()====="+item.getKeyListStringResult());
                    System.out.println("=======parametro================item.getValueListStringResult()====="+item.getValueListStringResult());
                    resolveParamsBusinessDB(regCont, item.getKeyListStringResult(), item.getValueListStringResult());
                    System.out.println("==========Salgo a resolveParamsBusinessDB=========");
                    System.out.println("==========Entro a resolveParamsBusinessDB 2=========");
                    System.out.println("=======parametro================comp==="+comp);
                    System.out.println("=======parametro================item.getKeyListStringResult()==="+item.getKeyListStringResult());
                    System.out.println("=======parametro================item.getValueListStringResult()==="+item.getValueListStringResult());
                    resolveParamsBusinessDB(comp, item.getKeyListStringResult(), item.getValueListStringResult());
                    System.out.println("==========Salgo a resolveParamsBusinessDB 2=========");
                    System.out.println("==========Entro a resolveParamsStaticDB=========");
                    System.out.println("=======parametro================regCont==="+regCont);
                    System.out.println("=======parametro================dominioContable==="+dominioContable);
                    System.out.println("=======parametro================regContable==="+regContable);
                    System.out.println("=======parametro================idParamComprobante==="+idParamComprobante);
                    System.out.println("=======parametro================idParamTransversal==="+idParamTransversal);
                    resolveParamsStaticDB(regCont, dominioContable, regContable, idParamComprobante, idParamTransversal);
                    System.out.println("==========Salgo a resolveParamsStaticDB=========");
                    listaRegistrosContables.add(regCont);
                }
                listReg.add(idBusinessParam);
                System.out.println("LIST REG SALIDA --->" + gson.toJson(listReg));
            }
        }
        LogUtils.info(idBusinessParam);
        System.out.println("SALGO DEL resolveBusinessParam ");

    }

    private static <T> T resolveParamsBusinessDB(T item, String keyStringResult, String valueStringResult) {
        List<String> keysListStringResult = Arrays.asList(keyStringResult.split("\\|"));
        List<String> valuesListStringResult = Arrays.asList(valueStringResult.split("\\|"));
        final Field[] attr = item.getClass().getDeclaredFields();
        for (int i = 0; i < attr.length; i++) {
            final Field temp = attr[i];
            temp.setAccessible(true);
            if (!java.lang.reflect.Modifier.isStatic(temp.getModifiers())) {
                for (int j = 0; j < keysListStringResult.size(); j++) {
                    if (keysListStringResult.get(j) != null
                            && temp.getName().equals(keysListStringResult.get(j))) {
                        try {
                            temp.set(item, valuesListStringResult.get(j));
                            break;
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return item;
    }

    private static <T> T resolveParamsStaticDB(T item,
                                               ResponseFindIdDominioDTO dominioContable,
                                               String idParentFilter,
                                               String idParamComprobante,
                                               String idParamTransversal) {

        final Field[] attr = item.getClass().getDeclaredFields();
        for (int i = 0; i < attr.length; i++) {
            final Field temp = attr[i];
            temp.setAccessible(true);
            if (!java.lang.reflect.Modifier.isStatic(temp.getModifiers())) {
                for (int j = 0; j < dominioContable.getBody().size(); j++) {
                    if (dominioContable.getBody().get(j).getIdPadre() != null
                            && (dominioContable.getBody().get(j).getIdPadre().toString().equals(idParamTransversal)
                            || dominioContable.getBody().get(j).getIdPadre().toString().equals(idParentFilter)
                            || dominioContable.getBody().get(j).getIdPadre().toString().equals(idParamComprobante))
                            && temp.getName().equals(dominioContable.getBody().get(j).getNombre())) {
                        try {
                            temp.set(item, (dominioContable.getBody().get(j).getValor() == null ? "" : dominioContable.getBody().get(j).getValor()));
                            break;
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        Gson gson = new Gson();
        System.out.println("Salida Helper resolveParamStatic --> "+ gson.toJson(item));
        return item;
    }

    private static List<BodyFindIdDominioDTO> determinarRegistrosByName(ResponseFindIdDominioDTO dominioContable, String nameValue) {
        ResponseFindIdDominioDTO temp = new ResponseFindIdDominioDTO();
        temp.setBody(new ArrayList<BodyFindIdDominioDTO>());
        temp.getBody().addAll(dominioContable.getBody());
        temp.setBody(temp.getBody().stream().filter(dominio ->
                dominio.getNombre().startsWith(nameValue)
        ).collect(Collectors.toList()));
        return temp.getBody();
    }

    private static List<BodyFindIdDominioDTO> determinarRegistrosByNameAndParentId(ResponseFindIdDominioDTO dominioContable, String nameValue, String idParent) {
        Gson gson = new Gson();
        System.out.println("Entrada a determinarRegistrosByNameAndParentId  --> "+ gson.toJson(dominioContable));
        System.out.println("Entrada a determinarRegistrosByNameAndParentId  nameValue --> "+ nameValue);
        System.out.println("Entrada a determinarRegistrosByNameAndParentId idParent --> "+ idParent);

        ResponseFindIdDominioDTO temp = new ResponseFindIdDominioDTO();
        temp.setBody(new ArrayList<BodyFindIdDominioDTO>());
        temp.getBody().addAll(dominioContable.getBody());
        temp.setBody(temp.getBody().stream().filter(dominio ->
                dominio.getNombre().startsWith(nameValue)
                        && (dominio.getIdPadre() != null
                        && dominio.getIdPadre().toString().equals(idParent))
        ).collect(Collectors.toList()));

        System.out.println("SALIDA  de determinarRegistrosByNameAndParentId  --> "+ temp.getBody());
        return temp.getBody();
    }

    private static List<BodyFindIdDominioDTO> determinarRegistrosByParentId(ResponseFindIdDominioDTO dominioContable, String idParent) {
        ResponseFindIdDominioDTO temp = new ResponseFindIdDominioDTO();
        temp.setBody(new ArrayList<BodyFindIdDominioDTO>());
        temp.getBody().addAll(dominioContable.getBody());
        temp.setBody(temp.getBody().stream().filter(dominio ->
                dominio.getIdPadre() != null
                        && dominio.getIdPadre().toString().equals(idParent)
        ).collect(Collectors.toList()));
        return temp.getBody();
    }

    public ContextComprobantes resolverConjuntoComprobantes(RequestEnviarComprobanteContabilidadDTO request
            , IBusinessApiClient businessApiClient) throws WebClientException {
        ContextComprobantes context = new ContextComprobantes();
        context.setListRegContables(new ArrayList<>());
        List<RequestRegistrarComprobanteDTO> listRequestCont = new ArrayList<>();
        try {
            //Buscar conjuntos de comprobantes a resolver
            List<Comprobante> setComprobantes = new ArrayList<>();
            verificarTipoSetComprobantes(request, businessApiClient, setComprobantes, context);
            context.setListComprobantes(setComprobantes);
            if (setComprobantes.size() > 0) {
                LogUtils.info(">>>>>>>>>>>>>>>\n\n Iniciando comprobante contable - Entidad = " + request.getBasic().getNit());
                for (Comprobante item : setComprobantes) {
                    RequestRegistrarComprobanteDTO rqCont = new RequestRegistrarComprobanteDTO();
                    rqCont.setUnaAccion(2);
                    rqCont.setUnComprobante(XmlUtils.jaxbObjectToXMLString(item));
                    LogUtils.info(rqCont.getUnComprobante());
                    listRequestCont.add(rqCont);
                }
                LogUtils.info(">>>>>>>>>>>>>>>\n\n Finalizando comprobante contable");
            } else {
                throw new WebClientException(BusinessException.codeSizeIsEmpty,
                        BusinessException.valueSizeIsEmpty + ", No hay parametros atados al Conjunto de comprobantes, denominados por el codigo : " + request.getConjuntoComprobantes());
            }

        } catch (WebClientException e) {
            throw new WebClientException(e.getMessage());
        }
        context.setListRequestCont(listRequestCont);
        return context;
    }

    public ResponseResolverComprobanteContableDTO resolverComprobante(RequestEnviarComprobanteContabilidadDTO rqComprobante,
                                                                      Integer posReg,
                                                                      IBusinessApiClient businessApiClient,
                                                                      ContextComprobantes context,
                                                                      Integer tipoResolv) throws WebClientException {
        RequestResolverComprobanteContableDTO rqComp = new RequestResolverComprobanteContableDTO();
        rqComp.setIdPago(rqComprobante.getIdPago());
        rqComp.setListRegContable(context.getListRegContables().get(posReg));
        rqComp.setBasic(rqComprobante.getBasic());
        rqComp.setCodigoContabilidad(context.getListComprobantes().get(posReg).getCodigoContabilidad());
        rqComp.setTipoResolComprobante(tipoResolv);
        for (RegistroContable item : context.getListComprobantes().get(posReg).getRegistrocontable()) {
            if (item.getTipoComprobante() != null && !"".equals(item.getTipoComprobante())
                    && item.getFecha() != null && !"".equals(item.getFecha())
                    && item.getPlanCuentas() != null && !"".equals(item.getPlanCuentas())
                    && item.getCuenta() != null && !"".equals(item.getCuenta())
                    && item.getDocumentoSoporte() != null && !"".equals(item.getDocumentoSoporte())
                    && item.getNumeroDocSoporte() != null && !"".equals(item.getNumeroDocSoporte())
            ) {
                rqComp.setTipoComprobante(item.getTipoComprobante());
                rqComp.setFecha(item.getFecha());
                rqComp.setPlanCuentas(item.getPlanCuentas());
                rqComp.setCuenta(item.getCuenta());
                rqComp.setCodigoDocSoporte(item.getDocumentoSoporte());
                rqComp.setNumeroDocSoporte(item.getNumeroDocSoporte());
                break;
            }
        }
        return businessApiClient.getResolverComprobanteContable(rqComp, rqComprobante.getTokenAutenticatedBusiness());
    }


    public Boolean enviarComprobante(RequestEnviarComprobanteContabilidadDTO rqComprobante,
                                     ContabilidadApiCliente contabilidadApiCliente,
                                     IBusinessApiClient businessApiClient) throws WebClientException {

        ContextComprobantes contexto = this.resolverConjuntoComprobantes(rqComprobante, businessApiClient);
        if (contexto.getListRequestCont().size() > 0) {
            for (int posReg = 0; posReg < contexto.getListRequestCont().size(); posReg++) {
                ResponseResolverComprobanteContableDTO resComp = this.resolverComprobante(rqComprobante, posReg, businessApiClient, contexto, 1);
                if (!resComp.getBusinessStatus().equals(String.valueOf(HttpStatus.CREATED.value()))) {
                    ResponseRegistrarComprobanteDTO responsePagos = contabilidadApiCliente.registrarComprobante(contexto.getListRequestCont().get(posReg));
                    if (responsePagos.getStatus() != HttpStatus.BAD_REQUEST) {
                        resComp = this.resolverComprobante(rqComprobante, posReg, businessApiClient, contexto, 2);
                        if (!resComp.getBusinessStatus().equals(String.valueOf(HttpStatus.OK.value()))) {
                            LogUtils.error(responsePagos.getMessage());
                            return Boolean.FALSE;
                        }
                    } else {
                        LogUtils.error(responsePagos.getMessage());
                        return Boolean.FALSE;
                    }
                }
            }
        }
        return Boolean.TRUE;
    }
}
