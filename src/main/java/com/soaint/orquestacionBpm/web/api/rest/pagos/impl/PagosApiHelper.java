package com.soaint.orquestacionBpm.web.api.rest.pagos.impl;


import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetDetallePagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetPagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetalleLiquidacionDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetallePagosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.details.PagosDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.jbpm.ParamBusinessName;
import com.soaint.orquestacionBpm.commons.constants.api.pagos.PagoStateFilter;
import com.soaint.orquestacionBpm.commons.constants.api.pagos.PagoStateReturn;
import io.swagger.annotations.Api;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Api(description = "Controles que  permiten generar operaciones en relacion a los pagos que se realizan a Fogacoop.")
public class PagosApiHelper {


    public static Boolean isCero(BigDecimal value) {
        return (value.compareTo(new BigDecimal(0))==0)?Boolean.TRUE:Boolean.FALSE;
    }

    public static void filtrarPagosHabilitadosComparacion(ResponseGetDetallePagosDTO responsePagos) {
        ResponseGetDetallePagosDTO temp = new ResponseGetDetallePagosDTO();
        temp.setBody(new ArrayList<BodyGetDetallePagosDTO>());
        temp.getBody().addAll(responsePagos.getBody());
        responsePagos.getBody().clear();
        responsePagos.setBody(temp.getBody().stream().filter(pago ->
                !pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.CREATED)
                        && !pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.PENDING)
                        && !pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.FAILED)
                        && !pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.NOT_AUTHORIZED)
                        && !pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.EXPIRED)
        ).collect(Collectors.toList()));
    }

    public static BigDecimal totalPagosHabilitados(ResponseGetDetallePagosDTO responsePagos) {
        Double sum = 0.0;
       try {
           if (responsePagos.getBody().size() > 0) {
               for (BodyGetDetallePagosDTO pago : responsePagos.getBody()) {
                   sum = sum + pago.getValorPagado().doubleValue();
               }
               return new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP);
           } 
       }
       catch (Exception e){
           LogUtils.error(e);
       }
        return new BigDecimal(0);
    }

    public static void filtrarByOrigen(ResponseGetDetallePagosDTO responsePagos, String origenPago) {
        ResponseGetDetallePagosDTO temp = new ResponseGetDetallePagosDTO();
        temp.setBody(new ArrayList<BodyGetDetallePagosDTO>());
        temp.getBody().addAll(responsePagos.getBody());
        responsePagos.getBody().clear();
        if(origenPago.equals(ParamBusinessName.ATTR_TIPOPAGO_PSE)){
            responsePagos.setBody(temp.getBody().stream().filter(pago ->
                    pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.PENDING)
                            || pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.OK)
             ).collect(Collectors.toList()));
        } else if(origenPago.equals(ParamBusinessName.ATTR_TIPOPAGO_CT)){
            responsePagos.setBody(temp.getBody().stream().filter(pago ->
                    pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.OK)
            ).collect(Collectors.toList()));
        } else if(origenPago.equals(ParamBusinessName.ATTR_TIPOPAGO_INFOPAGONOLIQ)){
            responsePagos.setBody(temp.getBody().stream().filter(pago ->
                    pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.NO_LIQUIDADO)
                            || pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.NO_COMPROBANTE)
                            || pago.getCodigoEstadoPago().toString().equalsIgnoreCase(PagoStateFilter.NO_APLICADO)
            ).collect(Collectors.toList()));
        }


    }

    public static BodyGetDetallePagosDTO obtenerPago(String idPago, ResponseGetDetallePagosDTO responsePagos) {
        ResponseGetDetallePagosDTO temp = new ResponseGetDetallePagosDTO();
        temp.setBody(new ArrayList<BodyGetDetallePagosDTO>());
        temp.getBody().addAll(responsePagos.getBody());
        responsePagos.getBody().clear();
        responsePagos.setBody(temp.getBody().stream().filter(pago ->
                pago.getNroChequeTransac().equalsIgnoreCase(idPago)
        ).collect(Collectors.toList()));
        return responsePagos.getBody().get(0);
    }

    public static String resolverSaldos(BodyGetDetalleLiquidacionDTO body, Boolean pagoCorteActual) {
        String result = "";
        //Validando cuando hay pagos en el corte actual
        if (pagoCorteActual) {
            if (body.getValorTotalSugerido().toString().equals("0")
                    && !body.getSaldoFavor().toString().equals("0")) {
                result = PagoStateReturn.PAGAR_EXCEDENTE;
            } else if (body.getValorTotalSugerido().toString().equals("0")
                    && body.getSaldoFavor().toString().equals("0")) {
                result = PagoStateReturn.PAGADO;
            } else if (!body.getValorTotalSugerido().toString().equals("0")) {
                result = PagoStateReturn.PAGAR_PARCIAL;
            }
            //Validando cuando no hay pagos en el corte actual
        } else {
            result = PagoStateReturn.PAGAR_TOTAL;
        }

        return result;
    }
}
