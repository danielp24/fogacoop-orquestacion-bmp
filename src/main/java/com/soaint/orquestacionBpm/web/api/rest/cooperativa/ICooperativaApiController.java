package com.soaint.orquestacionBpm.web.api.rest.cooperativa;

import com.soaint.orquestacionBpm.commons.domains.request.cooperativa.RequestGetContactosEntidadDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface ICooperativaApiController {

    ResponseEntity getContactosEntidad(RequestGetContactosEntidadDTO rq, HttpServletRequest request) throws WebClientException;

}