package com.soaint.orquestacionBpm.web.api.rest.report.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.report.ReportApiCliente;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.report.RequestFR71DTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.report.ResponseReportDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.report.EndpointReport;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.web.api.rest.report.IReportApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = EndpointBase.BASE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@Api(description = "Servicio que contiene las operaciones atadadas A la generación de un reporte específico.")
public class ReportApiController implements IReportApiController {

    @Autowired
    private final ReportApiCliente reportApiCliente;

    public ReportApiController(ReportApiCliente reportApiCliente) {
        this.reportApiCliente = reportApiCliente;
    }

    @PostMapping(EndpointReport.REPORT)
    @ApiOperation("Controlador que genera el reporte FR71")
    public ResponseEntity reportFR71(@RequestBody RequestFR71DTO fr71, HttpServletRequest request) throws WebClientException {

        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            ResponseReportDTO responseFR71 = reportApiCliente.dataReportConsume(fr71);
            res.withResponse(responseFR71);
            if (responseFR71.getBody().getBase64() == null) {
                res.withStatus(HttpStatus.OK)
                        .withBusinessStatus(String.valueOf(HttpStatus.BAD_REQUEST))
                        .withMessage(BusinessException.valueObjectNoFound);
            } else {
                res.withStatus(HttpStatus.OK)
                        .withMessage("OK");
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.OK)
                    .withBusinessStatus(HttpStatus.BAD_REQUEST + e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}
