package com.soaint.orquestacionBpm.web.api.rest.contabilidad;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarLotesProcesoRecaudo;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarProcesoVerificarPagos;
import com.soaint.orquestacionBpm.commons.domains.request.contabilidad.RequestEnviarComprobanteContabilidadDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IContabilidadApiController {

    ResponseEntity enviarComprobante(RequestEnviarComprobanteContabilidadDTO rqComoprobante, HttpServletRequest request) throws WebClientException;

}