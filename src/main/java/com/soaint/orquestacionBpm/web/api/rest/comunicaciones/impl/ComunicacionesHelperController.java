package com.soaint.orquestacionBpm.web.api.rest.comunicaciones.impl;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.RequestSaveCorreoOpDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.lstAdjuntosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseFindIdDominioDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetInfoBasicaDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyFindIdDominioDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyResponseGetInfoBasica;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones.ResponseGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.comunicaciones.body.BodyGetDestinatariosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.CorreoPlantillaDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.ObjectUtils;

import java.util.*;

@ApiOperation("Clase auxiliar para ejecutar operaciones rápidas y ser llamadas en el controlador")

public class ComunicacionesHelperController {

    public static String reemplazarCuerpoMensaje1(List<String> pTemplate, HashMap<String, String> template, String[] arreglo, String cuerpo) throws WebClientException {
        ResponseBuilder responseBuilder = ResponseBuilder.newBuilder();
        String texto;
        if (pTemplate.size() != arreglo.length) {
            throw new WebClientException(BusinessException.codeParametersListIncomplet, BusinessException.valueParametersListIncomplet);
        }

        if (ObjectUtils.isEmpty(pTemplate.size())) {
            throw new WebClientException(BusinessException.codeParametersListEmpty, BusinessException.valueParametersListEmpty);
        }
        for (int i = 0; i < pTemplate.size(); i++) {
            template.put(pTemplate.get(i), arreglo[i]);
            for (Map.Entry<String, String> entry : template.entrySet()) {
                texto = cuerpo.replaceAll("<" + entry.getKey() + "/>", entry.getValue());
                cuerpo = texto;
            }
        }
        return cuerpo;
    }

    public static String reemplazarCuerpoMensaje2(List<String> pTemplate, HashMap<String, String> template, List<String> businessList, String cuerpo) throws WebClientException {

        String texto;

        if (pTemplate.size() != businessList.size()) {
            throw new WebClientException(BusinessException.codeParametersListIncomplet, BusinessException.valueParametersListIncomplet);
        }

        if (ObjectUtils.isEmpty(pTemplate.size())) {
            throw new WebClientException(BusinessException.codeParametersListEmpty, BusinessException.valueParametersListEmpty);
        }

        for (int i = 0; i < pTemplate.size(); i++) {
            template.put(pTemplate.get(i), businessList.get(i));
            for (Map.Entry<String, String> entry : template.entrySet()) {
                texto = cuerpo.replaceAll("<" + entry.getKey() + "/>", entry.getValue());
                cuerpo = texto;
            }
        }
        return cuerpo;
    }

    public static String convertirBusinessListToString(List<String> businessList) {
        //concatenamos los valores ingresados, para pasarlos al SP
        String businessGeneric = "";
        for (int i = 0; i < businessList.size(); i++) {
            businessGeneric += businessList.get(i).concat("|");
        }
        return businessGeneric;
    }

    public static HashMap<String, String> getMapBusiness(HashMap<String, String> business, List<String> pBusiness, List<String> businessList) {
        for (int i = 0; i < pBusiness.size(); i++) {
            business.put(pBusiness.get(i), businessList.get(i));
        }
        return business;
    }

    public static RequestSaveCorreoOpDTO buildCorreo(List<String> destinatariosPrincipales, List<String> destinatariosSecundarios, String asunto, String cuerpo, List<lstAdjuntosDTO> adj, String setDe, String setUsuario, String setIdTipNotificausuario, String usuario, String password) {

        //Consumimos el servicio de opeCorreoGuarda para enviar el correo de norificacion
        RequestSaveCorreoOpDTO requestSaveCorreoOpDTO = new RequestSaveCorreoOpDTO();
        requestSaveCorreoOpDTO.setDe(setDe);
        requestSaveCorreoOpDTO.setLstPara(destinatariosPrincipales);
        requestSaveCorreoOpDTO.setLstParaCopia(destinatariosSecundarios);
        requestSaveCorreoOpDTO.setAsunto(asunto);
        requestSaveCorreoOpDTO.setCuerpo(cuerpo);
        requestSaveCorreoOpDTO.setLstAdjuntos(adj);
        requestSaveCorreoOpDTO.setConsRadicado("");
        requestSaveCorreoOpDTO.setCodExpediente("");
        requestSaveCorreoOpDTO.setNomSeparador("");
        requestSaveCorreoOpDTO.setIdTipNotifica(setIdTipNotificausuario);
        requestSaveCorreoOpDTO.setUsuario(setUsuario);
        requestSaveCorreoOpDTO.setUsername(usuario);
        requestSaveCorreoOpDTO.setPassword(password);

        LogUtils.debug(">>>>>>>>>>>>>>\n\n Envio de correo = " + requestSaveCorreoOpDTO.toString());

        return requestSaveCorreoOpDTO;
    }

    public static List<String> getTemplate(List<String> pTemplate, ResponseFindIdDominioDTO dominioDTO) {
        int indexTemplate = 0;
        String idPadreTemplate = "";

        if (dominioDTO.getBody().size() > 0) {
            //Verificando cual es el ID Padre
            for (BodyFindIdDominioDTO parameter : dominioDTO.getBody()) {
                if (parameter.getIdPadre() == null
                        && parameter.getNombre().equals("pTemplate")) {
                    idPadreTemplate = parameter.getIdParametro().toString();
                    break;
                }
            }
            //Creando lista temporal para los paramteros que pertenecen al ID padre
            List<BodyFindIdDominioDTO> listIdParent = new ArrayList<>();
            //agregando items teniendo el ID Padre
            for (BodyFindIdDominioDTO bodyFindIdDominioDTO : dominioDTO.getBody()) {
                if (bodyFindIdDominioDTO.getIdPadre() != null
                        && idPadreTemplate.equals(bodyFindIdDominioDTO.getIdPadre().toString())) {
                    listIdParent.add(bodyFindIdDominioDTO);
                    indexTemplate++;
                }
            }
            //ordenando la lista
            Collections.sort(listIdParent);
            for (BodyFindIdDominioDTO param : listIdParent) {
                pTemplate.add(param.getNombre());
            }
        }
        return pTemplate;
    }

    public static List<String> getBusiness(ResponseFindIdDominioDTO dominioDTO, List<String> pBusiness, int pos) {
        for (BodyFindIdDominioDTO bodyFindIdDominioDTO : dominioDTO.getBody()) {
            if (bodyFindIdDominioDTO.getIdDominio().equals(bodyFindIdDominioDTO.getIdPadre())) {
                pBusiness.add(pos, bodyFindIdDominioDTO.getNombre());
                pos++;
            }
        }
        return pBusiness;
    }

    public static List<String> getCorrerosElectronicos(ResponseGetInfoBasicaDTO infoBasicaDTO, List<String> correoElectronico) {
        for (BodyResponseGetInfoBasica basica : infoBasicaDTO.getBody()) {
            correoElectronico.add(basica.getCorreoElectronico());
        }
        return correoElectronico;
    }

    public static List<String> getDestinatariosPrincipales(ResponseGetDestinatariosDTO responseGetDestinatariosDTO, List<String> destinatariosPrincipales) throws WebClientException {
        for (BodyGetDestinatariosDTO destinatarios : responseGetDestinatariosDTO.getBody()) {
            if (ObjectUtils.isEmpty(destinatarios)) {
                throw new WebClientException(BusinessException.codeAddressesListEmpty, BusinessException.valueAddressesListEmpty);
            }
            if (destinatarios.getTipoDestinatario().equals("PR")) {
                destinatariosPrincipales.add(destinatarios.getCorreoElectronico());
            }
        }
        return destinatariosPrincipales;
    }

    public static List<String> getDestinatariosSecundarios(ResponseGetDestinatariosDTO responseGetDestinatariosDTO, List<String> destinatariosCopia) {
        for (BodyGetDestinatariosDTO destinatarios : responseGetDestinatariosDTO.getBody()) {
            if (destinatarios.getTipoDestinatario().equals("CC")) {
                destinatariosCopia.add(destinatarios.getCorreoElectronico());
            }
        }
        return destinatariosCopia;
    }

    public static CorreoPlantillaDTO buildCorreoNotificacion(CorreoPlantillaDTO correo, String plantilla, String dominio, String nit, List<String> bussinesList, String fechaConvert, TokenDTO tokenDTO) {

        correo.setIdPlantilla(plantilla);
        correo.setIdDominio(dominio);
        correo.setNit(nit);
        bussinesList.add(nit);
        bussinesList.add(fechaConvert);
        correo.setBusinessList(bussinesList);
        correo.setTypeParameter(1);
        correo.setTokenAutenticatedBusiness(tokenDTO);

        return correo;

    }

}
