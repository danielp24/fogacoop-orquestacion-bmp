package com.soaint.orquestacionBpm.web.api.rest.securityjbpm.impl;


import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestUpdateInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastByNitDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesLastFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchInstancesStatusDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.securityjbpm.ResponseSearchRecoveryPassDTO;
import com.soaint.orquestacionBpm.commons.Utils.date.DateUtil;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.securityjbpm.EndpointSecurityJbpm;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.commons.generic.ResponseGenericTransactionDTO;
import com.soaint.orquestacionBpm.service.securityjbpm.ISecurityJbpmService;
import com.soaint.orquestacionBpm.web.api.rest.jbpm.impl.JbpmApiHelper;
import com.soaint.orquestacionBpm.web.api.rest.securityjbpm.ISecurityJbpmApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = EndpointBase.BASE+EndpointSecurityJbpm.BASE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@Api(description = "Servicio que contiene las operaciones atadadas a gestionar datos del esquema de seguridad de JBPM.")
public class SecurityJbpmApiController implements ISecurityJbpmApiController {

    //Properties Parameters
    @Value("${date.permited.format.pattern}")
    public String DATE_FORMAT_PATTERN;

    @Autowired
    private final ISecurityJbpmService securityJbpmService;

    public SecurityJbpmApiController(ISecurityJbpmService securityJbpmService) {
        this.securityJbpmService = securityJbpmService;
    }

    @Override
    @ApiOperation("Operacion que permite obtener todas las instancias por estado")
    @GetMapping(EndpointSecurityJbpm.INSTANCES_BY_STATUS+"{idStatusInstance}")
    public ResponseEntity getInstancesByStatus(@PathVariable String idStatusInstance, HttpServletRequest request) throws WebClientException {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            List<ResponseSearchInstancesStatusDTO> response = this.securityJbpmService.getInstancesByStatus(idStatusInstance);
            res.withResponse(response);
            if(response.size() == 0){
                res.withStatus(HttpStatus.BAD_REQUEST)
                   .withBusinessStatus(BusinessException.codeSizeIsEmpty)
                   .withMessage(BusinessException.valueSizeIsEmpty)
                   .withTransactionState(TransactionState.FAIL);
            }else {
                res.withStatus(HttpStatus.OK)
                        .withMessage("OK")
                        .withTransactionState(TransactionState.OK);
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }

    }


    @Override
    @ApiOperation("Operacion que permite ingresar una instancia al Core de Seguridad Jbpm")
    @PostMapping(EndpointSecurityJbpm.INSERT_INSTANCE)
    public ResponseEntity<?> insertInstance(@RequestBody RequestInsertInstance instance, HttpServletRequest request) throws WebClientException {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyObject(instance);
            List<ResponseGenericTransactionDTO> result = this.securityJbpmService.insertInstance(instance);
            res.withResponse(result);
            if (result.size() > 0) {
                if (result.get(0).getCode().toString().equals(String.valueOf(HttpStatus.OK.value()))) {
                    res.withStatus(HttpStatus.OK)
                            .withMessage("Elemento Insertado Exitosamente")
                            .withTransactionState(TransactionState.OK);
                } else {
                    throw new WebClientException(BusinessException.codeBadOperationGeneric, result.get(0).getMessage());
                }
            } else {
                throw new WebClientException(BusinessException.codeSizeIsEmpty, BusinessException.valueSizeIsEmpty);
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }


    @Override
    @ApiOperation("Operacion que permite actualizar una instancia al Core de Seguridad Jbpm")
    @PutMapping(EndpointSecurityJbpm.UPDATE_INSTANCE)
    public ResponseEntity<?> updateInstance(@RequestBody RequestUpdateInstance instance, HttpServletRequest request) throws WebClientException {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            List<ResponseGenericTransactionDTO> upd = this.securityJbpmService.updateInstance(instance);
            if (upd.size() > 0) {
                if (upd.get(0).getCode().toString()
                        .equals(String.valueOf(HttpStatus.OK.value()))) {
                    res.withStatus(HttpStatus.OK)
                            .withMessage("Elemento Actualizado Exitosamente")
                            .withTransactionState(TransactionState.OK);
                } else {
                    throw new WebClientException(BusinessException.codeBadOperationGeneric, upd.get(0).getMessage());
                }
            } else {
                throw new WebClientException(BusinessException.codeSizeIsEmpty, BusinessException.valueSizeIsEmpty);
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }


    @Override
    @ApiOperation("Operacion que permite obtener todas las instancias de la fecha de corte anterior y la fecha de corte actual.")
    @GetMapping(EndpointSecurityJbpm.INSTANCES_BY_LAST_FECHACORTE + "{pFechaCorte}")
    public ResponseEntity<?> getInstancesByLastFechaCorte(@PathVariable("pFechaCorte") String fechaCorte, HttpServletRequest request) throws WebClientException {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyString(fechaCorte);
            if (!DateUtil.checkThisDateValid(fechaCorte, DATE_FORMAT_PATTERN)) {
                throw new WebClientException(BusinessException.codeCatchAllException, "Fecha de corte invalida, requiere el siguiente formato :'" + DATE_FORMAT_PATTERN + "'.");
            }
            List<ResponseSearchInstancesLastFechaCorteDTO> response = this.securityJbpmService.getInstancesByLastFechaCorte(fechaCorte);
            res.withResponse(response);
            if(response.size() == 0){
                res.withStatus(HttpStatus.BAD_REQUEST)
                        .withBusinessStatus(BusinessException.codeSizeIsEmpty)
                        .withMessage(BusinessException.valueSizeIsEmpty)
                        .withTransactionState(TransactionState.FAIL);
            }else {
                res.withStatus(HttpStatus.OK)
                        .withMessage("OK")
                        .withTransactionState(TransactionState.OK);
            }
        } catch (WebClientException e) {
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }


    @Override
    @ApiOperation("Operacion que permite buscar el token de recuperacion de contraseña.")
    @PostMapping(EndpointSecurityJbpm.SEARCH_TOKEN_PASSWORD + "{token}")
    public ResponseEntity<?> searchTokenPassword(@PathVariable String token, HttpServletRequest request) throws WebClientException {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            ResponseSearchRecoveryPassDTO result = this.securityJbpmService.searchTokenPassword(token);
            res.withResponse(result);

                if (result.getPExiste().equals(new BigDecimal(1))) {
                    res.withStatus(HttpStatus.OK)
                            .withMessage("Token Validado")
                            .withTransactionState(TransactionState.OK);
                } else if (result.getPExiste().equals(new BigDecimal(0)) ) {
                    res.withStatus(HttpStatus.UNAUTHORIZED)
                            .withMessage("Token no existe o caducado")
                            .withTransactionState(TransactionState.FAIL);
                }

        } catch (WebClientException e) {
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @Override
    @ApiOperation("Operacion que permite buscar la ultima instancia a traves del NIT.")
    @GetMapping(EndpointSecurityJbpm.INSTANCES_LAST_BY_NIT+"{nit}")
    public ResponseEntity<?> getLastInstanceByNit(@PathVariable String nit, HttpServletRequest request) throws WebClientException {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            List<ResponseSearchInstancesLastByNitDTO> result = this.securityJbpmService.getLastInstanceByNit(nit);
            if(result.size() == 0){
                res.withStatus(HttpStatus.BAD_REQUEST)
                        .withBusinessStatus(BusinessException.codeSizeIsEmpty)
                        .withMessage(BusinessException.valueSizeIsEmpty)
                        .withTransactionState(TransactionState.FAIL);
            }else {
                res.withResponse(result.get(0));
                res.withStatus(HttpStatus.OK)
                        .withMessage("OK")
                        .withTransactionState(TransactionState.OK);
            }

        } catch (WebClientException e) {
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } catch (Exception e) {
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}
