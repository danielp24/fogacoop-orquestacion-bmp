package com.soaint.orquestacionBpm.web.api.rest.pagos;


import com.soaint.orquestacionBpm.adapter.specific.domain.request.jbpm.recaudo.RequestIniciarProcesoVerificarPagos;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestConsultarPagosDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IPagosApiController {


    ResponseEntity<?> consultarCompararValor(RequestConsultarPagosDTO rq, HttpServletRequest request) throws WebClientException;
    ResponseEntity<?> iniciarVerificacionPago(RequestIniciarProcesoVerificarPagos rq, HttpServletRequest request) throws WebClientException;

}
