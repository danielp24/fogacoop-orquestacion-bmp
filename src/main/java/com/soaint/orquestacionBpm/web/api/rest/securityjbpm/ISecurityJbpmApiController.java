package com.soaint.orquestacionBpm.web.api.rest.securityjbpm;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertInstance;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestUpdateInstance;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface ISecurityJbpmApiController {


    ResponseEntity<?> getInstancesByStatus(String status, HttpServletRequest request) throws WebClientException;

    ResponseEntity<?> insertInstance(RequestInsertInstance status, HttpServletRequest request) throws WebClientException;

    ResponseEntity<?> updateInstance(RequestUpdateInstance instance, HttpServletRequest request) throws WebClientException;

    ResponseEntity<?> getInstancesByLastFechaCorte(String fechaCorte, HttpServletRequest request) throws WebClientException;

    ResponseEntity<?> searchTokenPassword(String token, HttpServletRequest request)throws WebClientException;

    ResponseEntity<?> getLastInstanceByNit(String nit, HttpServletRequest request)throws WebClientException;
}
