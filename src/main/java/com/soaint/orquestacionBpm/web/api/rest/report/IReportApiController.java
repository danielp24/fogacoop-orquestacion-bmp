package com.soaint.orquestacionBpm.web.api.rest.report;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.report.RequestFR71DTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IReportApiController {


    ResponseEntity<?> reportFR71(RequestFR71DTO requestFR71DTO, HttpServletRequest request) throws WebClientException;

}
