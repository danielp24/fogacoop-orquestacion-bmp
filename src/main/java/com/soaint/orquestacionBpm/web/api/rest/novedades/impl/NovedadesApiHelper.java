package com.soaint.orquestacionBpm.web.api.rest.novedades.impl;


import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.ISgdApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDocumentoNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.ArrMetaDataDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.RequestGuardaDocExpedienteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.RequestGuardarDocFilesystemDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseFindIdDominioDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyFindIdDominioDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.filesystem.ResponseGuardarDocFilesystemDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.guardarDocExpediente.ResponseGuardarDocExpedienteDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.sgd.guardarDocExpediente.RespuestaDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.Utils.date.DateUtil;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.sgd.DocumentSendType;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo.DocumentoDTO;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo.ExpendienteEntidadDTO;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo.ResolverGuardadoDocumentosDTO;
import com.soaint.orquestacionBpm.commons.domains.request.sgd.recaudo.TrdDocumentoDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class NovedadesApiHelper {


    public static String DOMAIN_EXPEDIENTE_ENTITYS;
    public static String DOMAIN_LISTADOTRD_FILESYTEM;
    public static String DOMAIN_LISTADOTRD_FILESYTEM_PARENT_RECAUDO;
    public static String DOMAIN_INTEGRATION_NOVEDADES_LIST;
    public static String SGD_USER;
    public static String SGD_PASSWORD;
    public static String PATTERN_DATE_FORMAT;
    @Autowired
    private static IBusinessApiClient businessApiClient;
    @Autowired
    private static ISgdApiClient iSgdApiClient;

    public NovedadesApiHelper(IBusinessApiClient businessApiCliente, ISgdApiClient iSgdApiClient) {
        businessApiClient = businessApiCliente;
        NovedadesApiHelper.iSgdApiClient = iSgdApiClient;
    }

    public static Boolean resolverDocumentsIntegration(ResolverGuardadoDocumentosDTO rqGuardarDocs,
                                                       TokenDTO token,
                                                       List<RequestInsertDocumentoNovedadDTO> listaInsertDocsNovedades,
                                                       String idReturn) throws WebClientException {
        ValidationUtils.validateNullEmptyObject(rqGuardarDocs);
        ResponseFindIdDominioDTO listaGatewayIntegrationDocs = businessApiClient.findIdDominio(DOMAIN_INTEGRATION_NOVEDADES_LIST, token);
        if (listaGatewayIntegrationDocs.getBody().size() > 0) {
            if (rqGuardarDocs.getListaDocumentos().size() > 0) {
                String integrationType = NovedadesApiHelper.getIntegrationType(listaGatewayIntegrationDocs, rqGuardarDocs);
               // String integrationType = "2";
                if (!"".equalsIgnoreCase(integrationType)) {
                    ExpendienteEntidadDTO expediente = NovedadesApiHelper.getExpedienteFromEntity(businessApiClient, DOMAIN_EXPEDIENTE_ENTITYS, rqGuardarDocs, token);
                    TrdDocumentoDTO trdDoc = NovedadesApiHelper.getListTRDRecaudoPSD(businessApiClient, DOMAIN_LISTADOTRD_FILESYTEM, rqGuardarDocs, token);
                    for (DocumentoDTO item : rqGuardarDocs.getListaDocumentos()) {
                        RequestInsertDocumentoNovedadDTO responseDocs = new RequestInsertDocumentoNovedadDTO();
                        ResponseGuardarDocFilesystemDTO docFilesystem = null;
                        ResponseGuardarDocExpedienteDTO docSgd = null;
                        if (integrationType.equalsIgnoreCase(DocumentSendType.ONLY_SGD.toString())
                                || integrationType.equalsIgnoreCase(DocumentSendType.ALL_INTEGRATIONS_DOCS.toString())) {
                            docSgd = NovedadesApiHelper.sendDocumentsSgd(item, rqGuardarDocs, expediente);
                        }
                        if (integrationType.equalsIgnoreCase(DocumentSendType.ONLY_FILESYSTEM.toString())
                                || integrationType.equalsIgnoreCase(DocumentSendType.ALL_INTEGRATIONS_DOCS.toString())) {
                            docFilesystem = NovedadesApiHelper.sendDocumentsFilesytem(item, expediente, trdDoc, token);
                        }
                        NovedadesApiHelper.fusionDocumentsIntegration(responseDocs, docFilesystem, docSgd);
                        responseDocs.setIdDetalleNovedad(Integer.parseInt(idReturn));
                        responseDocs.setNombreDocumento(NovedadesApiHelper.getNombreArchivo(item.getNomDocumento()));
                        responseDocs.setUsuarioClasificador(rqGuardarDocs.getUsuarioClasificador());
                        responseDocs.setFechaInicio(DateUtil.getDateToString(new Date(), PATTERN_DATE_FORMAT));
                        listaInsertDocsNovedades.add(responseDocs);
                    }
                } else {
                    throw new WebClientException(BusinessException.codeBadOperationGeneric,
                            BusinessException.valueBadOperationGeneric + "No se ha encontrado la novedad " + rqGuardarDocs.getIdNovedad() + " asociada a un tipo de integracion documental");
                }
            } else {
                throw new WebClientException(BusinessException.codeBadOperationGeneric,
                        BusinessException.valueBadOperationGeneric + "No se han encontrado documentos para ingresar en la integracion documental");
            }
        } else {
            throw new WebClientException(BusinessException.codeBadOperationGeneric,
                    BusinessException.valueBadOperationGeneric + "No se han retornado valores de la lista de novedades parametrizadas para la integracion documental");
        }
        return Boolean.TRUE;
    }

    private static void fusionDocumentsIntegration(RequestInsertDocumentoNovedadDTO responseDocs,
                                                   ResponseGuardarDocFilesystemDTO docFilesystem,
                                                   ResponseGuardarDocExpedienteDTO docSgd) throws WebClientException {
        try {
            if (docSgd != null) {
                if (docSgd.getBody().getObjRta().getRta().size() > 0) {
                    RespuestaDTO res = docSgd.getBody().getObjRta().getRta().get(0);
                    responseDocs.setCodigoDocumento((res.getCodigo() != null) ? res.getCodigo() : "");
                    responseDocs.setIdDocSgdea((res.getIdDocumento() != null) ? res.getIdDocumento() : 0);
                }
            }
            if (docFilesystem != null) {
                responseDocs.setRutaDocumento((docFilesystem.getBody().getRutaArchivo() != null) ? docFilesystem.getBody().getRutaArchivo() : "");
            }
        } catch (Exception e) {
            LogUtils.error(e);
            throw new WebClientException(BusinessException.codeBadOperationGeneric,
                    BusinessException.valueBadOperationGeneric);
        }


    }

    private static String getExtensionArchivo(String nomDocumento) {
        if (nomDocumento.contains(".")) {
            String[] split = nomDocumento.split("\\.(?=[^\\.]+$)");
            return split[1];
        }
        return "";
    }

    private static ResponseGuardarDocFilesystemDTO sendDocumentsFilesytem(DocumentoDTO item,
                                                                          ExpendienteEntidadDTO expediente,
                                                                          TrdDocumentoDTO trdDoc,
                                                                          TokenDTO token) throws WebClientException {
        List<String> contexto = new ArrayList<>();
        NovedadesApiHelper.setContextoTrd(contexto, trdDoc, item.getIdTipDoc(), expediente);
        RequestGuardarDocFilesystemDTO rq = new RequestGuardarDocFilesystemDTO();
        rq.setBinBase64(item.getFileBase64());
        rq.setNombreArchivo(NovedadesApiHelper.getNombreArchivo(item.getNomDocumento()));
        rq.setContextoFilesystem(contexto);
        rq.setExtensionArchivo(NovedadesApiHelper.getExtensionArchivo(item.getNomDocumento()));
        return iSgdApiClient.guardarDocumento(rq, token.getTokenBusiness());
    }

    private static String getNombreArchivo(String nomDocumento) {
        if (nomDocumento.contains(".")) {
            String[] split = nomDocumento.split("\\.(?=[^\\.]+$)");
            return split[0];
        }
        return "";
    }

    private static ResponseGuardarDocExpedienteDTO sendDocumentsSgd(DocumentoDTO item,
                                                                    ResolverGuardadoDocumentosDTO rqGuardarDocs,
                                                                    ExpendienteEntidadDTO expediente) throws WebClientException {
        try {
            RequestGuardaDocExpedienteDTO rq = new RequestGuardaDocExpedienteDTO();
            rq.setUsername(SGD_USER);
            rq.setPassword(Integer.parseInt(SGD_PASSWORD));
            rq.setFchDoc(DateUtil.getDateToString(new Date(), PATTERN_DATE_FORMAT));
            rq.setFileBase64(item.getFileBase64());
            rq.setIdTipDoc(Integer.parseInt(item.getIdTipDoc()));
            rq.setIdExpediente(Integer.parseInt(expediente.getIdExpediente()));
            rq.setNomDocumento(item.getNomDocumento());
            rq.setUsuario(rqGuardarDocs.getUsuarioClasificador());
            rq.setNomDocVisible(item.getNomDocVisible());
            List<ArrMetaDataDTO> arryMetaDto = new ArrayList<>();
            arryMetaDto.add(new ArrMetaDataDTO("idSerie", "23456"));
            rq.setArrMetaData(arryMetaDto);
            return iSgdApiClient.guardaDocExpediente(rq, "");
        } catch (Exception e) {
            LogUtils.error(e);
            throw new WebClientException(BusinessException.codeBadOperationGeneric,
                    BusinessException.valueBadOperationGeneric + e.getMessage());
        }

    }

    private static String getIntegrationType(ResponseFindIdDominioDTO listaGatewayIntegrationDocs, ResolverGuardadoDocumentosDTO rqGuardarDocs) {
        listaGatewayIntegrationDocs.getBody().forEach(item -> LogUtils.info(item.getNombre() + " - " + item.getValor()));
        for (BodyFindIdDominioDTO bodyListaIntDocs : listaGatewayIntegrationDocs.getBody()) {
            if (bodyListaIntDocs.getValor() != null
                    && bodyListaIntDocs.getIdPadre() != null
                    && rqGuardarDocs.getIdNovedad().equals(bodyListaIntDocs.getValor())) {
                List<BodyFindIdDominioDTO> listItemFilter = listaGatewayIntegrationDocs.getBody().stream().filter(
                        item -> item.getIdParametro().toString().equalsIgnoreCase(bodyListaIntDocs.getIdPadre().toString())
                ).collect(Collectors.toList());
                if (listItemFilter.size() > 0) {
                    return listItemFilter.get(0).getValor();
                }
                break;
            }
        }
        return "";
    }

    private static ExpendienteEntidadDTO filterDominioByNit(ResponseFindIdDominioDTO listaGatewayIntegrationDocs, String nit) throws WebClientException {
        List<BodyFindIdDominioDTO> listItemFilter = listaGatewayIntegrationDocs.getBody().stream().filter(
                item -> item.getNombre().equalsIgnoreCase(nit)
        ).collect(Collectors.toList());
        if (listItemFilter.size() > 0) {
            String parent = listItemFilter.get(0).getIdParametro().toString();
            listItemFilter.clear();
            listItemFilter = listaGatewayIntegrationDocs.getBody().stream().filter(
                    item -> item.getIdPadre() != null
                            && item.getIdPadre().toString().equalsIgnoreCase(parent)
            ).collect(Collectors.toList());
            ExpendienteEntidadDTO exp = new ExpendienteEntidadDTO();
            listItemFilter.forEach(
                    item -> {
                        if (item.getNombre().equals("idExpediente")) {
                            exp.setIdExpediente(item.getValor());
                        } else if (item.getNombre().equals("codExpediente")) {
                            exp.setCodExpediente(item.getValor());
                        }
                        if (item.getNombre().equals("nombreEntidad")) {
                            exp.setNombreEntidad(item.getValor());
                        }
                    });
            return exp;
        } else {
            throw new WebClientException(BusinessException.codeBadOperationGeneric,
                    BusinessException.valueBadOperationGeneric + "No se encontro un expediente para la entidad " + nit);
        }
    }

    private static void setContextoTrd(List<String> contexto, TrdDocumentoDTO trdDoc, String tipoDoc, ExpendienteEntidadDTO expediente) {

        if (trdDoc.getDependencia() != null && !"".equals(trdDoc.getDependencia())) {
            contexto.add(trdDoc.getDependencia());
        }
        if (trdDoc.getSerie() != null && !"".equals(trdDoc.getSerie())) {
            contexto.add(trdDoc.getSerie());
        }
        if (trdDoc.getSubserie() != null && !"".equals(trdDoc.getSubserie())) {
            contexto.add(trdDoc.getSubserie());
        }
        if (expediente != null && !"".equals(expediente)) {
            contexto.add(expediente.getCodExpediente());
        }
        if (tipoDoc != null && !"".equals(tipoDoc)) {
            contexto.add(tipoDoc);
        }
    }

    private static TrdDocumentoDTO getListTRDRecaudoPSD(IBusinessApiClient businessApiClient, String domainListadotrdFilesytem, ResolverGuardadoDocumentosDTO rqGuardarDocs, TokenDTO token) throws WebClientException {
        ResponseFindIdDominioDTO res = businessApiClient.findIdDominio(domainListadotrdFilesytem, token);
        return NovedadesApiHelper.filterTRDRecaudo(res);
    }

    private static ExpendienteEntidadDTO getExpedienteFromEntity(IBusinessApiClient businessApiClient, String domainExpedienteEntitys, ResolverGuardadoDocumentosDTO rqGuardarDocs, TokenDTO token) throws WebClientException {
        ResponseFindIdDominioDTO res = businessApiClient.findIdDominio(domainExpedienteEntitys, token);
        return NovedadesApiHelper.filterDominioByNit(res, rqGuardarDocs.getNit());
    }

    private static TrdDocumentoDTO filterTRDRecaudo(ResponseFindIdDominioDTO listaTrdDominio) throws WebClientException {
        TrdDocumentoDTO trd = new TrdDocumentoDTO();
        for (BodyFindIdDominioDTO itemDependencia : listaTrdDominio.getBody()) {
            if (itemDependencia.getNombre().equals("dependencia")
                    && itemDependencia.getIdParametro().toString().equals(DOMAIN_LISTADOTRD_FILESYTEM_PARENT_RECAUDO)) {
                trd.setDependencia(itemDependencia.getValor());
                for (BodyFindIdDominioDTO itemSerie : listaTrdDominio.getBody()) {
                    if (itemSerie.getIdPadre() != null
                            && itemDependencia.getIdParametro().toString().equals(itemSerie.getIdPadre().toString())) {
                        trd.setSerie(itemSerie.getValor());
                        for (BodyFindIdDominioDTO itemSubSerie : listaTrdDominio.getBody()) {
                            if (itemSubSerie.getIdPadre() != null
                                    && itemSerie.getIdParametro().toString().equals(itemSubSerie.getIdPadre().toString())) {
                                trd.setSubserie(itemSubSerie.getValor());
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        return trd;
    }

    //Vars
    @Autowired
    public void setVars(@Value("${domain.entidades.expedientes}") String domainEntityExpediente,
                        @Value("${domain.listadotrd.filesystem}") String domainListTrdFilesystem,
                        @Value("${domain.listadotrd.filesystem.parent.recaudo}") String domainListTrdFilesystemParentRecaudo,
                        @Value("${domain.novedades.almacenamientodocs}") String domainIntegrationNnedadesList,
                        @Value("${sgd.authorized.user}") String userSgd,
                        @Value("${sgd.authorized.password}") String passwordSgd,
                        @Value("${date.permited.format.pattern}") String formatDate
    ) {
        DOMAIN_EXPEDIENTE_ENTITYS = domainEntityExpediente;
        DOMAIN_LISTADOTRD_FILESYTEM = domainListTrdFilesystem;
        DOMAIN_LISTADOTRD_FILESYTEM_PARENT_RECAUDO = domainListTrdFilesystemParentRecaudo;
        DOMAIN_INTEGRATION_NOVEDADES_LIST = domainIntegrationNnedadesList;
        SGD_USER = userSgd;
        SGD_PASSWORD = passwordSgd;
        PATTERN_DATE_FORMAT = formatDate;
    }


}
