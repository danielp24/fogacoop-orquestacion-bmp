package com.soaint.orquestacionBpm.web.api.rest.novedades.impl;

import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.business.IBusinessApiClient;
import com.soaint.orquestacionBpm.adapter.specific.api.module.firstapi.impl.sgd.ISgdApiClient;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.novedades.RequestInsertDocumentoNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.pagos.RequestNitFechaCorteWithSecurityDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetDetalleNovedadesDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetDetalleNovedadesWithDocsDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.ResponseGetListaDocumentosNovedadDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetDetalleNovedadesDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.response.business.body.BodyGetListaDocumentosNovedadDTO;
import com.soaint.orquestacionBpm.commons.Utils.exception.ValidationUtils;
import com.soaint.orquestacionBpm.commons.Utils.log.LogUtils;
import com.soaint.orquestacionBpm.commons.constants.api.EndpointBase;
import com.soaint.orquestacionBpm.commons.constants.api.novedades.EndpointNovedades;
import com.soaint.orquestacionBpm.commons.domains.request.novedades.RequestResolverIngresoNovedadesDTO;
import com.soaint.orquestacionBpm.commons.domains.response.builder.ResponseBuilder;
import com.soaint.orquestacionBpm.commons.domains.response.business.ResponseGenericTransactionWResDTO;
import com.soaint.orquestacionBpm.commons.enums.TransactionState;
import com.soaint.orquestacionBpm.commons.exception.webClient.BusinessException;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import com.soaint.orquestacionBpm.web.api.rest.novedades.INovedadesApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping(value = EndpointBase.BASE + EndpointNovedades.NOVEDADES)
@Api(description = "Servicio que contiene las operaciones atadadas a las novedades de la entidad")
public class NovedadesApiController implements INovedadesApiController {

    @Autowired
    private final IBusinessApiClient businessApiClient;
    @Autowired
    private final ISgdApiClient iSgdApiClient;

    public NovedadesApiController(IBusinessApiClient businessApiCliente, ISgdApiClient iSgdApiClient) {
        this.businessApiClient = businessApiCliente;
        this.iSgdApiClient = iSgdApiClient;
    }

    @Override
    @PostMapping(EndpointNovedades.RESOLVER_INGRESO_NOVEDAD)
    @ApiOperation("Controlador que permite ingresar todos los datos de la novedad segun sea el caso, base de datos e integracion documental")
    public ResponseEntity resolverIngresoNovedad(@RequestBody RequestResolverIngresoNovedadesDTO ingresoNovedades, HttpServletRequest request) throws WebClientException {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyObject(ingresoNovedades);
            Boolean docsSucess;
            ResponseGenericTransactionWResDTO resInsDetNovedades = businessApiClient.insertDetalleNovedades(ingresoNovedades.getNovedad(), ingresoNovedades.getTokenAutenticatedBusiness());
            if (resInsDetNovedades.getBody().getCode().toString()
                    .equals(String.valueOf(HttpStatus.OK.value()))) {
                if (!"".equals(resInsDetNovedades.getBody().getIdReturn())) {
                    if (ingresoNovedades.getDocumentos() != null) {
                        if (ingresoNovedades.getDocumentos().getListaDocumentos() != null
                                && ingresoNovedades.getDocumentos().getListaDocumentos().size() > 0) {
                            List<RequestInsertDocumentoNovedadDTO> listaInsertDocsNovedades = new ArrayList<>();
                            docsSucess = NovedadesApiHelper.resolverDocumentsIntegration(ingresoNovedades.getDocumentos(),
                                    ingresoNovedades.getTokenAutenticatedBusiness(),
                                    listaInsertDocsNovedades,
                                    resInsDetNovedades.getBody().getIdReturn());
                            if (docsSucess) {
                                for (RequestInsertDocumentoNovedadDTO document : listaInsertDocsNovedades) {
                                    this.businessApiClient.insertDocumentoNovedad(document, ingresoNovedades.getTokenAutenticatedBusiness());
                                }
                            }
                            response.withResponse(resInsDetNovedades.getBody())
                                    .withStatus(HttpStatus.OK)
                                    .withBusinessStatus(String.valueOf(HttpStatus.OK.value()))
                                    .withMessage("Resolver ingreso de la novedad ejecutado exitosamente");
                        }
                    }
                    if (ingresoNovedades.getNovedadRadicado() != null) {
                        ingresoNovedades.getNovedadRadicado().setIdDetalleNovedad(Integer.parseInt(resInsDetNovedades.getBody().getIdReturn()));
                        this.businessApiClient.insertDetalleNovedadesRadicado(ingresoNovedades.getNovedadRadicado(), ingresoNovedades.getTokenAutenticatedBusiness());
                    }

                }
                response.withResponse(resInsDetNovedades.getBody())
                        .withStatus(HttpStatus.OK)
                        .withBusinessStatus(String.valueOf(HttpStatus.OK.value()))
                        .withMessage("Resolver ingreso de la novedad ejecutado exitosamente");
            } else {
                throw new WebClientException(BusinessException.codeCatchAllException,
                        BusinessException.valueBadOperationGeneric + ", insertDetalleNovedades no retorno el id");
            }

        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @Override
    @PostMapping(EndpointNovedades.GET_LISTA_NOVEDADES_DOCUMENTOS)
    @ApiOperation("Controlador que permite consultar las novedades y los documentos suministrados a traves de el numero de identificacion y la fecha de corte")
    public ResponseEntity getListaDetallesNovedades(@RequestBody RequestNitFechaCorteWithSecurityDTO rq, HttpServletRequest request) throws WebClientException {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyObject(rq);
            List<ResponseGetDetalleNovedadesWithDocsDTO> listDetallesNov = new ArrayList<>();
            ResponseGetDetalleNovedadesDTO resLstDetalleNovedades = this.businessApiClient.getDetalleNovedades(rq, rq.getTokenAutenticatedBusiness());
            if (resLstDetalleNovedades.getBody() != null && resLstDetalleNovedades.getBody().length > 0) {
                for (BodyGetDetalleNovedadesDTO item : resLstDetalleNovedades.getBody()) {
                    ResponseGetDetalleNovedadesWithDocsDTO itemRes = new ResponseGetDetalleNovedadesWithDocsDTO();
                    itemRes.setNovedad(item);
                    ResponseGetListaDocumentosNovedadDTO docs = this.businessApiClient.getListaDocumentosNovedad(Integer.parseInt(item.getIdDetalleNovedad().toString()), rq.getTokenAutenticatedBusiness());
                    if (docs.getBody() != null
                            && docs.getBody().length > 0) {
                        List<BodyGetListaDocumentosNovedadDTO> listDocs = Arrays.asList(docs.getBody());
                        itemRes.setListaDocumentos(listDocs);
                    }
                    listDetallesNov.add(itemRes);
                }
                response.withResponse(listDetallesNov)
                        .withStatus(HttpStatus.OK)
                        .withBusinessStatus(String.valueOf(HttpStatus.OK.value()))
                        .withMessage("Lista de novedades retornados exitosamente.");
            } else {
               /* throw new WebClientException(BusinessException.codeCatchAllException,
                        "No se retornaron novedades para el período y fecha de corte suministrados");*/
                response.withResponse(listDetallesNov)
                        .withStatus(HttpStatus.OK)
                        .withBusinessStatus(String.valueOf(HttpStatus.OK.value()))
                        .withMessage(BusinessException.valueSizeIsEmpty);
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(BusinessException.codeCatchAllException)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }
}



