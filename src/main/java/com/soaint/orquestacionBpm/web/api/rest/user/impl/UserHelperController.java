package com.soaint.orquestacionBpm.web.api.rest.user.impl;

import com.soaint.orquestacionBpm.adapter.specific.domain.request.securityjbpm.RequestInsertRecoveryPassword;
import com.soaint.orquestacionBpm.adapter.specific.domain.request.sgd.lstAdjuntosDTO;
import com.soaint.orquestacionBpm.adapter.specific.domain.token.TokenDTO;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.CorreoPlantillaDTO;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserHelperController {


    public static String converBase64(String token) throws IOException {

        String encodedString = Base64.getEncoder().encodeToString(token.getBytes());

        return encodedString;
    }


    public static String reemplazarCuerpoMensaje(List<String> pTemplate, HashMap<String, String> template, String token, String cuerpo) {

        String texto;
        for (int i = 0; i < pTemplate.size(); i++) {
            template.put(pTemplate.get(i), token);
            for (Map.Entry<String, String> entry : template.entrySet()) {
                texto = cuerpo.replaceAll("<" + entry.getKey() + "/>", entry.getValue());
                cuerpo = texto;
            }
        }
        return cuerpo;
    }

    public static RequestInsertRecoveryPassword buildRequestRecovery(String token, String email) {
        RequestInsertRecoveryPassword spRecovery = new RequestInsertRecoveryPassword();
        spRecovery.setEmail(email);
        spRecovery.setToken(token);
        return spRecovery;
    }

    public static CorreoPlantillaDTO buildBussinesParameters(String idPlantilla, String idDominio, String nit, List<String> businessList,
                                                             Integer typeParameter, List<lstAdjuntosDTO> lstAdjuntos, TokenDTO tokenAutenticatedBusiness) {

        CorreoPlantillaDTO plantillaDTO = new CorreoPlantillaDTO();
        plantillaDTO.setIdPlantilla(idPlantilla);
        plantillaDTO.setIdDominio(idDominio);
        plantillaDTO.setNit(nit);
        plantillaDTO.setBusinessList(businessList);
        plantillaDTO.setTypeParameter(typeParameter);
        plantillaDTO.setLstAdjuntos(lstAdjuntos);
        plantillaDTO.setTokenAutenticatedBusiness(tokenAutenticatedBusiness);

        return plantillaDTO;
    }
}
