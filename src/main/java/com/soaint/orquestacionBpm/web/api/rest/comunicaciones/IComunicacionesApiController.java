package com.soaint.orquestacionBpm.web.api.rest.comunicaciones;

import com.soaint.orquestacionBpm.commons.domains.generic.jbpm.UserDto;
import com.soaint.orquestacionBpm.commons.domains.request.comunicaciones.CorreoPlantillaDTO;
import com.soaint.orquestacionBpm.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IComunicacionesApiController {

    ResponseEntity enviarCorreoPlantilla(CorreoPlantillaDTO plantillaDTO, HttpServletRequest request) throws WebClientException;

    ResponseEntity notiFechaPagoEntidadInscrita(UserDto userDto, HttpServletRequest request) throws WebClientException;


}
