package com.soaint.orquestacionBpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.util.Calendar;
import java.util.Date;


public class test {


    public static Duration durationISO8601(String oldDateString, String newDateString, String pattern) {
        if (pattern == null || "".equals(pattern)) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime oldDate = LocalDateTime.parse(oldDateString, formatter);
        LocalDateTime newDate = LocalDateTime.parse(newDateString, formatter);

        System.out.println(oldDate);
        System.out.println(newDate);

        return Duration.between(oldDate, newDate);
    }

    public static void main(String[] args) {
        //Duration dur = test.durationISO8601(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "2020-03-04 20:00:00",null);
        test.addDurationToDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "PT4H", "");
        //System.out.println(dur.toString());
    }

    public static String addDurationToDate(String oldDateString, String duration, String pattern) {
        if (pattern == null || "".equals(pattern)) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        Duration time = Duration.parse(duration);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(new SimpleDateFormat(pattern).parse(oldDateString));
            cal.add(Calendar.SECOND, (int) time.getSeconds());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("old date = " + oldDateString);
        System.out.println("new date = " + new SimpleDateFormat(pattern).format(cal.getTime()));


        return new SimpleDateFormat(pattern).format(cal.getTime());
    }

}